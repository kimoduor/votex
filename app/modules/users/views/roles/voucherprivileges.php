<?php
$this->breadcrumbs = array(
    Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
    $this->pageTitle,
);
$model_class_name = RolesOnVoucherPrivileges::model()->getClassName();
?>
<div class="row">
    <div class="col-md-2">
        <?php $this->renderPartial('users.views.layouts._tab') ?>
    </div>
    <div class="col-md-10">
        <div class="wells well-lights">
            <?php echo CHtml::beginForm(Yii::app()->createUrl($this->route, $this->actionParams), 'POST', array('class' => '', 'id' => 'my-roles-view-form', 'role' => 'form')) ?>
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="page-title txt-color-blueDark">
                        <?php echo CHtml::encode($this->pageTitle) ?>
                        <small><?php echo CHtml::encode($model->description) ?></small>
                    </h1>
                </div>
                <div class="col-sm-4 padding-top-10">
                    <div class="pull-right">
                        <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-check"></i> <?php echo Lang::t('Save Changes') ?></button>
                        <a class="btn btn-danger" href="<?php echo $this->createUrl('index') ?>"><i class="fa fa-times"></i> <?php echo Lang::t('Close') ?></a>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-edit"></i> <?php echo Lang::t('Review Voucher Privileges for ') . CHtml::encode($this->pageTitle) . Lang::t('  Role') ?><span class="pull-right"><button class="btn btn-link my-select-all" type="button"><?php echo Lang::t('Check All') ?></button></span></h3>
                </div>
                <div class="table-responsive">
                    <table class="table table-condensed">
                        <thead>
                            <tr style="background-color:inherit;background-image: none">
                                <th><?php echo Lang::t('Privilege') ?></th>
                                <th><?php echo Lang::t('Can View') ?></th>
                                <th><?php echo Lang::t('Can Create') ?></th>
                                <th><?php echo Lang::t('Can Update') ?></th>
                                <th><?php echo Lang::t('Can Delete') ?></th>
                                <th><?php echo Lang::t('Can Approve') ?></th>
                                <th><?php echo Lang::t('Can Export') ?></th>
                                <th><?php echo Lang::t('Normal Search') ?></th>
                                <th><?php echo Lang::t('Advanced Search') ?></th>
                                <th><?php echo Lang::t('can cancel') ?></th>
                                <th><?php echo Lang::t('can redeem') ?></th>
                                <th><?php echo Lang::t('Can Export All') ?></th>

                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($privileges as $p): ?>
                                <tr>
                                    <td><?php echo $p['description'] ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'viewable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][view]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][view]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'view'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'createable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][create]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][create]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'create'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'updateable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][update]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][update]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'update'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'deleteable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][delete]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][delete]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'delete'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'approveable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][approve]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][approve]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'approve'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'exportable') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][export]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][export]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'export'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'normal_search') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][search]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][search]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'search'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'advanced_search') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][a_search]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][a_search]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'a_search'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'cancel') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][cancel]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][cancel]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'cancel'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'uncancel') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][uncancel]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][uncancel]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'uncancel'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                    <td><?php if (VoucherPrivileges::model()->get($p['id'], 'exportall') == 1): ?><?php echo CHtml::hiddenField($model_class_name . '[' . $p['id'] . '][exportall]', 0) ?><?php echo CHtml::checkBox($model_class_name . '[' . $p['id'] . '][exportall]', RolesOnVoucherPrivileges::model()->getValue($p['id'], $model->id, 'exportall'), array('class' => 'my-roles-checkbox')) ?><?php else: ?>N/A<?php endif ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <?php echo CHtml::endForm(); ?>
    </div>
</div>
<?php
Yii::app()->clientScript
        ->registerCssFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.min.css')
        ->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugin/chosen/chosen.jquery.min.js', CClientScript::POS_END)
        ->registerScript('users.roles.view', "UsersModule.Roles.init();$('.chosen-select').chosen();");
?>