<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
<?php
$modelx = UserRoles::model()->findAll();
$output = array();
$output[""] = "[Select One]";
foreach ($modelx as $modely) {
    $output[$modely->id] = $modely->name;
}
$theoutput = json_encode($output);
?>
<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div> Roles: </div><div>{role_id} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'users/newroles/data',
            // we set the changes to be made at client side using predefined word clientArray
            //editurl: '',
            datatype: "json",
            rownumbers: true,
            toppager: true,
            colModel: [
                {name: 'id', hidden: true, editable: true, search: false},

                {
                    label: 'First Name',
                    name: 'fname',
                    width: 200,
                    editable: true,
                    editrules: {required: true}
                },
                {
                    label: 'Last Name',
                    name: 'lname',
                    width: 200,
                    editable: true,
                    editrules: {required: true}
                },   {
                    label: 'Other Name',
                    name: 'othername',
                    width: 200,
                    editable: false,
                    editrules: {required: true}
                },
                
                {
                    label: 'Username',
                    name: 'username',
                    width: 200,
                    editable: false,
                    editrules: {required: true}
                }, 
                {
                    label: 'Initials',
                    name: 'initials',
                    width: 200,
                    editable: true,
                    editrules: {required: true}
                },{
                    label: 'Email',
                    name: 'email',
                    width: 200,
                    editable: false,
                    editrules: {required: true}
                },
                {
                    label: 'mobile Number',
                    name: 'phone',
                    width: 200,
                    editable: false,
                    editrules: {required: true}
                },
                {
                    label: 'Date Joined',
                    name: 'date_created',
                    width: 200,
                    editable: false,
                    editrules: {required: true}
                },
                {
                    label: 'Last Login',
                    name: 'last_login',
                    width: 200,
                    editable: false,
                    editrules: {required: true}
                },
                {
                    label: 'Role',
                    name: 'role_id',
                    width: 200,
                    sortable: false,
                    search: false,
                    edittype: "select",
                    editable: true,
                    formatter: 'select',
                    // formatoptions: {baseLinkUrl: 'members/relationship/data'},
                    editrules: {required: true},
                    editoptions: {value:<?php echo $theoutput; ?>}
                },
            ],
            sortname: 'id',
            sortorder: 'asc',
            loadonce: true,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager"
        }).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                        {edit: true, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
                {url: "users/newroles/update", closeAfterEdit: true, afterSubmit: processAddEdit},
                {url: "users/newroles/create", closeAfterAdd: true, afterSubmit: processAddEdit},
                {url: "users/newroles/delete"},
                {// reload
                    reloadAfterSubmit: true
                },
                // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }

                });
                $('#jqGrid').jqGrid('filterToolbar', {
                    // JSON stringify all data from search, including search toolbar operators
                    stringResult: true,
                    // instuct the grid toolbar to show the search options
                    searchOperators: false,
                });
            });
</script>

