<?php
$this->breadcrumbs = array(
    $this->pageTitle,
);
?>
<div class="row">
    <div class="col-sm-10">
        <h1 class="txt-color-blueDark">
            <?php echo CHtml::encode($model->name); ?>
        </h1>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-xs-12 col-sm-10">
        <?php $this->renderPartial('forms/_form', array('model' => $model)); ?>
    </div>
</div>
