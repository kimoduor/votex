<?php
$can_update = $model->checkPrivilege($this, Acl::ACTION_UPDATE);
$can_view_activity = $this->showLink(UsersModuleConstants::RES_USER_ACTIVITY);
?>
<div class="text-center">
    <?php echo CHtml::image(UserImages::model()->getProfileImageUrl($model->id, 250, 200), CHtml::encode($model->name), array('id' => 'avator', 'class' => 'editable img-responsive thumbnail', 'style' => 'margin-left:auto;margin-right:auto;')); ?>
    <p><i class="fa fa-star"></i> <?php echo UserLevels::decodeUserLevel($model->user_level); ?></p>
    <p><i class="fa fa-envelope-o"></i> <?php echo CHtml::encode($model->email); ?></p>
    <?php if (!empty($model->phone)): ?>
        <p><i class="fa fa-phone"></i> <?php echo CHtml::encode($model->phone) ?></p>
    <?php endif; ?>
</div>
<div class="list-group">
        <?php if (Users::isMyAccount($model->id)): 
            $label=null;
        if($model->user_level===UserLevels::LEVEL_AGRODEALER){
           // $label='Return to Verify';
        }else{$label='Return Home';}
            ?>
<!--        <a class="list-group-item" href="<?php // echo $this->createUrl('../default/verification'); ?>"><i class="fa fa-navicon text-success"></i> <?php // echo Lang::t($label) ?></a>-->
    <?php endif; ?>
    <?php if (Users::isMyAccount($model->id)): ?>
        <a class="list-group-item" href="<?php echo $this->createUrl('changePassword') ?>"><i class="fa fa-lock text-success"></i> <?php echo Lang::t('Change your password') ?></a>
    <?php endif; ?>
         
                
  <?php if ($can_update || Users::isMyAccount($model->id)&&($model->user_level!=UserLevels::LEVEL_AGRODEALER)&&($model->user_level!=UserLevels::LEVEL_SUPPLIER)): ?>

        <a class="list-group-item" href="<?php echo $this->createUrl('update', array('id' => $model->id)) ?>"><i class="fa fa-edit text-success"></i> <?php echo Lang::t('Edit Profile') ?></a>
      <?php endif; ?>
        
    <?php if ($can_update): ?>
        <a class="list-group-item" href="<?php echo $this->createUrl('resetPassword', array('id' => $model->id)) ?>"><i class="fa fa-lock text-success"></i> <?php echo Lang::t('Reset Password') ?></a>
        <?php if ($model->status === Users::STATUS_ACTIVE): ?>
            <a class="list-group-item change_status" href="#" data-ajax-url="<?php echo $this->createUrl('changeStatus', array('id' => $model->id, 'status' => Users::STATUS_BLOCKED)) ?>" ><i class="fa fa-ban text-danger"></i> <?php echo Lang::t('Block Account') ?></a>
        <?php else: ?>
            <a class="list-group-item change_status" href="#" data-ajax-url="<?php echo $this->createUrl('changeStatus', array('id' => $model->id, 'status' => Users::STATUS_ACTIVE)) ?>"><i class="fa fa-check-circle text-success"></i> <?php echo Lang::t('Activate Account') ?></a>
        <?php endif; ?>
    <?php endif; ?>
    <?php if ($model->checkPrivilege($this, Acl::ACTION_DELETE)): ?>
        <a id="delete_user" class="list-group-item" href="javascript:void(0);" data-href="<?php echo $this->createUrl('delete', array('id' => $model->id)) ?>" data-confirm="<?php echo Lang::t('You are about to permanently delete this account. Are you sure?') ?>"><i class="fa fa-trash-o text-danger"></i> <?php echo Lang::t('Delete account') ?></a>
    <?php endif; ?>
    <?php if ($can_view_activity): ?>
        <a class="hidden list-group-item" href="<?php echo $this->createUrl('activityLog', array('id' => $model->id)) ?>"><i class="fa fa-signal"></i> <?php echo Lang::t('Account activities') ?></a>
    <?php endif; ?>
</div>
<?php Yii::app()->clientScript->registerScript('users.default.userMenu', "UsersModule.User.init();"); ?>


