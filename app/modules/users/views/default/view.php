<?php
if ($this->showLink(UsersModuleConstants::RES_USERS)):
    $this->breadcrumbs = array(
        Lang::t(Common::pluralize($this->resourceLabel)) => array('index'),
        $model->name,
    );
else:
    $this->breadcrumbs = array(
        $model->name,
    );
endif;
?>
<div class="row">
    <div class="col-sm-10">
        <h1 class="txt-color-blueDark">
            <?php echo CHtml::encode($this->pageTitle); ?>
        </h1>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-xs-12 col-sm-3">
        <?php $this->renderPartial('_userMenu', array('model' => $model)) ?>
    </div>
    <div class="col-xs-12 col-sm-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><?php echo Lang::t('Account Info') ?></h3>
            </div>
            <?php
            $this->widget('application.components.widgets.DetailView', array(
                'data' => $model,
                'attributes' => array(
                    array(
                        'name' => 'id',
                    ),

                    array(
                        'name' => 'username',
                    ),
                    array(
                        'name' => 'email',
                    ),
                    
                    array(
                        'name' => 'role_id',
                      
                        'value' => UserRoles::model()->get($model->role_id, 'name'),
                    ),
                    array(
                        'name' => 'timezone',
                    ),
                    array(
                        'name' => 'date_created',
                        'value' => MyYiiUtils::formatDate($model->date_created),
                    ),
                    array(
                        'name' => 'created_by',
                        'value' => Users::model()->get($model->created_by, "username"),
                        'visible' => !empty($model->created_by),
                    ),
                    array(
                        'name' => 'last_login',
                        'value' => MyYiiUtils::formatDate($model->last_login),
                    ),

                ),
            ));
            ?>
        </div>
    </div>
</div>