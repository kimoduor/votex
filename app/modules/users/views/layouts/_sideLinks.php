<?php if ($this->showLink(UsersModuleConstants::RES_USERS)): ?>
    <li class="<?php echo $this->activeMenu === UsersModuleConstants::MENU_USERS ? 'active' : '' ?>">
        <a href="<?php echo Yii::app()->createUrl('users/default/index') ?>"><i class="fa fa-lg fa-fw fa-lock"></i> <span class="menu-item-parent"><?php echo Lang::t('ADMINISTRATION') ?></span></a>
    </li>
<?php endif; ?>