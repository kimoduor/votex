<?php

/**
 * This is the model class for table "user_images".
 *
 * The followings are the available columns in table 'user_images':
 * @property string $id
 * @property string $user_id
 * @property string $image
 * @property string $image_size_id
 * @property string $date_created
 * @property integer $is_profile_image
 *
 */
class UserImages extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user_images';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('user_id, image', 'required'),
            array('is_profile_image', 'numerical', 'integerOnly' => true),
            array('user_id, image_size_id', 'length', 'max' => 11),
            array('image', 'length', 'max' => 128),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return UserImages the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    protected function getDefaultProfileImagePath()
    {
        return Yii::getPathOfAlias('users.assets.images') . DS . 'defaultProfile.png';
    }

    /**
     * Upate profile image
     * @param type $user_id
     * @param type $image_path
     */
    public function updateProfileImage($user_id, $image_path)
    {
        //using fineuploader
        if (!empty($image_path)) {
            $this->removeProfileImage($user_id);
            $is_profile_image = 1;
            $temp_file = Common::parseFilePath($image_path);
            $image_name = $temp_file['name'];
            $temp_dir = $temp_file['dir'];
            $new_path = Users::model()->getDir($user_id) . DS . $image_name;
            if (copy($image_path, $new_path)) {
                if (!empty($temp_dir))
                    Common::deleteDir($temp_dir);

                $this->addImage($user_id, $image_name, NULL, $is_profile_image);
                $this->createThumbs($user_id, $new_path, $image_name, $is_profile_image);
            }
        }
    }

    /**
     * Create image thumbs
     * @param type $user_id
     * @param type $image_path
     * @param type $image_name
     * @param type $is_profile_image
     */
    public function createThumbs($user_id, $image_path, $image_name, $is_profile_image = 1)
    {
        $sizes = SettingsImageSizes::model()->getSizes();
        if (!empty($sizes)) {
            $base_dir = Users::model()->getDir($user_id);
            foreach ($sizes as $size) {
                $thumb_name = $size['width'] . '_' . $size['height'] . '_' . $image_name;
                $new_path = $base_dir . DS . $thumb_name;
                MyYiiUtils::createEasyImageThumb($image_path, $new_path, (int) $size['width'], (int) $size['height']);
                $this->addImage($user_id, $thumb_name, $size['id'], $is_profile_image);
            }
        }
    }

    /**
     * Add image
     * @param type $user_id
     * @param type $image
     * @param type $image_size_id
     * @param type $is_profile_image
     * @return type
     */
    public function addImage($user_id, $image, $image_size_id = NULL, $is_profile_image = 1)
    {
        return Yii::app()->db->createCommand()
                        ->insert($this->tableName(), array(
                            'user_id' => $user_id,
                            'image' => $image,
                            'image_size_id' => $image_size_id,
                            'is_profile_image' => $is_profile_image,
        ));
    }

    /**
     * Remove profile image
     * @param type $user_id
     */
    public function removeProfileImage($user_id)
    {
        $conditions = '`user_id`=:t1 AND `is_profile_image`=:t2';
        $params = array(':t1' => $user_id, ':t2' => 1);
        $images = $this->getData('id,image', $conditions, $params);
        if (!empty($images)) {
            foreach ($images as $img) {
                $image_path = Users::model()->getDir($user_id) . DS . $img['image'];
                if (file_exists($image_path))
                    @unlink($image_path);
            }

            Yii::app()->db->createCommand()
                    ->delete($this->tableName(), $conditions, $params);
        }
    }

    /**
     * Get profile image
     * @param type $user_id
     * @param type $width
     * @param type $height
     * @param type $default_src
     * @return type
     */
    public function getProfileImageUrl($user_id, $width = NULL, $height = NULL, $default_src = "#")
    {
        $image = NULL;
        $image_path = NULL;
        if (!empty($user_id)) {
            if (!empty($width) && !empty($height)) {
                $image = Yii::app()->db->createCommand()
                        ->select('a.image')
                        ->from($this->tableName() . ' a')
                        ->join(SettingsImageSizes::model()->tableName() . ' b', '`a`.`image_size_id`=`b`.`id`')
                        ->where('`a`.`user_id`=:t1 AND `is_profile_image`=1 AND (`b`.`width`=:t2 AND `b`.`height`=:t3)', array(':t1' => $user_id, ':t2' => $width, ':t3' => $height))
                        ->queryScalar();
                $image_path = Users::model()->getDir($user_id) . DS . $image;
                if (!empty($image) && file_exists($image_path))
                    return Yii::app()->baseUrl . '/public/' . Users::BASE_DIR . '/' . $user_id . '/' . $image;
            }
            if (empty($image)) {
                $image = $this->getScalar('image', '`user_id`=:t1 AND `is_profile_image`=1 AND `image_size_id` IS NULL', array(':t1' => $user_id));
                if (!empty($image))
                    $image_path = Users::model()->getDir($user_id) . DS . $image;
            }
        }
        if (empty($image) || !file_exists($image_path))
            $image_path = $this->getDefaultProfileImagePath();

        if (!file_exists($image_path))
            return $default_src;
        return Yii::app()->assetManager->publish($image_path);
    }

}
