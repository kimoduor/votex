<?php

/**
 * This is the model class for table "user_levels".
 *
 * The followings are the available columns in table 'user_levels':
 * @property string $id
 *  @property string $description
 * @property string $banned_resources
 * @property string $banned_resources_inheritance
 * @property integer $rank
 */
class UserLevels extends ActiveRecord implements IMyActiveSearch {

    const LEVEL_ENGINEER = '-1';
    const LEVEL_SUPERADMIN = '1';
    const LEVEL_ADMIN = '2';
    const LEVEL_AGRODEALER = '3';
    const LEVEL_SUPPLIER = '4';
    const LEVEL_DISTRICTME = '5';
    const LEVEL_PROVINCEME = '6';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return UserLevels the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'user_levels';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        return array(
            array('id,description,rank', 'required'),
            array('id', 'length', 'max' => 255),
            array('id,description', 'unique', 'message' => '{value} already exists.'),
            array('banned_resources,banned_resources_inheritance', 'safe'),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        return array(
            'users' => array(self::HAS_MANY, 'Users', 'user_level'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Level ID',
            'description' => 'Level Name',
            'rank' => 'Rank',
            'banned_resources' => 'Banned Resources',
            'banned_resources_inheritance' => 'Parent',
        );
    }

    public function searchParams() {
        return array(
            array('description', self::SEARCH_FIELD, true, 'OR'),
            array('id', self::SEARCH_FIELD, true, 'OR'),
        );
    }

    public function beforeSave() {
        if ($this->id === self::LEVEL_ENGINEER) {
            $this->banned_resources_inheritance = NULL;
            $this->banned_resources = NULL;
        }

        return parent::beforeSave();
    }

    /**
     *
     * @param type $level_id
     * @return type
     */
    public static function decodeUserLevel($level_id) {
        $level = '';
        switch ($level_id) {
            case self::LEVEL_ENGINEER:
                $level = Lang::t('Developer');
                break;
            case self::LEVEL_SUPERADMIN:
                $level = Lang::t('Super Admin');
                break;
            case self::LEVEL_AGRODEALER:
                $level = Lang::t('Agro Dealer');
                break;
            case self::LEVEL_SUPPLIER:
                $level = Lang::t('supplier');
                break;
            case self::LEVEL_DISTRICTME:
                $level = Lang::t('districtme');
                break;
             case self::LEVEL_PROVINCEME:
                $level = Lang::t('province monitor');
                break;
        }

        return $level;
    }

}
