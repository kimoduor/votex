<?php

/**
 * @author Joakim <kimoduor@gmail.com>
 * Parent controller for the users module
 */
class VotexModuleController extends Controller {

        public function init() {
                parent::init();
        }

        public function setModulePackage() {

                $this->module_package = array(
                    'baseUrl' => $this->module_assets_url,
                    'js' => array(
                       // 'js/module.js',
                    ),
                    'css' => array(
                    ),
                );
        }

}
