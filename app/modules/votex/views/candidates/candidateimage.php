<?php
$candidate_id = 0;
$candidate_idy = 0;
$candidate_idx = "";
$yz = 1;
$urlheader = @$_SERVER['HTTP_REFERER'];

if (strpos($urlheader, "?")) {
    $candidate_idx = substr($urlheader, strpos($urlheader, "?") + 1);
    $candidate_idy = substr($candidate_idx, strpos($candidate_idx, "//") + 2);
    $yz = substr($candidate_idx, strpos($candidate_idx, "yz") + 2, strpos($candidate_idx, "xx") - 2);
}
if ($candidate_idy > 0) {
    $candidate_id = $candidate_idy;
}
$model->key = $yz > 0 ? $yz : $model->key;

$model = Candidates::model()->loadModel($candidate_id);


//determine if teh pupil has a google location if not, your location is the default
?>
<style>
    .portlet-body{
        display: none;
    }
    #body<?php echo $model->key; ?>{
        display: block; 
    }
    .datepicker,.table-condensed { width: 30em; font-size: 14px; }
</style>


<div class="row">
    <div class="col-md-12">
    
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box yellow-gold">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Passport size Photo
                </div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="collapseicon">
                    </a>

                </div>
            </div>
            <style type="text/css">
                .myimage {width:200px;
                          margin: 20px;
                          box-shadow:#ccc 1px;

                }
                legend{
                    font-size: 14px;
                }

            </style>
            <div class="portlet-body form" id="body2">       
                <fieldset>

                    <div id="front-end" style="float:left; padding: 40px;">
                        <form action="votex/candidates/updateexternal" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="Candidates[candidate_id]" value="<?php echo $model->candidate_id; ?>">
                            Select Passport size Photo  
                            <br />
                            <img id="uploadPreview" src="<?php echo $model->passport ? "public/candidateimage/$model->passport" : "malt/img/default.jpg"; ?>" class="myimage" /><br />
                            <input type="file" name="uploadImage" id="uploadImage" onchange="PreviewImage()">
                            <br />  <input type="submit" class="btn blue-madison" value="Upload Passport" name="submit">
                        </form>
                    </div>

                </fieldset>

                <script type="text/javascript">
                    function PreviewImage() {
                        var oFReader = new FileReader();
                        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
                        oFReader.onload = function(oFREvent) {
                            document.getElementById("uploadPreview").src = oFREvent.target.result;
                        };
                    }
                </script>

                <!-- END FORM-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>

    </div>
</div>
<!------------------------------------end passport-------------->
<script>
    $(document).ready(function(){
        $("#collapseicon").trigger("click");
    });
    </script>
