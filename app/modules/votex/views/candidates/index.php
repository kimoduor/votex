<div class="row">
    <div class="col-md-12">
        <div class="portlet box blue-madison">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-briefcase"></i>Candidates</div>
                <div class="tools">
                    <a href="javascript:;" class="expand" id="widget1">
                    </a>
                </div>
            </div>


            <div class="portlet-body form" id="body2">
                <div style="margin-left:20px">
                    <table id="jqGrid"></table>
                    <div id="jqGridPager"></div>
                </div> 
            </div>


        </div>

    </div>
</div>


<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>

<script type="text/javascript">
    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div> First Name: </div><div>{topic} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/candidates',
            datatype: "local",
            rownumbers: true,
            toppager: true, cloneToTop: true,
            colModel: [
                {
                    label: '#',
                    name: 'candidate_id',
                    hidden: true,
                    width: 8,
                    key: true,
                    search: false,
                },
                {
                    label: 'First Name',
                    name: 'fname',
                    width: 100,
                    search: true,
                    editable: true,
                },
                 {
                    label: 'Last Name',
                    name: 'lname',
                    width: 100,
                    search: true,
                    editable: true,
                },
                  {
                    label: 'Code',
                    name: 'code',
                    width: 100,
                    search: true,
                    editable: true,
                },
                {label: 'Post',
                    name: 'post_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php  echo Details::postsjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::postsjson(); ?>},
                    width: 200,
                   
                },
               
              
            ],
            sortname: 'post_id',
            sortorder: 'asc',
            loadonce: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            pager: "#jqGridPager",
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                        {edit: true, add: true, del: true, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
                {url: "votex/candidates/update", afterSubmit: processAddEdit, closeAfterEdit: true},
                {url: "votex/candidates/create", afterSubmit: processAddEdit, closeAfterAdd: true},
                {url: "votex/candidates/delete"},
                {// reload
                    reloadAfterSubmit: true
                },
                // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }

                }).navButtonAdd('#jqGridPager', {
            caption: "",
            toppager: true,
            cloneToTop: true,
            buttonicon: "glyphicon glyphicon-pencil",
            onClickButton: function() {
                try {
                    var rowId = $("#jqGrid").jqGrid('getGridParam', 'selrow');
                    var rowData = $("#jqGrid").getRowData(rowId);
                    var colData = rowData['candidate_id'];
                    if (colData == undefined) {
                        alert("Kindly select the Record to View");
                    }
                    else {
                        //  window.location.href = "#/requestloan?l=" + colData;
                        window.location.href = "?<?php echo md5(date("Y-m-d H:s")); ?>1ssd787hj//" + colData + "#/candidate";
                    }

                } catch (e) {

                }
            },
        });
                

                $('#jqGrid').jqGrid('filterToolbar', {
                    // JSON stringify all data from search, including search toolbar operators
                    stringResult: true,
                    // instuct the grid toolbar to show the search options
                    searchOperators: false,
                });
            });
 
   

</script>

