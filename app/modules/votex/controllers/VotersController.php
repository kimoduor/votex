<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class VotersController extends VotexModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('create'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('index', 'delete', 'update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionCreate() {
        $errorlist = "";
        //start by saving the user details
        $model = new Voters();
        if (isset($_POST)) {
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->idno = $_POST['idno'];
            $model->pin = $_POST['pin'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }


    public function actionUpdate() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Voters::model()->loadModel($id);
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->idno = $_POST['idno'];
            $model->pin = $_POST['pin'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Voters::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
