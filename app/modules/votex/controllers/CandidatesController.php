<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class CandidatesController extends VotexModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('create'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('index', 'delete', 'update' , 'candidateimage','updateexternal'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionCreate() {
        $errorlist = "";
        //start by saving the user details
        $model = new Candidates();
        if (isset($_POST)) {
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->code = $_POST['code'];
            $model->post_id = $_POST['post_id'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Candidates::model()->loadModel($id);
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->code = $_POST['code'];
            $model->post_id = $_POST['post_id'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Candidates::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }
    public function actionCandidateimage() {
        $this->render('candidateimage', array('model' => Candidates::model()->searchModel(array(), 10, 'candidate_id')));
    }
    public function actionUpdateexternal() {
        // if (isset($_POST['pupil_id'])) {
        $model = Candidates::model()->loadModel($_POST['Candidates']['candidate_id']);
        $model_class_name = get_class($model);

        //--------------------------------upload passport
        $target_dir = getcwd() . "/public/candidateimage/";
        if (isset($_FILES["uploadImage"])) {
            $img = rand(0, 1000000) . "-" . basename($_FILES["uploadImage"]["name"]);
            $target_file = $target_dir . $img;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
            if (isset($_POST) && !empty($_FILES["uploadImage"]["tmp_name"])) {
                $check = getimagesize($_FILES["uploadImage"]["tmp_name"]);
                if ($check !== false) {
                    $uploadOk = 1;
                } else {
                    Yii::app()->session['error'] = "No file was Uploaded OR File is Not an Image";
                    $this->redirect("../../#/candidateimage", array('model' => $model));
                    $uploadOk = 0;
                }
            }
// Check file size
            if ($_FILES["uploadImage"]["size"] > 1500000) {
                Yii::app()->session['error'] = "Sorry, the Image is too large";
                $this->redirect("../../#/candidateimage", array('model' => $model));
                $uploadOk = 0;
            }
// Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                Yii::app()->session['error'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $this->redirect("../../#/candidateimage", array('model' => $model));
                $uploadOk = 0;
            }
// Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                Yii::app()->session['error'] = "Sorry, your file was not uploaded.";
                $this->redirect("../../#/candidateimage", array('model' => $model));
            } else {//now upload
                if (move_uploaded_file($_FILES["uploadImage"]["tmp_name"], $target_file)) {
                    $model->passport = $img;
                }
            }
        }
        //---------------------------------finished uploading passport



        $model->save();

        $this->redirect("../../?yzxx" . md5(date("Y-m-d H:s")) . "//" . $model->candidate_id . "#/candidateimage");
        
    }
}
