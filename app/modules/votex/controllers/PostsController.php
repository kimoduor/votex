<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class PostsController extends VotexModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('create'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('index', 'delete', 'update'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionCreate() {
        $errorlist = "";
        //start by saving the user details
        $model = new Posts();
        if (isset($_POST)) {
            $model->postname = $_POST['postname'];
            $model->code = $_POST['code'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }


    public function actionUpdate() {

        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Posts::model()->loadModel($id);
            $model->postname = $_POST['postname'];
            $model->code = $_POST['code'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Posts::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
