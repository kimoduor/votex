<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class OzekiController extends VotexModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array(),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $newmessages = Ozekimessagein::model()->findAll(array('condition' => "processed=0", 'limit' => "5"));
        foreach ($newmessages as $newmessage) {
            $msg = strtoupper($newmessage->msg);
            //check if the user is registered{find the pin} V{vote}*12{post}*3344{candidate}*2247{pin}
            $pin = self::getpin($msg);
            $voter_id = Voters::model()->getScalar("voter_id", "pin='$pin'");
            if (!$voter_id) {
                $receiver = $newmessage->sender;
                $message = "REF:" . $newmessage->reference . " You seem Not to have Registered as a Voter. If you have then , you may"
                        . " have forgotten your PIN. Use Format V*postcode*Candidate*PIN to vote.";
                $reference = $newmessage->reference;
                self::sendsms($reference, $receiver, $message);
            } else {//meaning the pin can be found
                $array = explode("*", $msg);
                $post = @$array[1];
                $post_id = Posts::model()->getScalar("post_id", "code=$post");
                if (!$post_id) {
                    $receiver = $newmessage->sender;
                    $message = "REF:" . $newmessage->reference . ". Unknown Post code $post. Use Format V*postcode*Candidate*PIN to vote.";
                    $reference = $newmessage->reference;
                    self::sendsms($reference, $receiver, $message);
                } else {

                    switch (strtoupper(self::getop($msg))) {
                        case "V"://
                            $candidate = @$array[2];
                            $candidate_id = Candidates::model()->getScalar("candidate_id", "code=$candidate AND post_id=$post_id");
                            if (!$candidate_id) {
                                $receiver = $newmessage->sender;
                                $message = "REF:" . $newmessage->reference . ". Unknown Candidate code $candidate for the post,Kindly confirm and use Format V*postcode*Candidate*PIN to vote.";
                                $reference = $newmessage->reference;
                                self::sendsms($reference, $receiver, $message);
                            } else {//now you can vote
                                $votex_id = Votes::model()->getScalar("vote_id", "voter_id=$voter_id AND candidate_id IN"
                                        . " (SELECT candidate_id FROM candidates WHERE post_id='$post_id')");
                                if ($votex_id > 0) {//this guy has already voted for that post
                                    $votedcandidate_id = Votes::model()->get($votex_id, "candidate_id");
                                    $receiver = $newmessage->sender;
                                    $message = "REF:" . $newmessage->reference . "Sorry, You have already voted for  "
                                            . " " . Candidates::model()->get($votedcandidate_id, "CONCAT(fname,' ',lname)") . " "
                                            . " for " . Posts::model()->get($post_id, "postname");
                                    $reference = $newmessage->reference;
                                    self::sendsms($reference, $receiver, $message);
                                } else {
                                    $votesmodel = new Votes();
                                    $votesmodel->candidate_id = $candidate_id;
                                    $votesmodel->voter_id = $voter_id;
                                    $votesmodel->success = 1;
                                    $votesmodel->save();
                                    $receiver = $newmessage->sender;
                                    $message = "REF:" . $newmessage->reference . ". \n "
                                            . " SUCCESSFULLY Voted for "
                                            . " " . Candidates::model()->get($candidate_id, "CONCAT(fname,' ',lname)") . " "
                                            . " for " . Posts::model()->get($post_id, "postname");
                                    $reference = $newmessage->reference;
                                    self::sendsms($reference, $receiver, $message);
                                }
                            }
                            break;
                        case "R":
                            $totalvotes = Votes::model()->count("candidate_id IN (SELECT candidate_id FROM candidates"
                                    . " WHERE post_id='$post_id')");
                            $candidatevotestring = "";
                            $receiver = $newmessage->sender;
                            $allcandidatesinthatpost = Candidates::model()->findAll("post_id='$post_id'");

                            foreach ($allcandidatesinthatpost as $onecandidate) {
                                $candidatevotes = Votes::model()->count("candidate_id ='$onecandidate->candidate_id'");
                                $candidatevotestring = $candidatevotestring . " \n"
                                        . $onecandidate->fname . " " . $onecandidate->lname . " :" . $candidatevotes;
                            }
                           $postname= Posts::model()->get($post_id,"postname");
                            $message = "REF:" . $newmessage->reference . ". Total Votes Casted for $postname : $totalvotes \n."
                                    . " " . $candidatevotestring;
                            $reference = $newmessage->reference;
                            self::sendsms($reference, $receiver, $message);
                            break;
                    }
                }
            }
        }
        return true;
    

    }

    public static function getpin($string) {//find out when registered
        $apin = substr($string, strripos($string, "*") + 1);
        $pin = $apin;
        $lastdigit = substr($string, -1);
        if (!is_numeric($lastdigit)) {
            $pin = substr($apin, 0, -1);
        }
        return $pin;
    }

    public static function getop($string) {//find out when registered
        $op = substr($string, stripos($string, "*") - 1, 1);
        return $op;
    }

    public static function getoptried($string) {//find out when registered
        if (self::getop($string) == "R") {//tried to check results
            return "Kindly Use Format R*postcode*pin to check Results.e.g.R*12*2247 ";
        } elseif (self::getop($string) == "V") {
            return "Kindly Use Format V*postcode*Candidate code*pin to Vote. e.g.V*12*3344*2247 ";
        }
        return "Unknown Operation, Kindly start with V to vote, R to check Results";
    }

    public static function sendsms($reference, $receiver, $message) {
        $ozekioutmodel = new Ozekimessageout();
        $ozekioutmodel->sender = "VoteX";
        $ozekioutmodel->receiver = $receiver;
        $ozekioutmodel->msg = $message;
        $ozekioutmodel->reference = $reference;
        $ozekioutmodel->status = "send";
        $ozekioutmodel->save();
        //mark the processed message as 1
        $ozekiin_id = Ozekimessagein::model()->getScalar("id", "reference='$reference'");
        $modelozekin = Ozekimessagein::model()->loadModel($ozekiin_id);
        $modelozekin->processed = 1;
        $modelozekin->save();
    }

}
