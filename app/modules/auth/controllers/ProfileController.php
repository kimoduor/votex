<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class ProfileController extends AuthModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('My Profile');
        $this->resource = 'profile';
        $this->activeTab = 'profile';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'uploadfiles', 'changePassword'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        //$this->hasPrivilege(Acl::ACTION_UPDATE);
        $this->pageTitle = Lang::t('profile');

        $model = new Users();
        $model_class_name = get_class($model);
        $this->render('index', array('model' => $model));
    }

    public function actionUpdate($id) {
        Yii::app()->session['key']=1;
        $model = Users::model()->loadModel($id);
        $model_class_name = get_class($model);
        if (isset($_POST[$model_class_name])) {
            $model->attributes = $_POST[$model_class_name];

            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                $errorlist = '';
                foreach ($error_message_decoded as $error) {
                    $errorlist.=$error[0] . '<br />';
                }
                Yii::app()->session['error'] = $errorlist;
            } else {
                $model->save(FALSE);
                Yii::app()->session['success'] = Lang::t('SUCCESS_MESSAGE');
                $this->redirect(array('../#/profile'));
                Yii::app()->end();
            }
        }


        $this->redirect(array('../#/profile'));
    }

    public function actionDelete($id) {


        if (isset($_GET['confirmdelete'])) {
            try {
                Promotion::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

    public function actionUploadfiles() {
        Yii::app()->session['key'] = 2;
        $target_dir = getcwd() . "/public/idfore/";
        if (isset($_FILES["uploadImage"])) {
            $model = Users::model()->loadModel(Yii::app()->user->id);
            $img = rand(0, 1000000) . "-" . basename($_FILES["uploadImage"]["name"]);
            $target_file = $target_dir . $img;
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image
            if (isset($_POST) && !empty($_FILES["uploadImage"]["tmp_name"])) {
                $check = getimagesize($_FILES["uploadImage"]["tmp_name"]);
                if ($check !== false) {
                    $uploadOk = 1;
                } else {
                    Yii::app()->session['error'] = "No file was Uploaded OR File is Not an Image";
                    $this->redirect("../../#/profile", array('model' => $model));
                    $uploadOk = 0;
                }
            }
// Check file size
            if ($_FILES["uploadImage"]["size"] > 1500000) {
                Yii::app()->session['error'] = "Sorry, the Image is too large";
                $this->redirect("../../#/profile", array('model' => $model));
                $uploadOk = 0;
            }
// Allow certain file formats
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                Yii::app()->session['error'] = "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $this->redirect("../../#/profile", array('model' => $model));
                $uploadOk = 0;
            }
// Check if $uploadOk is set to 0 by an error
            if ($uploadOk == 0) {
                Yii::app()->session['error'] = "Sorry, your file was not uploaded.";
                $this->redirect("../../#/profile", array('model' => $model));
            } else {//now upload
                if (move_uploaded_file($_FILES["uploadImage"]["tmp_name"], $target_file)) {
                    $model->photo_url = $img;
                    $model->save();
                    Yii::app()->session['success'] = "Your File was successfully Uploaded.";
                    $this->redirect("../../#/profile", array('model' => $model));
                } else {
                    Yii::app()->session['error'] = "Sorry, your file was not uploaded.";
                    $this->redirect("../../#/profile", array('model' => $model));
                }
            }
        }
    }

    public function actionChangePassword() {
         Yii::app()->session['key']=3;
        $id = Yii::app()->user->id;
        $username = Yii::app()->user->username;
        $this->pageTitle = Lang::t('Change your password');

        $model = Users::model()->loadModel($id);
        $model->setScenario(Users::SCENARIO_CHANGE_PASSWORD);
        $model->pass = $model->password;
        $model->password = null;
        $model_class_name = $model->getClassName();
        if (isset($_POST[$model_class_name])) {
            $username = Yii::app()->user->username;
            $model->attributes = $_POST[$model_class_name];
            $currentPass = $model->currentPassword;

            $error_message = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error_message);
            if (!empty($error_message_decoded)) {
                $errorlist = '';
                foreach ($error_message_decoded as $error) {
                    $errorlist.=$error[0] . '<br />';
                }
                Yii::app()->session['error'] = $errorlist;
            } else {
                $model->save(FALSE);
                Yii::app()->session['success'] = Lang::t('SUCCESS_MESSAGE');
                $this->redirect(array('../#/profile'));
                Yii::app()->end();
            }

            $this->redirect(array('../#/profile'));
        }
    }

}
