<?php
$model = Users::model()->loadModel(Yii::app()->user->id);
if (isset(Yii::app()->session['key'])) {
    $model->key = Yii::app()->session['key'];
} 
?>
<style>
    .portlet-body{
        display: none;
    }
    #body<?php echo $model->key; ?>{
        display: block; 
    }
</style>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box green">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-university"></i>Basic Information &nbsp;&nbsp;&nbsp;&nbsp; User email: <?php echo $model->username; ?>
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"  onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <div class="portlet-body form" id="body1">
                <form action="auth/profile/update?id=<?php echo Yii::app()->user->id; ?>" id="form-username" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="id" value="<?php echo $model->id; ?>">

                    <div class="form-group">
                        <label class="col-sm-1 control-label">First Name</label>     
                        <div class="col-sm-3">
                            <input type="text" id="fname" name="Users[fname]" value="<?php echo $model->fname; ?>" class="form-control"/>
                        </div>
                        <label class="col-sm-1 control-label">Last Name</label>     
                        <div class="col-sm-3">
                            <input type="text" id="lname" name="Users[lname]" value="<?php echo $model->lname; ?>" class="form-control"/>
                        </div>
                        <label class="col-sm-1 control-label">Other Name</label>     
                        <div class="col-sm-3">
                            <input type="text" id="othername" name="Users[othername]" value="<?php echo $model->othername; ?>" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">mobile</label>
                        <div class="col-sm-4">

                            <input type="text" id="mobile" name="Users[phone]" value="<?php echo $model->phone; ?>" class="form-control"/>

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-4">                            
                            <input type="text" id="email" name="Users[email]" value="<?php echo $model->email; ?>" class="form-control"/>
                        </div>


                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>

<!---------------------------------hone details--------------------------->






<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box yellow-gold">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Passport size Photo
                </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse" onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <style type="text/css">
                .myimage {width:200px;
                          margin: 20px;
                          box-shadow:#ccc 1px;

                }
                legend{
                    font-size: 14px;
                }

            </style>
            <div class="portlet-body form" id="body2">       
                <fieldset>

                    <div id="front-end" style="float:left; padding: 40px;">
                        <form action="auth/profile/uploadfiles" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo $model->id; ?>">
                            Select Passport size Photo  
                            <br />
                            <img id="uploadPreview" src="<?php echo $model->photo_url ? "public/idfore/$model->photo_url" : "malt/img/default.jpg"; ?>" class="myimage" /><br />
                            <input type="file" name="uploadImage" id="uploadImage" onchange="PreviewImage()">
                            <br />  <input type="submit" class="btn blue-madison" value="Upload Passport" name="submit">
                        </form>
                    </div>

                </fieldset>

                <script type="text/javascript">
                    function PreviewImage() {
                        var oFReader = new FileReader();
                        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);
                        oFReader.onload = function(oFREvent) {
                            document.getElementById("uploadPreview").src = oFREvent.target.result;
                        };
                    }
                </script>

                <!-- END FORM-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>


<!--passworsa-->



<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet box blue-madison">
            <div class="portlet-title">
                <div class="caption"><i class="fa fa-lock"></i>Change Password </div>
                <div class="tools">
                    <a href="javascript:;" class="collapse"  onclick="widgetclicked()">
                    </a>

                </div>
            </div>
            <div class="portlet-body form" id="body3">
                <form action="auth/profile/changePassword" class="form-horizontal form-bordered" method="post">
                    <input type="hidden" name="id" value="<?php echo $model->id; ?>">


                    <div class="form-group">
                        <label class="col-sm-2 control-label">Old Password</label>
                        <div class="col-sm-4">
                            <input type="password" id="oldemail" name="Users[currentPassword]" value="" class="form-control"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">New Password</label>
                        <div class="col-sm-4">                            
                            <input type="password" id="newpassword" name="Users[password]" value="" class="form-control"/>
                        </div>
                        <div class="col-sm-4">  
                            <div class="progress progress-sm" style="height:20px">
                                <div class="progress-bar progress-bar-danger" id="percent" role="progressbar">
                                    <span id="passwordprogress"></span>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="form-group">

                        <label class="col-sm-2 control-label">Confirm Password</label>
                        <div class="col-sm-4">                            
                            <input type="password" id="confirmpassword" name="Users[confirm]" value="" class="form-control"/>
                        </div>
                        <div class="col-sm-4" >  
                            <a id="signhref" class="btn btn-circle btn-icon-only btn-danger" href="#" >
                                <i id="sign" class="fa fa-close"></i>
                            </a>
                        </div>


                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button id="changepasswrd" disabled="" type="submit" class="btn green"><i class="fa fa-check"></i> Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </div>
                    </div>
                </form>

            </div>
        </div>

    </div>
</div>
<script>
    function widgetclicked() {
        $('.portlet-body').hide();
        $('.collapse').addClass("expand");
        $('.expand').removeClass("collapse");
        $(this).addClass("collapse");
        $(this).removeClass("expand");
    }
    var cd=false;
    $('#newpassword').keyup(function() {
        var status = "";
        var newpass = $(this).val();
        if (newpass.length < 5) {
            $('#percent').addClass('progress-bar-danger');
            $('#percent').removeClass('progress-bar-striped');
            $('#percent').removeClass('progress-bar-warning');
            var newval = ((newpass.length) * 10);
            status = "Weak";
            cd=false;
            $('#percent').css({'width': newval + '%'});
        }
        else {
            status = "Fair";
            $('#percent').removeClass('progress-bar-danger');
            $('#percent').removeClass('progress-bar-striped');
            $('#percent').addClass('progress-bar-warning');
            $('#percent').css({'width': 68 + '%'});
            cd=true;

            var matches = newpass.match(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
            if (matches != null) {
                status = "Strong";
                $('#percent').removeClass('progress-bar-danger');
                $('#percent').removeClass('progress-bar-warning');
                $('#percent').addClass('progress-bar-striped');
                cd=true;
                $('#percent').css({'width': 98 + '%'});
                if (newpass.length > 8) {
                    $('#percent').css({'width':100+'%'});
                }
            }
        }

        $('#passwordprogress').text(status);
        $('#confirmpassword').trigger('keyup');
    });
    $('#confirmpassword').keyup(function() {
        if ($('#newpassword').val() == $('#confirmpassword').val()) {
            $('#sign').removeClass('fa-close');
            $('#sign').addClass('fa-check');
            $('#signhref').removeClass('btn-danger');
            $('#signhref').addClass('btn-success');
            if(cd==true){
              $('#changepasswrd').prop('disabled',false);
          }
            
        }
        else {
            $('#sign').addClass('fa-close');
            $('#sign').removeClass('fa-check');
            $('#signhref').addClass('btn-danger');
            $('#signhref').removeClass('btn-success');
            $('#changepasswrd').prop('disabled',true);
        }

    });
</script>

