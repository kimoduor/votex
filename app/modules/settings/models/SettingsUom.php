<?php

/**
 * This is the model class for table "settings_uom".
 *
 * The followings are the available columns in table 'settings_uom':
 * @property string $id
 * @property string $unit
 * @property string $description
 * @property string $date_created
 * @property string $created_by
 */
class SettingsUom extends ActiveRecord implements IMyActiveSearch
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_uom';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('unit', 'required'),
            array('unit', 'length', 'max' => 20),
            array('description', 'length', 'max' => 255),
            array('unit', 'unique', 'message' => Lang::t('{value} already exists.')),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'unit' => Lang::t('Unit'),
            'description' => Lang::t('Description'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SettingsUom the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function searchParams()
    {
        return array(
            array('unit', self::SEARCH_FIELD, true),
        );
    }

}
