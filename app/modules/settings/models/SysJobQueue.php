<?php

/**
 * This is the model class for table "sys_job_queue".
 *
 * The followings are the available columns in table 'sys_job_queue':
 * @property string $id
 * @property string $params
 * @property string $job_id
 * @property string $date_created
 * @property integer $max_attempts
 * @property integer $attempts
 * @property string $pop_key
 */
class SysJobQueue extends ActiveRecord
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sys_job_queue';
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SysJobQueue the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get queued jobs
     * @param type $job_id
     * @return type
     */
    public function getQueue($job_id)
    {
        $pop_key = MyYiiUtils::uuid();
        $limit = 10;

        //update the queue
        $sql = "UPDATE `{$this->tableName()}` SET `pop_key`=:pop_key WHERE `job_id`=:job_id AND `pop_key` IS NULL LIMIT {$limit}";
        Yii::app()->db->createCommand($sql)
                ->bindValues(array(':job_id' => $job_id, ':pop_key' => $pop_key))
                ->execute();
        //fetch the updated values
        $condition = '`job_id`=:job_id AND `pop_key`=:pop_key';
        $params = array(':job_id' => $job_id, ':pop_key' => $pop_key);
        $queue = $this->getData('*', $condition, $params, 'id', $limit);
        //delete the fetched
        if (!empty($queue)) {
            Yii::app()->db->createCommand()
                    ->delete($this->tableName(), '`pop_key`=:t1 AND `job_id`=:t2', array(':t1' => $pop_key, ':t2' => $job_id));
        }
        return $queue;
    }

    /**
     * Push a job to a queue
     * @param type $job_id
     * @param type $params
     * @param type $max_attempts
     * @return type
     */
    public function push($job_id, $params, $max_attempts = 1)
    {
        $values = array(
            'params' => serialize($params),
            'job_id' => $job_id,
            'max_attempts' => $max_attempts,
            'owner_id' => Yii::app() instanceof CWebApplication ? Yii::app()->user->id : NULL,
        );
        Yii::app()->db->createCommand()
                ->insert($this->tableName(), $values);
        return $values;
    }

    /**
     * Increment attempts incase of a failure
     * @param array $queue
     */
    public function incrementAttempts($queue)
    {
        $values = array(
            'id' => $queue['id'],
            'params' => $queue['params'],
            'job_id' => $queue['job_id'],
            'attempts' => $queue['attempts'] + 1,
            'owner_id' => Yii::app() instanceof CWebApplication ? Yii::app()->user->id : NULL,
        );
        Yii::app()->db->createCommand()
                ->insert($this->tableName(), $values);
    }

}
