<?php

/**
 * This is the model class for table "settings_modules_enabled".
 *
 * The followings are the available columns in table 'settings_modules_enabled':
 * @property string $id
 * @property string $name
 * @property string $description
 * @property string $is_active
 * @property string $date_created
 * @property integer $created_by
 */
class ModulesEnabled extends ActiveRecord implements IMyActiveSearch
{

    const STATUS_ENABLED = 'Enabled';
    const STATUS_DISABLED = 'Disabled';
    //modules
    const MOD_SURVEY = 'survey';
    const MOD_CLIENT = 'client';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_modules_enabled';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        return array(
            array('id, name', 'required'),
            array('id, name', 'length', 'max' => 30),
            array('description', 'length', 'max' => 255),
            array('is_active', 'safe'),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('Module ID'),
            'name' => Lang::t('Module Name'),
            'description' => Lang::t('Description'),
            'is_active' => Lang::t('Active'),
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ModulesEnabled the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function searchParams()
    {
        return array(
            array('name', self::SEARCH_FIELD, true),
            'is_active',
        );
    }

    /**
     * Check if module is enabled
     * @param type $id
     * @return type
     */
    public function isModuleEnabled($id)
    {
        return $this->exists('`id`=:id AND `is_active`=1', array(':id' => $id));
    }

}
