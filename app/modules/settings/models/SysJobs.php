<?php

/**
 * This is the model class for table "sys_jobs".
 *
 * The followings are the available columns in table 'sys_jobs':
 * @property string $id
 * @property string $last_run
 * @property string $is_active
 * @property integer $threads
 * @property integer $max_threads
 * @property integer $sleep
 */
class SysJobs extends ActiveRecord
{

    //A job times out if last run in seconds or more
    const TIMEOUT_SECS = 300;
    //job constants
    const JOB_SEND_EMAIL = 'sendEmail';
    const JOB_CLEAN_UP = 'cleanUp';
    //execution types
    const EXEC_TYPE_CRON = '1';
    const EXEC_TYPE_DAEMON = '2';

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'sys_jobs';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        return array(
            array('id', 'required'),
            array('threads, max_threads,sleep', 'numerical', 'integerOnly' => true),
            array('id', 'length', 'max' => 30),
            array('id', 'unique', 'message' => Lang::t('{attribute} ({value}) already exists.')),
            array('last_run,execution_type,is_active', 'safe'),
            array(self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'queue' => array(self::HAS_MANY, 'SysJobQueue', 'job_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Job ID',
            'last_run' => 'Last Run',
            'is_active' => 'Active',
            'threads' => 'Processes',
            'max_threads' => 'Max Processes',
            'sleep' => 'Sleep (in Sec.)',
            'execution_type' => 'Type',
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SysJobs the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Sets that status of the job as active so that it executes when the cron job runs
     * @param type $job_id
     */
    public function startJob($job_id)
    {
        Yii::app()->db->createCommand()
                ->update($this->tableName(), array('is_active' => 1), '`id`=:id', array(':id' => $job_id));
    }

    /**
     * Stop a job from executing
     * @param type $job_id
     */
    public function stopJob($job_id)
    {
        Yii::app()->db->createCommand()
                ->update($this->tableName(), array('is_active' => 0, 'threads' => 0, 'last_run' => null), '`id`=:id', array(':id' => $job_id));

        SysJobProcesses::model()->deleteAll('`job_id`=:t1', array(':t1' => $job_id));
    }

    /**
     * prepare Yii command param string e.g "--id=3 --name=Fred --interactive=1"
     * @param array $params key=>vaue pair of the params
     * @return string
     */
    protected function prepareYiiCommandParams(array $params)
    {
        $params_string = '';
        $params['interactive'] = 1;

        foreach ($params as $k => $v) {
            $param = '--' . $k . '=' . $v;
            if (!empty($params_string))
                $params_string.=' ';
            $params_string.=$param;
        }

        return $params_string;
    }

    /**
     * Check whether a job has timed out
     * @param type $job_id
     * @return boolean
     */
    public function hasTimedOut($job_id)
    {
        $sql = "SELECT (UNIX_TIMESTAMP(NOW())-UNIX_TIMESTAMP(`last_run`)) as `last_run`,`sleep` FROM `{$this->tableName()}` WHERE `id`=:id";
        $row = Yii::app()->db->createCommand($sql)
                ->bindValue(':id', $job_id)
                ->queryRow();
        if (empty($row))
            return TRUE;
        if ((int) $row['last_run'] >= (self::TIMEOUT_SECS + (int) $row['sleep'])) {
            return TRUE;
        }
        return FALSE;
    }

    /**
     *
     * @param type $job_id
     */
    public function updateLastRun($job_id)
    {
        Yii::app()->db->createCommand()
                ->update($this->tableName(), array('last_run' => new CDbExpression('NOW()')), '`id`=:id', array(':id' => $job_id));
    }

    public function dateTimeFields()
    {
        return array_merge(parent::dateTimeFields(), array(
            'last_run',
        ));
    }

    /**
     *
     * @return type
     */
    public static function executionTypeOptions()
    {
        return array(
            self::EXEC_TYPE_CRON => self::decodeExecutionType(self::EXEC_TYPE_CRON),
            self::EXEC_TYPE_DAEMON => self::decodeExecutionType(self::EXEC_TYPE_DAEMON),
        );
    }

    /**
     *
     * @param type $exec_type
     * @return type
     */
    public static function decodeExecutionType($exec_type)
    {
        $decoded = "";
        switch ($exec_type) {
            case self::EXEC_TYPE_CRON:
                $decoded = Lang::t('Cron Job');
                break;
            case self::EXEC_TYPE_DAEMON:
                $decoded = Lang::t('Daemon');
                break;
        }

        return $decoded;
    }

}
