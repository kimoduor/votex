<?php

/**
 * This is the model class for table "settings_email_template".
 *
 * The followings are the available columns in table 'settings_email_template':
 * @property integer $id
 * @property string $description
 * @property string $subject
 * @property string $body
 * @property string $sender
 * @property string $comments
 * @property string $date_created
 * @property integer $created_by
 */
class SettingsEmailTemplate extends ActiveRecord implements IMyActiveSearch
{

    //email template ids

    const ID_FORGOT_PASSWORD = 'forgot_password';
    const ID_RESET_PASSWORD = 'admin_reset_password';
    const ID_NEW_USER = 'new_user';
    const ID_ACCOUNT_ACTIVATION = 'account_activation';

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SettingsEmailTemplate the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'settings_email_template';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {

        return array(
            array('id,description, subject, body, sender', 'required'),
            array('created_by', 'numerical', 'integerOnly' => true),
            array('id,description', 'length', 'max' => 128),
            array('sender', 'email'),
            array('subject, sender', 'length', 'max' => 255),
            array('comments', 'safe'),
            array('id', 'unique', 'message' => Lang::t('{value} already exists.')),
            array('id,' . self::SEARCH_FIELD, 'safe', 'on' => self::SCENARIO_SEARCH),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Lang::t('ID'),
            'subject' => Lang::t('Subject'),
            'body' => Lang::t('Body'),
            'sender' => Lang::t('From'),
            'description' => Lang::t('Description'),
        );
    }

    public function searchParams()
    {
        return array(
            array('id', self::SEARCH_FIELD, true, 'OR'),
            array('description', self::SEARCH_FIELD, true, 'OR'),
            'id',
        );
    }

}
