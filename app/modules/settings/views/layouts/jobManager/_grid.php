<?php

$grid_id = 'queue-tasks-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => Common::pluralize($this->resourceLabel),
    'titleIcon' => '<i class="fa fa-tasks"></i>',
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible' => $this->showLink($this->resource, Acl::ACTION_CREATE), 'modal' => true),
    'toolbarButtons' => array(),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '!$data->is_active?"bg-danger":""',
        'columns' => array(
            'id',
            array(
                'name' => 'execution_type',
                'value' => 'SysJobs::decodeExecutionType($data->execution_type)',
            ),
            array(
                'name' => 'last_run',
                'value' => 'MyYiiUtils::formatDate($data->last_run,"d/m/Y H:i:s")',
            ),
            array(
                'name' => 'threads',
                'value' => '"<span class=\'label label-default\'>".$data->threads ."/".$data->max_threads. "</span>&nbsp;<span class=\'label label-success\'> R: " . SysJobProcesses::model()->getTotalRunning($data->id) . "</span>&nbsp;<span class=\'label label-danger\'> S: " . SysJobProcesses::model()->getTotalSleeping($data->id) . "</span>"',
                'type' => 'raw',
                'htmlOptions' => array('style' => 'min-width:180px;'),
            ),
            'sleep',
            array(
                'name' => 'is_active',
                'value' => 'MyYiiUtils::decodeBoolean($data->is_active)',
            ),
            array(
                'class' => 'ButtonColumn',
                'template' => '{start}{stop}{view}{update}{delete}',
                'htmlOptions' => array('style' => 'min-width: 150px;'),
                'buttons' => array(
                    'view' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-eye fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("view",array("id"=>$data->id))',
                        'options' => array(
                            'title' => Lang::t('View Processes'),
                        ),
                    ),
                    'start' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-check fa-2x text-success"></i>',
                        'url' => 'Yii::app()->controller->createUrl("start",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '") && !$data->is_active?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Start the process'),
                        ),
                    ),
                    'stop' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-ban fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("stop",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '") && $data->is_active?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-confirm' => false,
                            'data-grid_id' => $grid_id,
                            'class' => 'my-update-grid',
                            'title' => Lang::t('Stop all processes'),
                        ),
                    ),
                    'update' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-edit fa-2x"></i>',
                        'url' => 'Yii::app()->controller->createUrl("update",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '","' . Acl::ACTION_UPDATE . '")?true:false',
                        'options' => array(
                            'class' => 'show_modal_form',
                            'title' => Lang::t(Constants::LABEL_UPDATE),
                        ),
                    ),
                    'delete' => array(
                        'imageUrl' => false,
                        'label' => '<i class="fa fa-trash-o fa-2x text-danger"></i>',
                        'url' => 'Yii::app()->controller->createUrl("delete",array("id"=>$data->id))',
                        'visible' => '$this->grid->owner->showLink("' . $this->resource . '", "' . Acl::ACTION_DELETE . '")&& $data->canDelete()?true:false',
                        'url_attribute' => 'data-ajax-url',
                        'options' => array(
                            'data-grid_id' => $grid_id,
                            'data-confirm' => Lang::t('DELETE_CONFIRM'),
                            'class' => 'delete my-update-grid',
                            'title' => Lang::t(Constants::LABEL_DELETE),
                        ),
                    ),
                )
            ),
        ),
    )
));
?>  