<?php

$grid_id = 'job-processes-grid';
$this->widget('ext.MyGridView.ShowGrid', array(
    'title' => $this->pageTitle,
    'titleIcon' => null,
    'showExportButton' => false,
    'showSearch' => false,
    'createButton' => array('visible' => false),
    'toolbarButtons' => array(
        CHtml::tag('span', array('class' => 'well well-sm well-light'), Lang::t('Running: {running}, Sleepng {sleeping}', array('{running}' => SysJobProcesses::model()->getTotalRunning($model->job_id), '{sleeping}' => SysJobProcesses::model()->getTotalSleeping($model->job_id)))),
    ),
    'showRefreshButton' => true,
    'grid' => array(
        'id' => $grid_id,
        'model' => $model,
        'rowCssClassExpression' => '$data->status==="' . SysJobProcesses::STATUS_RUNNING . '"?"bg-success":"bg-danger"',
        'columns' => array(
            'id',
            array(
                'name' => 'status',
                'value' => 'SysJobProcesses::decodeStatus($data->status)',
            ),
            array(
                'name' => 'date_created',
                'value' => 'MyYiiUtils::formatDate($data->date_created,"d/m/Y H:i:s")',
            ),
            array(
                'name' => 'last_run_datetime',
                'value' => 'MyYiiUtils::formatDate($data->last_run_datetime,"d/m/Y H:i:s")',
            ),
        ),
    )
));
?>
