<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class PapertypesController extends SettingsModuleController {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $model = new Levels();
        $this->render('index', array('model' => $model));
    }

    public function actionCreate() {
        if (isset($_POST)) {
            $model = new Papertypes();
            $model->papertype = $_POST['papertype'];
            $model->rate = $_POST['rate'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Papertypes::model()->loadModel($id);
            $model->papertype = $_POST['papertype'];
            $model->rate = $_POST['rate'];
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Papertypes::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
