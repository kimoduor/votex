<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class EmailsController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('Emails');
        // $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'Emails';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('index', 'create', 'update', 'delete', 'data'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionIndex() {
        $this->pageTitle = Lang::t('Emails');

        $model = new Emails();
        $model_class_name = get_class($model);
        //$this->render('index', array('model' => $model));
        $condition = '';
        $this->render('index', array('model' => Emails::model()->searchModel(array(), 10, 'route_id', $condition)));
    }

    public function actionCreate() {
        $this->pageTitle = Lang::t('Emails');
        if (isset($_POST)) {
            $model = new Emails();
            $model->email = $_POST['email'];
            $model->email_category = $_POST['email_category'];
            
            $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                  echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionUpdate() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $model = Emails::model()->loadModel($id);
            $model->email = $_POST['email'];
            $model->email_category = $_POST['email_category'];
            
          $error = CActiveForm::validate($model);
            $error_message_decoded = CJSON::decode($error);
            if (empty($error_message_decoded)) {
                $model->save();
                  echo json_encode(true);
            } else {
                echo $error;
            }
        }
    }

    public function actionDelete() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];

            try {
                Emails::model()->loadModel($id)->delete();
                Yii::app()->user->setFlash('success', Lang::t("Successfully Deleted"));
            } catch (Exception $e) {
                Yii::app()->user->setFlash('error', Lang::t("Sorry, This record cannot be deleted, it is being used elsewhere "));
            }
        }
    }

}
