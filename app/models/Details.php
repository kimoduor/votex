<?php

class Details {

    public static function usersjson() {
//class
        $modelx = Users::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $role = UserRoles::model()->get($modely->role_id, "name");
            $output[$modely->id] = $modely->fname . " [$role]";
        }
        return json_encode($output);
    }

    public static function clientsjson() {
//class
        $modelx = Users::model()->findAll("role_id=4");
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $role = UserRoles::model()->get($modely->role_id, "name");
            $output[$modely->id] = $modely->fname . " " . $modely->lname . "-" . $modely->email;
        }
        return json_encode($output);
    }

    public static function postsjson() {
        $modelx = Posts::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $output[$modely->post_id] = $modely->postname;
        }
        return json_encode($output);
    }
    public static function candidatejson() {
        $modelx = Candidates::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $output[$modely->candidate_id] = $modely->fname." ".$modely->lname;
        }
        return json_encode($output);
    }
        public static function votersjson() {
        $modelx = Voters::model()->findAll();
        $output = array();
        $output[""] = "[Select One]";
        foreach ($modelx as $modely) {
            $output[$modely->voter_id] = $modely->fname." ".$modely->lname;
        }
        return json_encode($output);
    }
    

}
