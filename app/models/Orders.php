<?php

/**
 * This is the model class for table "orders".
 *
 * The followings are the available columns in table 'orders':
 * @property string $order_id
 * @property integer $words
 * @property string $client_id
 * @property string $dateordered
 * @property integer $timeframe_id
 * @property string $deadline
 * @property string $topic
 * @property string $writer_id
 * @property string $level_id
 * @property string $papertype_id
 * @property string $instructions
 * @property string $files
 * @property double $amountfromclient
 * @property double $amounttowriter
 * @property string $style_id
 * @property integer $payment_status
 * @property integer $orderstatus
 */
class Orders extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'orders';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('client_id', 'required'),
			array('words, timeframe_id, payment_status, orderstatus', 'numerical', 'integerOnly'=>true),
			array('amountfromclient, amounttowriter', 'numerical'),
			array('client_id, writer_id, level_id, papertype_id, style_id', 'length', 'max'=>11),
			array('deadline, topic, instructions, files', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('order_id, words, client_id, dateordered, timeframe_id, deadline, topic, writer_id, level_id, papertype_id, instructions, files, amountfromclient, amounttowriter, style_id, payment_status, orderstatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'order_id' => 'Order',
			'words' => 'Words',
			'client_id' => 'Client',
			'dateordered' => 'Dateordered',
			'timeframe_id' => 'Timeframe',
			'deadline' => 'Deadline',
			'topic' => 'Topic',
			'writer_id' => 'Writer',
			'level_id' => 'Level',
			'papertype_id' => 'Papertype',
			'instructions' => 'Instructions',
			'files' => 'Files',
			'amountfromclient' => 'Amountfromclient',
			'amounttowriter' => 'Amounttowriter',
			'style_id' => 'Style',
			'payment_status' => '0=unpaid, 1=paid',
			'orderstatus' => '0=new,1=available,2=assigned,3=completed,4=revision',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('order_id',$this->order_id,true);
		$criteria->compare('words',$this->words);
		$criteria->compare('client_id',$this->client_id,true);
		$criteria->compare('dateordered',$this->dateordered,true);
		$criteria->compare('timeframe_id',$this->timeframe_id);
		$criteria->compare('deadline',$this->deadline,true);
		$criteria->compare('topic',$this->topic,true);
		$criteria->compare('writer_id',$this->writer_id,true);
		$criteria->compare('level_id',$this->level_id,true);
		$criteria->compare('papertype_id',$this->papertype_id,true);
		$criteria->compare('instructions',$this->instructions,true);
		$criteria->compare('files',$this->files,true);
		$criteria->compare('amountfromclient',$this->amountfromclient);
		$criteria->compare('amounttowriter',$this->amounttowriter);
		$criteria->compare('style_id',$this->style_id,true);
		$criteria->compare('payment_status',$this->payment_status);
		$criteria->compare('orderstatus',$this->orderstatus);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Orders the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
