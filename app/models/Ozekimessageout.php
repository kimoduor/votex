<?php

/**
 * This is the model class for table "ozekimessageout".
 *
 * The followings are the available columns in table 'ozekimessageout':
 * @property integer $id
 * @property string $sender
 * @property string $receiver
 * @property string $msg
 * @property string $senttime
 * @property string $receivedtime
 * @property string $reference
 * @property string $status
 * @property string $msgtype
 * @property string $operator
 * @property string $errormsg
 */
class Ozekimessageout extends ActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ozekimessageout';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('senttime,', 'required'),
			array('sender, receiver', 'length', 'max'=>30),
			array('reference, operator', 'length', 'max'=>100),
			array('status', 'length', 'max'=>20),
			array('msgtype', 'length', 'max'=>160),
			array('errormsg', 'length', 'max'=>250),
			array('msg', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sender, receiver, msg, senttime, receivedtime, reference, status, msgtype, operator, errormsg', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sender' => 'Sender',
			'receiver' => 'Receiver',
			'msg' => 'Msg',
			'senttime' => 'Senttime',
			'receivedtime' => 'Receivedtime',
			'reference' => 'Reference',
			'status' => 'Status',
			'msgtype' => 'Msgtype',
			'operator' => 'Operator',
			'errormsg' => 'Errormsg',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('sender',$this->sender,true);
		$criteria->compare('receiver',$this->receiver,true);
		$criteria->compare('msg',$this->msg,true);
		$criteria->compare('senttime',$this->senttime,true);
		$criteria->compare('receivedtime',$this->receivedtime,true);
		$criteria->compare('reference',$this->reference,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('msgtype',$this->msgtype,true);
		$criteria->compare('operator',$this->operator,true);
		$criteria->compare('errormsg',$this->errormsg,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Ozekimessageout the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
