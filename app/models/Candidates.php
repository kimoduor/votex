<?php

/**
 * This is the model class for table "candidates".
 *
 * The followings are the available columns in table 'candidates':
 * @property string $candidate_id
 * @property string $fname
 * @property string $lname
 * @property string $code
 * @property string $post_id
 * @property string $passport
 */
class Candidates extends ActiveRecord
{
    public $key ;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'candidates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('fname, lname, code, post_id', 'required'),
			array('fname, lname, code', 'length', 'max'=>255),
			array('post_id', 'length', 'max'=>11),
			array('passport', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('candidate_id, fname, lname, code, post_id, passport', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'candidate_id' => 'Candidate',
			'fname' => 'Fname',
			'lname' => 'Lname',
			'code' => 'Code',
			'post_id' => 'Post',
			'passport' => 'Passport',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('candidate_id',$this->candidate_id,true);
		$criteria->compare('fname',$this->fname,true);
		$criteria->compare('lname',$this->lname,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('post_id',$this->post_id,true);
		$criteria->compare('passport',$this->passport,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Candidates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
