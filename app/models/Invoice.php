<?php

class Invoice {

    public static function showinvoice($email,$itemname,$descript,$amount) {
        
?>
        <title>Invoice</title>

        <link rel='stylesheet' type='text/css' href='<?php echo Yii::app()->baseUrl;?>/malt/assets/invoice/style.css' />

        <div id="page-wrap" style="margin-top: 20px;">

            <label id="header">INVOICE</label>

            <div id="identity">
                <div id="logo"><img id="image" src="<?php echo Yii::app()->baseUrl;?>/malt/img/logo.png" alt="logo" height="50" width="50"  /></div>
                <label id="address">
                    Geek Assignment,<br />                    
                    email:payment@geekassignment.com,<br />  
                    Web: geekassignment.com.
                </label>
            </div>

            <div style="clear:both"></div>

            <div id="customer">

                <label id="customer-title">To:  <span id="clientemail"><?php echo $email;?></span></label>
                <table id="meta">
                    
                    <tr>

                        <td class="meta-head">Date</td>
                        <td><label id="date"><?php echo Date('d-m-Y'); ?></label></td>
                    </tr>
                    <tr>

                        <td class="meta-head">Payment Method</td>
                        <td><label id="paymentmethod"><?php echo "paypal"; ?></label></td>
                    </tr>

                </table>

            </div>

            <table id="items">

                <tr>
                    <th>Item</th>
                    <th>Description</th>
                    <th>Unit Cost</th>
                    <th>Quantity</th>
                    <th>Price</th>
                </tr>

                <tr class="item-row">
                    <td class="item-name"><label><?php echo $itemname; ?></label></td>
                    <td class="description"><label><?php echo $descript; ?></label></td>
                    <td><label class="cost">$<?php echo $amount; ?></label></td>
                    <td><label class="qty">1</label></td>
                    <td><span class="price">$<?php echo $amount; ?></span></td>
                </tr>                <tr>
                    <td colspan="2" class="blank"> </td>
                    <td colspan="2" class="total-line balance">Total</td>
                    <td class="total-value balance"><div class="due">$<?php echo $amount; ?></div></td>
                </tr>

            </table>


            <!--    start paypal        n-->
            <?php
//$paypal_url='https://www.sandbox.paypal.com/cgi-bin/webscr'; // Test Paypal API URL
            $paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //------------real paypal url
            $paypal_id = 'payment@geekassignment.com'; // Business email ID
            ?>
            <form action="<?php echo $paypal_url; ?>" method='post' name='form'>
                <input type='hidden' name='business' value='<?php echo $paypal_id; ?>'>
                <input type='hidden' name='cmd' value='_xclick'>
                <input type='hidden' name='item_name' value='<?php echo $itemname; ?>'>
                <input type='hidden' name='item_number' value='1'>
                <input type='hidden' name='amount' value='<?php echo $amount;?>'>
                <input type='hidden' name='no_shipping' value='1'>
                <input type='hidden' name='currency_code' value='USD'>
                <input type='hidden' name='cancel_return' value='http://geekassignment.com/'>
                <input type='hidden' name='return' value="http://geekassignment.com/">
                <br />
                <button name="submit" value="pay" class="btn-large btn btn-success" style="margin-bottom: 5px; float: right;"><i class="fa fa-check-circle"></i> Checkout</button>
            </form>
            <!--End paypal-->

        </div>
   

      
        <?php
    }


}
?>

