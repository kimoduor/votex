
<?php echo $this->renderPartial('application.views.default.tpl.headerlib') ?> 
<?php
if (Yii::app()->user->name == "Guest")
    echo $this->renderPartial('application.views.default.tpl.header')
    ?>    
<br />
<style>
    .portlet-body{
        margin: 10px;
    }
    #info{
        line-height: 50px;
        font-weight: 100 !important;
        font-size: 16px; 
         font-family: "Open Sans", sans-serif !important;
    }
</style>


<div class=" portlet box blue-madison" style="clear:both; margin: 10px;">
    <div class="portlet-title">
                <div class="caption"><i class="fa fa-envelope-o fa-3x"></i>Contact Us</div>
            </div>
      
        <div class="row col-md-12 ">

            

            <div class="portlet-body form col-md-7">    
                <form action="" class="sky-form" id="sky-form">
                    <header><i class="fa fa-phone"></i>Contact Details</header>
                    <fieldset>
                        <section id="info">
                          <i class="fa fa-envelope-o"></i>  General Email: info@geekassignment.com <br />
                          <i class="fa fa-skype"></i> Skype ID: info@geekassignment.com <br />
                          <i class="fa fa-mobile-phone"></i> Phone Contacts:+1622362323<br />
                          <i class="fa fa-map-marker"></i> Address: 23423-2300- Momasu, Sikl. Asia
                          <br />
                           

                        </section>


                    </fieldset>

                </form>

            </div>


            <div class="portlet-body form col-md-4">    
                <form action="" class="sky-form">
                    <header>Contact Us</header>
                    <fieldset>	
                        <section>
                            <label class="input">
                                <i class="icon-append icon-envelope-alt"></i>
                                <input id="email" type="email" class="form-control" placeholder="Email address">
                                <b class="tooltip tooltip-bottom-right">Required for Reply</b>
                            </label>
                        </section>

                        <div class="row">
                            <section class="col col-12">
                                <label class="input">
                                    <textarea id="textarea"  class="form-control" rows="5" cols="60" placeholder="Enter Message"></textarea>
                                </label>
                            </section>

                        </div>

                        <section>

                        </section>


                    </fieldset>
                    <footer>
                        <img id="loader" style="display: none;" src="<?php echo Yii::app()->baseUrl ?>/malt/img/loader.gif">
                        <label id="success" style="display: none; color: #23b176;"><i class="fa fa-check fa-2x"></i></label>
                        <button type="submit" id="submit" class="button">Submit</button>
                    </footer>
                </form>

            </div>
        </div>

    </div>
<script>
    function validateEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }
    $('#submit').click(function(event) {
        $('#loader').show();

        var error = 0;
        event.preventDefault();
        var textarea = $('#textarea').val();
        var email = $('#email').val();
        if (textarea == "") {
            $('#textarea').css("background-color", "#fef4ef");
            $('#textarea').css("border", "1px solid #f56709");
            error = 1;
            $('#loader').hide();
            $('#success').hide();
        }

        else {
            $('#textarea').css("background-color", "#fff");
            $('#textarea').css("border", "2px solid #e5e5e5");
        }
        if (email == "") {
            $('#email').css("background-color", "#fef4ef");
            $('#email').css("border", "1px solid #f56709");
            error = 1;
            $('#loader').hide();
            $('#success').hide();
        }

        else {
            var email = $('#email').val();
            var re = /[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/igm;
            if (re.test(email)) {
                $('#email').css("background-color", "#fff");
                $('#email').css("border", "2px solid #e5e5e5");
            } else {
                $('#email').css("background-color", "#fef4ef");
                $('#email').css("border", "1px solid #f56709");
                error = 1;
                $('#loader').hide();
                $('#success').hide();

            }

        }
        if (error == 0) {//meaninf there is no error

            $.ajax({
                'type': 'POST',
                'url': '<?php echo Yii::app()->baseUrl . '/default/savecontactus'; ?>',
                'cache': false,
                'data': {'email': email, 'message': textarea},
                'success': function() {
                    $('#success').show();
                    $('#loader').hide();
                    setTimeout(function() {
                        $('#success').hide();
                        $('#email').val("");
                        $('#textarea').val("");
                    }, 3000);
                }
            });
        }





    });
</script>
<?php
if(Yii::app()->user->name =="Guest"){?>
<div class="row footer col-md-12"  style="clear:both; height: 50px; position: fixed; margin-bottom:0px !important; bottom: 0px; ">
    <center> <?php echo $this->renderPartial('application.views.default.tpl.footer') ?>    </center>
</div>
<?php }?>