<?php
/* @var $this WritersController */
/* @var $model Writers */
/* @var $form CActiveForm */
$model=new Users();
?>

<div class="form">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'writers-form',
	'enableAjaxValidation' => false,
        'htmlOptions' => array(
            'enctype' => 'multipart/form-data',
        ),
    )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>


	<div class="row">
		<?php echo $form->labelEx($model,'FirstName'); ?>
		<?php echo $form->textField($model,'fname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'FirstName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Last Name'); ?>
		<?php echo $form->textField($model,'lname',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Last Name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Password (Atleast 5 Characters)'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>255,
                    'onblur'=>'confirmpsd()','id'=>'pass1')); ?>
		<?php echo $form->error($model,'Password'); ?>
	</div>
        <div class="row">
		<?php echo $form->labelEx($model,'Confirm Password'); ?>
		<?php echo $form->passwordField($model,'password2',array('size'=>60,'onblur'=>'confirmpsd()','id'=>'pass2')); ?>
            <img src="<?php echo Yii::app()->baseurl.'/images/accept.png';?>" alt="accepted" id="accept" style="display: none;" />
            <img src="<?php echo Yii::app()->baseurl.'/images/delete.png';?>" alt="invalid" id="invalid" style="display: none;" />

	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'Address'); ?>
		<?php echo $form->textField($model,'address',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'Address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'Email'); ?>
	</div>

	<?php echo $form->hiddenField($model,'activationcode',array('value'=>11)); ?>

		

          <div class="row">
		<?php echo $form->labelEx($model,'Photo of IDentity Card'); ?>
		        <?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'attribute'=>'IdCard',
      'name'=>'IdCard',
     'accept'=>'jpg|gif|png',
     'options'=>array(
        'afterFileRemove'=>'function(e, v, m){ alert("Removed - "+v+ " Successfully") }',
     ),
     'denied'=>'File is not allowed',
     'max'=>1, // max 10 files
  ));
?>
		<?php echo $form->error($model,'idcard'); ?>
        </div>

         <?php //------------------------------------------------------------------------?>

  <div class="row">
    <?php echo $form->labelEx($model,'Passport Photo'); ?>
        <?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'attribute'=>'Passport',
      'name'=>'Passport',
     'accept'=>'jpg|gif|png',
     'options'=>array(
        'afterFileRemove'=>'function(e, v, m){ alert("Removed - "+v+ " Successfully") }',
     ),
     'denied'=>'File is not allowed',
     'max'=>1, // max 10 files
  ));
?>
		<?php echo $form->error($model,'photo'); ?>
        </div>
	  <?php //-------------------------------------------------------------------------?>
        <div class="row">
		<?php echo $form->labelEx($model,'Any Equivalent Bachelor, Masters or PHD Degree'); ?>
        <?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'attribute'=>'Degree',
      'name'=>'Degree',
     'accept'=>'jpg|gif|png|doc|docx|txt|pdf|zip|rar|xls|xlsx|accdb|pub',
     'options'=>array(
        'afterFileRemove'=>'function(e, v, m){ alert("Removed - "+v+ " Successfully") }',
     ),
     'denied'=>'File is not allowed',
     'max'=>1, // max 10 files
  ));
?>
		<?php echo $form->error($model,'Degree'); ?>
        </div>
        <?php //--------------------------------------------------------------------------------------------?>
        <div class="row">
		<?php echo $form->labelEx($model,'Sample of Paper Written Before(At least 3)'); ?>
		 <?php
  $this->widget('CMultiFileUpload', array(
     'model'=>$model,
     'attribute'=>'PaperSamples',
      'name'=>'PaperSamples',
     'accept'=>'doc|docx',
     'options'=>array(
        'afterFileRemove'=>'function(e, v, m){ alert("Removed - "+v+ " Successfully") }',
     ),
     'denied'=>'File is not allowed',
     'max'=>4, // max 10 files
  ));
?>
		<?php echo $form->error($model,'PaperSamples'); ?>
        </div>
 <?php //--------------------------------------------------------------------------------------------?>
        Remember Clicking the "Create" Button Means you have accepted
        <a href="<?php echo Yii::app()->baseurl.'/index.php/writers/terms';?>" target="_blank">Terms and Conditions</a>
        of becoming part of Our Writing Team.
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
<script type="text/javascript">
    function confirmpsd(){
    var pas1=document.getElementById('pass1').value;
    var pas2=document.getElementById('pass2').value;
    if(pas1!=pas2){
        $("#accept").hide();
      $("#invalid").show('slow');
       document.getElementById('btt').disabled=true;
    }
    if(pas1===pas2 && pas1.length>4){
     $("#invalid").hide();
     $("#accept").show('slow');
    document.getElementById('btt').disabled=false;
    }
    }
</script>