<div class="page-sidebar navbar-collapse collapse">
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
        <li class="heading">
            <h3>The Votex</h3> 
        </li>
            <li class="start">
            <a href="#/dashboard">
                <i class="icon-home"></i>
                <span class="title">Standing  Results</span>
            </a>
        </li>
        <li class="start">
            <a href="#/current">
                <i class="icon-home"></i>
                <span class="title">Current Results</span>
            </a>
        </li>
           <li>
            <a href="#/votes">
                <i class="fa fa-bar-chart-o"></i>
                <span class="title">Votes</span>
            </a>
        </li>
        <li>
            <a href="#/candidates">
                <i class="fa fa-ge"></i>
                <span class="title">Candidates</span>
            </a>
        </li>
         <li>
            <a href="#/voters">
                <i class="fa fa-user"></i>
                <span class="title">Voters</span>
            </a>
        </li>
      
        <li>
            <a href="#/posts">
                <i class="fa fa-check-circle-o"></i>
                <span class="title">Posts</span>
            </a>
        </li>
        <li>
                <a href="javascript:;">
                    <i class="icon-user-following"></i>
                    <span class="title">Users</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    
                    
                    
                    <li>
                        <a href="#/newroles">
                            <i class="icon-user"></i> Manage Users
                        </a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="fa fa-envelope-square"></i>
                    <span class="title">Messages</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <?php
                    $role_id = Users::model()->get(Yii::app()->user->id, "role_id");
                    if ($role_id == 1 || $role_id == 2) {
                        ?>
                        <li>
                            <a href="#/messages">
                                <i class="fa fa-envelope-o"></i> Manage Messages
                            </a>
                        </li>
                    <?php } ?>


                </ul>
            </li>


    </ul>
    <!-- END SIDEBAR MENU -->
</div>	
