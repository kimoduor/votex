<?php $model = new Orders(); ?>
<style>

    .errordisplay{
        background-color:#fef4ef;
        border:1px solid #f56709;
        position:absolute;
        width:28.2%;
        padding: 10px;
        top: 20px;
        color:red;
        z-index:100;

    }.error{
        background-color:#fef4ef;
        border:1px solid #f56709;
        position:absolute;
        width:28.2%;
        padding: 10px;
        top: 90px;
        color:red;
        font-size: 13px;
        z-index:100;
        box-shadow: 1px #fef4ef;
        border-radius: 3px;

    }
    .success{
        background-color:#ccffcc;
        border:1px solid green;
        position:absolute;
        width:28.2%;
        padding: 10px;
        top: 90px;
        font-size: 13px;
        z-index:100;
        box-shadow: 1px #ccffcc;
        border-radius: 3px;
    }
    .loading{
        opacity:0.4;
    }

</style>
    <div class="col-md-12 sky-form"  id="sky-form">
            <fieldset>
                <form action="<?php echo Yii::app()->baseUrl . '/orders/orders/create'; ?>" method="post"  enctype="multipart/form-data">
                    <div class="row fm">

                        <div class="col-md-4"> Email </div> 
                        <div class="col-md-8"> <label class="input">
                                <i class="icon-append fa-envelope-o"></i>
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email address">
                                <b class="tooltip tooltip-bottom-right">Enter Email to verify your account</b>
                            </label>
                        </div>
                    </div>
                    <div class="row fm">
                        <div class="col-md-4"> Type of Paper </div> 
                        <div class="col-md-8">
                            <label class="input">
                                <i class="icon-append "></i>
                                <?php echo CHtml::activeDropDownList($model, 'papertype_id', Papertypes::model()->getListData('papertype_id', 'papertype'), array('class' => 'form-control', 'required' => true, 'onchange' => 'calculateamount()')); ?>
                                <b class="tooltip tooltip-bottom-right">Type of the Paper</b>
                            </label>
                        </div>
                    </div>
                    <div class="row fm">
                        <div class="col-md-4"> Topic of the Paper</div> 
                        <div class="col-md-8">
                            <label class="input">
                                <i class="icon-append fa-ils"></i>
                                <input type="text"  class="form-control" name="title" id="topic" placeholder="Topic">
                                <b class="tooltip tooltip-bottom-right">Topic of the Paper</b>
                            </label>
                        </div>
                    </div>
                    <div class="row fm">
                        <div class="col-md-4"> Number of Words</div> 
                        <div class="col-md-8">
                            <label class="input">
                                <i class="icon-append fa-h-square"></i>
                                <input type="number" onclick="calculateamount()" onkeyup="calculateamount()"  class="form-control" name="words" id="words" placeholder="Number of Words">
                                <b class="tooltip tooltip-bottom-right">Number of Words</b>                          
                            </label>
                        </div>
                    </div>
                    <div class="row fm">
                        <div class="col-md-4">Paper Level</div> 
                        <div class="col-md-8">
                            <label class="input">
                                <i class="icon-append"></i>
                                <?php echo CHtml::activeDropDownList($model, 'level_id', Levels::model()->getListData('level_id', 'level'), array('class' => 'form-control', 'required' => true, 'onchange' => 'calculateamount()')); ?>

                                <b class="tooltip tooltip-bottom-right">Paper Level</b>
                            </label>
                        </div>
                    </div>
                    <div class="row fm">
                        <div class="col-md-4">Deadline</div> 
                        <div class="col-md-8">
                            <label class="input">
                                <i class="icon-append fa-calendar"></i>
                                <input type="text" class="form-control" name="deadline" placeholder="Deadline" id="datepicker">
                                <b class="tooltip tooltip-bottom-right">Deadline</b>
                            </label>
                            <label>Time:</label> 
                            <input type="range" name="hours" min="0" max="23" value="0" oninput="this.form.amountInput.value=this.value" onchange="datedifference()" />
                            <input style="border:0; background: #f0f0f0; width: 29px; padding: 0" id="hrs" disabled="true" type="number" name="amountInput" min="0" max="23" value="0" oninput="this.form.amountRange.value=this.value" onchange="datedifference()" />.00  Hrs
                            <br /> <strong>[<span id="left"></span>  Left]</strong>

                        </div>
                    </div>

                    <div class="row fm">
                        <div class="col-md-4">Upload Additional Materials</div> 
                        <div class="col-md-8">
                            <label class="input">
                                <i class="icon-append fa-file"></i>
                                <?php
                                $this->widget('CMultiFileUpload', array(
                                    'model' => $model,
                                    'attribute' => 'Files',
                                    'name' => 'Files',
                                    'accept' => 'jpg|gif|png|doc|docx|txt|pdf|zip|rar|xls|xlsx|accdb|pub',
                                    'options' => array(
                                        'afterFileRemove' => 'function(e, v, m){ alert("Ready to Remove - "+v+"") }',
                                    ),
                                    'denied' => 'File is not allowed',
                                    'max' => 10, // max 10 files
                                ));
                                ?>
                            </label>
                        </div>
                    </div>
                    <footer> 
                        <label class="arrow_box" style="float: left;">$<span  id="amount">0</span></label>
                        <input type="hidden" id="amountfromclient" name="amountfromclient">
                        <button type="submit" class="button" id="submit">Continue ></button>
                        <img id="loader" style="display: none;" src="<?php echo Yii::app()->baseUrl ?>/malt/img/loader.gif">
                    </footer>
                </form>
            </fieldset>

</div>







<script>


    var dateToday = new Date();
    $(function() {
        $("#datepicker").datepicker({
            showButtonPanel: true,
            changeMonth: true,
            changeYear: true,
            minDate: dateToday,
            onSelect: function() {
                datedifference();
            },
            dateFormat: "yy-mm-dd"
        });
    });
    function datediffunc(mydate, myhrs) {
        if (mydate.length >= 4) {
            var date = new Date(mydate);
            var chosendate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + " " + myhrs + ":00:00";
            var now = new Date();
            var nowproper = (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes();
            //----------------------------start the difference--------------------------
            var diff = moment.duration(moment(chosendate).diff(moment(nowproper)));
            var days = parseInt(diff.asDays()); //84
            var hours = parseInt(diff.asHours());
            hours = hours - days * 24; // 23 hours
            var minutes = parseInt(diff.asMinutes());
            minutes = minutes - (days * 24 * 60 + hours * 60);
            var outputstring = days + " Days " + hours + " Hours " + minutes + " Minutes";
            //calculateamount();
            return (days * 24) + hours;
            //----------------------------end the difference----------------------------
        }
    }
    function datedifference() {
        var mydate = $('#datepicker').val();
        var myhrs = $('#hrs').val();
        if (mydate.length >= 4) {
            var date = new Date(mydate);
            var chosendate = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + " " + myhrs + ":00:00";
            var now = new Date();
            var nowproper = (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear() + " " + now.getHours() + ":" + now.getMinutes();
            //----------------------------start the difference--------------------------
            var diff = moment.duration(moment(chosendate).diff(moment(nowproper)));
            var days = parseInt(diff.asDays()); //84
            var hours = parseInt(diff.asHours());
            hours = hours - days * 24; // 23 hours
            var minutes = parseInt(diff.asMinutes());
            minutes = minutes - (days * 24 * 60 + hours * 60);
            var outputstring = days + " Days " + hours + " Hours " + minutes + " Minutes";
            $('#left').html(outputstring);
            calculateamount();

        }
    }

    function calculateamount() {
        var hoursleft = datediffunc($('#datepicker').val(), $('#hrs').val());
        $.ajax({'type': 'POST',
            'url': '<?php echo Yii::app()->baseUrl . '/default/pricedetails'; ?>',
            'cache': false,
            'data': {'level_id': $('#Orders_level_id').val(), 'papertype_id': $('#Orders_papertype_id').val(), 'hoursleft': hoursleft},
            'success': function(jsondetails) {
                var ojb = JSON.parse(jsondetails);
                var minimalcpp = parseFloat(ojb['minimumcpp']);
                var levelrate = parseFloat(ojb['levelrate']);
                var papertyperate = parseFloat(ojb['papertyperate']);
                var timeframerate = parseFloat(ojb['timeframerate']);
                var words = parseFloat($('#words').val());
                var wordsamount = parseFloat(words) * parseFloat(ojb['amountperword']);
                var level = wordsamount * levelrate;
                var papertype = wordsamount * papertyperate;
                var timeframerateamount = wordsamount * timeframerate;
                var amount = minimalcpp + wordsamount + level + papertype + timeframerateamount;
                $('#amount').html(Math.round(amount));
                  $('#amountfromclient').val(Math.round(amount));
            }
        });
    }
    $('#submit').click(function(event) {
        $('#loader').show();
        $('.body').addClass("loading");
        $(":text,#email,:password, select").each(function() {
            if ($(this).val() === "") {
                event.preventDefault();
                $(this).css("background-color", "#fef4ef");
                $(this).css("border", "1px solid #f56709");
                $('.body').removeClass("loading");
                $('#loader').hide();
            }
            else {
                $(this).css("background-color", "#fff");
                $(this).css("border", "2px solid #e5e5e5");
                
            }

        });
    });
</script>



