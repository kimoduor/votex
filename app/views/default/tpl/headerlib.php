<!DOCTYPE html>
<html lang="en" data-ng-app="MuskeyApp">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <title data-ng-bind="'Votex| ' + $state.current.data.pageTitle"></title>

        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <!-- BEGIN HEADER INNER -->
        <script src="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/js/jquery-1.10.2.js'; ?>"></script>
        <script src="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/js/jquery-ui.js'; ?>"></script>


        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/css/main.css'; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/css/sky-forms.css'; ?>">
        <link rel="stylesheet" href="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/css/jquery-ui.css'; ?>">
       <script src="<?php echo Yii::app()->baseUrl . '/malt/assets/registration/js/moment.js'; ?>"></script>

        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>


        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before"/>
        <!-- END DYMANICLY LOADED CSS FILES -->

        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/css/components-rounded.css" id="style_components" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/admin/layout4/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/admin/layout4/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo Yii::app()->baseUrl; ?>/malt/assets/admin/layout4/css/custom.css" rel="stylesheet" type="text/css"/>
        <script type="text/ecmascript" src="<?php echo Yii::app()->baseUrl; ?>/malt/jqgrid/js/grid.locale-en.js"></script> 
        <script type="text/ecmascript" src="<?php echo Yii::app()->baseUrl; ?>/malt/jqgrid/js/jquery.jqGrid.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo Yii::app()->baseUrl; ?>/malt/jqgrid/css/ui.jqgrid-bootstrap.css" />

