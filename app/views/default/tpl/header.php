<style>
    .page-header-inner #mainmenu {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        margin-top: 20px;
        float: left;
        margin-right: 20px; 
        line-height: 20px;
    }
    #mainmenu li{
        float: left;

    }
    #mainmenu li a{
        display: block;
        text-align: center;
        padding: 14px 16px;
        text-decoration: none;
        font-weight: 300;
        font-family: "Open Sans", sans-serif !important;

    }
    #mainmenu li a:hover {
        background-color: #f2f2f2;
    }
    .headlogo{ font-family: Brush Script MT;
               font-size: 48px;
    }

</style>

<?php if (Yii::app()->user->name == 'Guest') { ?> 
    <style>
        .page-header-inner #mainmenu {
            float: right;
        }
    </style>
<?php } ?>
<div class="page-header-inner">
    <!-- BEGIN LOGO -->
    <div class="page-logo" style="margin-left: 10px; float: left">
        <a href="#/dashboard">
        </a> <span class="headlogo">Votex </span>
        <div class="menu-toggler sidebar-toggler">

            <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
        </div>

    </div>



    <!-- END LOGO -->
    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
    </a>
    <div class="page-top">
        <!-- END HEADER SEARCH BOX -->
        <!-- BEGIN TOP NAVIGATION MENU -->
   
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
               
                <li class="separator hide"></li>

                <?php if (Yii::app()->user->name != 'Guest') { ?>   

                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span id="newnotifications" class=" badge badge-success">0 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3><span class="bold"><span id="newnotifications2" class="newnotificationsbadge">0</span> pending</span> notifications</h3>

                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                    <li>
                                        <a href="javascript:;">
                                            <span class="time"><?php echo date("H:i"); ?></span>
                                            <span class="details">
                                                <span class="label label-sm label-icon label-danger">
                                                    <i class="fa fa-won"></i>
                                                </span>
                                                Welcome to Geek Writers
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <li class="separator hide"></li> 


                    <!-- BEGIN INBOX DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-envelope-open"></i>
                            <span class="badge badge-danger" id="msg">
                                0 </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>You have <span class="bold" id="mymessages">Loading...</span> Unread Messages</h3>
                                <a href="#/messages">view all</a>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                    <?php
                                    $messages = Messages::model()->findAll(array('condition' => "touser_id='" . Yii::app()->user->id . "' AND (status_id=0 OR status_id=1)", 'limit' => "3"));
                                    foreach ($messages as $message) {
                                        $sender = Users::model()->get($message->fromuser_id, "CONCAT(fname,' ',lname)");
                                        $date = $message->date;
                                        $subject = $message->subject;
                                        $body = substr($subject, 0, 54) . "..";
                                        $photo = Users::model()->get($message->fromuser_id, "photo_url");
                                        $passportphoto = "";
                                        if (strlen($photo) > 1) {
                                            $passportphoto = Yii::app()->baseUrl . '/public/idfore/' . $photo;
                                        } else {
                                            $passportphoto = Yii::app()->baseUrl . '/malt/img/default.jpg';
                                        }
                                        ?>
                                        <li>
                                            <a href="#/messages">
                                                <span class="photo">
                                                    <img src="<?php echo $passportphoto; ?> " class="img-circle" alt="">
                                                </span>
                                                <span class="subject">
                                                    <span class="from">
                                                        <?php echo $sender; ?> </span>
                                                    <span class="time"> <?php echo $date; ?> </span>
                                                </span>
                                                <span class="message">
                                                    <?php echo $body; ?>
                                                </span>
                                            </a>
                                        </li>

                                        <?php
                                    }
                                    ?>

                                </ul>
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->
                    <li class="separator hide"></li>
                    <!-- BEGIN TODO DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user dropdown-dark">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <span class="username username-hide-on-mobile">

                                <?php
                                echo Yii::app()->user->name;
                                ?>
                            </span>
                            <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                            <?php
                            $user_id = Yii::app()->user->id;
                            $imageurl = Users::model()->getScalar('photo_url', "id='$user_id'");
                            ?>
                            <img id="uploadPreview3"  alt="" class="img-circle" height="45" width="50"  src="<?php echo $imageurl ? "public/idfore/$imageurl" : Yii::app()->baseUrl . "/malt/img/default.jpg"; ?>" class="myimage" />

                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="#/registration2">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>

                            <li>
                                <a href="#/messages">
                                    <i class="icon-envelope-open" ></i> My Inbox <span class="badge badge-danger" id="myinbox">
                                        0 </span>
                                </a>
                            </li>

                            <li class="divider">
                            </li>
                            <li>
                                <a href="auth/default/logout">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                <?php }
                ?>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END PAGE TOP -->
</div>
<!-- END HEADER INNER -->
