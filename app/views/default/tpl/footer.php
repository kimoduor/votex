<div class="page-footer-inner">
	<?php echo date("Y");?> &copy; Geek  Writers
</div>
<div class="scroll-to-top" style="float: right;margin-right: 0;">
	<i class="icon-arrow-up"></i>
</div>
 <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <!-- END CORE JQUERY PLUGINS -->
