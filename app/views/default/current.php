<script>
    $.jgrid.defaults.width = 1200;
    $.jgrid.defaults.responsive = true;
    $.jgrid.defaults.styleUI = 'Bootstrap';</script>
<div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        $.jgrid.styleUI.Bootstrap.base.rowTable = "table table-bordered table-striped";
        var template = "<div style='margin-left:15px;'>";
        template += "<div>Candidate: </div><div>{candidate_id} </div>";
        template += "<hr style='width:100%;'/>";
        template += "<div> {sData} {cData}  </div></div>";
        $("#jqGrid").jqGrid({
            url: 'data/totalvotes',
            // we set the changes to be made at client side using predefined word clientArray
            datatype: "local",
            rownumbers: true,
            toppager: true,
            colModel: [
                {label: 'Candidacy Posts',
                    name: 'post_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    summaryType: 'sum',
                    summaryTpl: "<b>Total Votes Casted </b>",
                    editoptions: {value:<?php echo Details::postsjson(); ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::postsjson(); ?>},
                    width: 200,
                },
                {label: 'Candidate',
                    name: 'candidate_id',
                    editable: true,
                    edittype: 'select',
                    formatter: 'select',
                    editoptions: {value:<?php echo Details::candidatejson() ?>},
                    stype: 'select',
                    searchoptions: {value:<?php echo Details::candidatejson(); ?>},
                    width: 200,
                },
                {label: 'total Votes ',
                    name: 'total',
                    width: 200,
                    summaryType: 'sum',
                    summaryTpl: "<b>{0}</b>",
                    formatter: "number",
                    editable: true
                }

            ],
            sortname: 'post_id',
            sortorder: 'asc',
            loadonce: true,
            toolbar: true,
            viewrecords: true,
            width: 1000,
            height: 400,
            rowNum: 20,
            grouping: true,
            groupingView: {
                groupField: ['post_id'],
                groupSummary: [true],
                groupColumnShow: [true],
                groupText: ['<strong>{0}</strong>'],
                groupCollapse: false,
                groupOrder: ['asc'],
            },
            pager: "#jqGridPager",
        }).setGridParam({datatype: 'json'}).trigger('reloadGrid');
        $('#jqGrid').navGrid('#jqGridPager',
                // the buttons to appear on the toolbar of the grid
                        {edit: false, add: false, del: false, search: true, refresh: true, view: true, position: "left", cloneToTop: true},
                {url: "settings/voteheadyear/update", afterSubmit: processAddEdit, closeAfterEdit: true},
                {url: "settings/voteheadyear/create", afterSubmit: processAddEdit, closeAfterAdd: true},
                {url: "settings/voteheadyear/delete"},
                {// reload
                    reloadAfterSubmit: true
                },
                // options for the Edit Dialog
                {
                    editCaption: "The Edit Dialog",
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Add Dialog
                {
                    template: template,
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }
                },
                // options for the Delete Dailog
                {
                    errorTextFormat: function(data) {
                        return 'Error: ' + data.responseText
                    }

                });

                $('#jqGrid').jqGrid('filterToolbar', {
                    // JSON stringify all data from search, including search toolbar operators
                    stringResult: true,
                    // instuct the grid toolbar to show the search options
                    searchOperators: false,
                });
            });

</script>

