<?php echo $this->renderPartial('application.views.default.tpl.headerlib') ?> 
        <!-- END THEME STYLES -->

        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed-hide-logo page-on-load" ng-class="{'page-sidebar-closed': settings.layout.pageSidebarClosed}">

        <!-- BEGIN PAGE SPINNER -->
        <div ng-spinner-bar class="page-spinner-bar">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <!-- END PAGE SPINNER -->
 
        <!-- BEGIN HEADER -->
        <div data-ng-include="'default/header'" data-ng-controller="HeaderController" class="page-header navbar navbar-fixed-top">
        </div>
        <!-- END HEADER -->

        <div class="clearfix">
        </div>

        <!-- BEGIN CONTAINER -->  <div class="page-container">
            <?php if (isset(Yii::app()->session['success'])) { ?>

                <div class="datamessages note note-success note-bordered">
                    <i class="fa fa-check-circle-o"></i>  <?php echo Yii::app()->session['success']; ?><span class="close" data-close="note"></span>
                </div>
                <?php
                unset(Yii::app()->session['success']);
            } elseif (isset(Yii::app()->session['error'])) {
                ?> <div class="datamessages note note-danger note-bordered">
                    <i class="fa fa-close"></i> Error:<?php echo Yii::app()->session['error']; ?><span class="close" data-close="note"></span>
                </div>
                <?php
                unset(Yii::app()->session['error']);
            }
            ?>

            <!-- BEGIN SIDEBAR -->
            <div data-ng-include="'default/sidebar'" data-ng-controller="SidebarController" class="page-sidebar-wrapper">			
            </div>
            <!-- END SIDEBAR -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD -->

                    <!-- END PAGE HEAD -->
                    <!-- BEGIN ACTUAL CONTENT -->
                    <div ui-view class="fade-in-up">
                    </div> 
                    <!-- END ACTUAL CONTENT -->
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
  <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS --> 

       

        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
        <!-- BEGIN FOOTER -->
      <?php echo $this->renderPartial('application.views.default.tpl.footer') ?> 
        
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>	
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>	
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/plugins/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
        <!-- END CORE ANGULARJS PLUGINS -->
  


        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS --> 

        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/js/app.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/js/controllers.js"></script> 
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/js/directives.js" type="text/javascript"></script>

        <!-- END APP LEVEL ANGULARJS SCRIPTS -->


        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/global/scripts/muskey.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/admin/layout4/scripts/layout.js" type="text/javascript"></script>
        <script src="<?php echo Yii::app()->baseUrl; ?>/malt/assets/admin/layout4/scripts/demo.js" type="text/javascript"></script>  
        <!-- END APP LEVEL JQUERY SCRIPTS -->

        <script type="text/javascript">
                    /* Init Muskey's core jquery plugins and layout scripts */
                    $(document).ready(function() {
                        Muskey.init(); // Run Muskey theme
                        Muskey.setAssetsPath('malt/assets/'); // Set the assets folder path	

                    });
                    function processAddEdit(response) {
                        if (response.responseText != "true") {
                            var json = eval('(' + response.responseText + ')');
                            //each error
                            var errorstring = "Errors:";
                            for (var key in json) {
                                if (json.hasOwnProperty(key)) {
                                    errorstring = errorstring + "<br/>" + json[key];
                                }
                            }
                            var new_id = "1";

                            return [false, errorstring, new_id];
                        } else
                        {
                            $("#jqGrid").setGridParam({datatype: 'json'}).trigger('reloadGrid');
                            $('.ui-jqdialog-titlebar-close').trigger("click");
                            return [true, "Data Successfully Saved", "1"];


                        }
                    }
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>


       
