<?php

/**
 * Home controller
 * @author Fred<mconyango@gmail.com>
 */
class DefaultController extends Controller {

    public function init() {
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users
                'actions' => array('firststep','invoicetest', 'header', 'headerlib', 'dashboard', 'sidebar', 'footer', 'orderpage',
                    'pagehead', 'registration', 'createmember', 'pricedetails',
                    'writersregistration', 'contactus', 'savecontactus','current'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('index', 'uploads', '_reportCSV',
                    'messages', 'messagesreplied', 'messagereplydelete',
                    'messagesreply', 'messagedelete'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    public function actionHeader() {
        $this->renderPartial('tpl/header');
    }

    public function actionHeaderlib() {
        $this->renderPartial('tpl/headerlib');
    }

    public function actionLib() {
        $this->renderPartial('tpl/lib');
    }

    public function actionDashboard() {
        //$model = new Orders();

        $this->render('dashboard');
        // $this->renderPartial('dashboard');
    }
     public function actionCurrent() {
        //$model = new Orders();

        $this->render('current');
        // $this->renderPartial('dashboard');
    }

    public function actionCreatemember() {

        $model = new Membership();
        if (isset($_POST['fname']) && isset($_POST['lname']) && isset($_POST['email']) && isset($_POST['mobile'])) {
            $model->fname = $_POST['fname'];
            $model->lname = $_POST['lname'];
            $model->email = $_POST['email'];
            $model->mobile = $_POST['mobile'];
            $model->dob = $_POST['dob'];

            if ($model->save()) {//start saving on the user end
                // $model->membership_code = $model->member_id < 10 ? 'UBS0' . $model->member_id : 'UBS' . $model->member_id;
                $model->save();
                $usermodel = new Users(ActiveRecord::SCENARIO_CREATE);
                $usermodel->name = $model->fname . " " . $model->lname;
                $usermodel->username = $_POST['email'];
                $usermodel->email = $model->email;
                $usermodel->phone = $model->mobile;
                $salt = Common::generateSalt();
                // $usermodel->setScenario(Users::SCENARIO_UPDATE);
                $usermodel->password = $salt . md5($_POST['password']);
                // var_dump($usermodel->password);
                $usermodel->salt = $salt;
                $usermodel->user_level = 3;
                $usermodel->role_id = 3;
                $error_message = CActiveForm::validate($usermodel);
                $error_message_decoded = json_decode($error_message);
                if (empty($error_message_decoded)) {
                    $usermodel->save();
                    $usermodel->password = $salt . md5($_POST['password']);
                    $usermodel->salt = $salt;
                    $usermodel->save();

                    $model->user_id = $usermodel->id;
                    $model->save();
// the message
                    $msg = "Thank you for Registering to Become a member of Ubunifu Sacco. Your Login Email is  " . $usermodel->username . ", Password is " . $_POST['password'] . ". Kindly Login using this Link http://dash.ubunifusacco.com/auth/default/login";
                    //get all emails to send to rem the first one is the registrant
                    $msg = wordwrap($msg, 70);
                    mail($usermodel->email, "Welcome to Ubunifu Sacco, Ready to transform your Future through Savings", $msg);
                    $adminemails = Emails::model()->findAll(array('condition' => "email_category=1"));
                    $adminmsg = "A new Member has Just Registered and has email " . $usermodel->email . ". "
                            . "Kindly Login and Check his Profile. "
                            . "Also advise the New Member to complete His/ Her Registration Profile";
                    $adminmsg = wordwrap($adminmsg, 70);
                    foreach ($adminemails as $emails) {
                        mail($emails->email, "New Member Code:  " . $model->membership_code, $adminmsg);
                    }


                    echo json_encode(array('operation' => "success"));
                } else {
                    $errorlist = "<br />";
                    foreach ($error_message_decoded as $error) {
                        $errorlist.=$error[0] . '<br />';
                    }
                    //echo $error_message_decoded;
                    echo json_encode(array("operation" => "error", "the_errors" => $errorlist));
                    // $this->redirect('registration/index',array('model'=>$model));
                }



                //Yii::app()->user->Flash()
            }
        }




        //   echo "more..";
        //  exit;
        else {
            $this->render('registration/index', array('model' => $model));
        }
    }

    public function actionSidebar() {
        $this->renderPartial('tpl/sidebar');
    }

    public function actionFooter() {
        $this->renderPartial('tpl/footer');
    }
    
    public function actionInvoicetest() {
        echo $this->renderPartial('application.views.default.tpl.headerlib');
        $email="kimoduor";
        $itemname="NEW oRDER";
        $descript="The new Order";
        $amount="90";        
        Invoice::showinvoice($email, $itemname, $descript, $amount);
            }

    public function actionPagehead() {
        $this->renderPartial('tpl/pagehead');
    }

    public function actionRegistration() {
        $model = new Orders();

        $this->render('registration/index', array('model' => $model));
    }

    public function actionWritersregistration() {
        $this->render('registration/writersregistration');
    }

    public function actionContactus() {
        $this->render('registration/contactus');
    }

    public function actionOrderpage() {
        $this->render('tpl/orderpage');
    }

    public function actionSavecontactus() {//make sure you send email to all the admins
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
            $message = $_POST['message'];
            //send email to those whose role is 2
            $usermodels = Users::model()->findAll('role_id=2');
            foreach ($usermodels as $usermodel) {
                Common::sendEmail($usermodel->email, $email, "$email via Contact us", $message);
                $notificationmodel = new Notifications();
                $notificationmodel->touser_id = $usermodel->id;
                $notificationmodel->subject = "$email via Contact us";
                $notificationmodel->body = $message;
                // var_dump($notificationmodel);                
                $notificationmodel->save();
            }
            echo true;
        }
        echo true;
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->pageTitle = Lang::t('Dashboard');
        $this->activeMenu = 1;


        $this->render('index', array(
        ));
    }

    public function actionPricedetails() {
        $model = Prices::model()->loadModel(1);
        $levelrate = 0;
        $papertyperate = 0;
        $timeframerate = 0;
        if (isset($_POST['level_id']) && $_POST['level_id'] > 0) {
            $levelmodel = Levels::model()->loadModel($_POST['level_id']);
            $levelrate = $levelmodel->rate;
        }
        if (isset($_POST['papertype_id']) && $_POST['papertype_id'] > 0) {
            $papertypemodel = Papertypes::model()->loadModel($_POST['papertype_id']);
            $papertyperate = $papertypemodel->rate;
        }
        if (isset($_POST['timeframe_id']) && $_POST['timeframe_id'] > 0) {
            $timeframemodel = Timeframe::model()->loadModel($_POST['timeframe_id']);
            $timeframerate = $timeframemodel->rate;
        }
        $array = array();
        $array['minimumcpp'] = $model->minimumcpp;
        $array['amountperword'] = $model->amountperword;
        $array['papertyperate'] = $papertyperate;
        $array['levelrate'] = $levelrate;
        $array['timeframerate'] = $timeframerate;
        $theoutput = json_encode($array);
        echo $theoutput;
    }

}
