<?php

class ApiController extends Controller {

    // Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers 
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    /**
     * Default responce strings
     */
    public $sender = '+260971231878';
    public $redempption = 'Evoucher';
    public $startString = 'ST';
    public $endString = 'EX';
    public $separator = '|';
    public $deviceAuth = 'STR';
    public $cardAuth = 'FMT';
    public $validate = 'VAL';
    public $save = 'SAV';
    public $logAgrodealerString = 'LAS';
    public $next = 'NXT';
    public $write = 'WRT';
    public $appurl = 'public/appversions/';

    /**
     *
     * @var type 
     */
    public $error_message = array(
        "status" => "error",
    );

    /**
     *
     * @var type 
     */
    public $success_message = array(
        "status" => 'ok'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('list', 'auth'),
                'users' => array('*'),
            ),
        );
    }

    // Actions
    public function actionList() {
        header('Content-type: application/json');
        $models = Posfarmers::model()->findAll();
        $rows = array();
        foreach ($models as $model)
            $rows[] = $model->attributes;
        echo CJSON::encode($rows);
    }

    public function actionChangeCamp() {
        $returnedJson = array();
        header('Content-type: application/json');
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $campid = isset($_GET['CAMP']) ? $_GET['CAMP'] : false;
        if ($ser && $campid) {
            $models = Agricamps::model()->findByAttributes(
                    array('camp_code' => $campid,
            ));
            $camp_id = $models->agri_camp_id;
            $camp = Yii::app()->db->createCommand()
                    ->select('camp_id')
                    ->from('tblposwritedevices')
                    ->where("`imei_no`=$ser")
                    ->limit(1)
                    ->queryRow();
            if ($camp) {
                $command = Yii::app()->db->createCommand();
                $command->update('tblposwritedevices', array(
                    'imei_no' => $ser,
                    'camp_id' => $campid
                        ), 'imei_no=:id', array(':id' => $ser));
                $returnedJson['RSP'] = 'TRUE';
            } else {
                $command = Yii::app()->db->createCommand();
                $command->insert('tblposwritedevices', array(
                    'imei_no' => $ser,
                    'camp_id' => $campid,
                ));
                $returnedJson['RSP'] = 'TRUE';
            }
        } else {
            $returnedJson['RSP'] = 'FALSE';
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->write .
        $this->separator .
        CJSON::encode($returnedJson) .
        $this->separator .
        $this->endString .
        $this->separator;
    }

    public function actionNextCard() {
        $returnedJson = array();
        header('Content-type: application/json');

        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $campid = 0;
        if ($ser) {
            $camp = Yii::app()->db->createCommand()
                    ->select('camp_id AS name')
                    ->from('tblposwritedevices')
                    ->where("`imei_no`=$ser")
                    ->limit(1)
                    ->queryRow();
            if ($camp) {
                $campid = $camp['name'];
                $models = Agricamps::model()->findByAttributes(
                        array('agri_camp_id' => $campid,
                        )
                );
                if ($models) {
                    $returnedJson['CPNM'] = $models->name;
                    $returnedJson['CPID'] = $models->camp_code;
                }
            }

            $farmer = Yii::app()->db->createCommand()
                    ->select('farmer_no,farmer_id,CONCAT(first_name," ",middle_name," ",surname) AS name')
                    ->from('2015_voucher_list')
                    ->where("card_write_status_id <2 AND agri_camp_id=$campid")
                    ->limit(1)
                    ->queryRow();
            $farmer_no = $farmer['farmer_no'];
            $returnedJson['FMNO'] = $farmer_no . '000000';
            $farmer_id = $farmer['farmer_id'];
            $finalFarmerNo = $farmer_no . '000000';
            $command = Yii::app()->db->createCommand();
            $command->update('2015_voucher_list', array(
                'card_write_status_id' => '1',
                    ), 'farmer_id=:id', array(':id' => $farmer_id));
            $returnedJson['FMNM'] = $farmer['name'];


            /**
             * Get farmers whose cards have been written and those who havent
             */
            $totalfarmers = 0;
            $totalfarmersWritten = 0;
            $farmersRemaining = 0;
            $farmers = Yii::app()->db->createCommand("SELECT 
    COUNT(`farmer_id`)as count FROM `2015_voucher_list` WHERE  agri_camp_id=$campid ")->queryAll();
            foreach ($farmers as $key => $value) {
                $totalfarmers = $farmers[$key]['count'];
            }
            $farmersWritten = Yii::app()->db->createCommand("SELECT 
    COUNT(`farmer_id`)as count FROM `2015_voucher_list` WHERE  card_write_status_id =2 AND agri_camp_id=$campid ")->queryAll();
            foreach ($farmersWritten as $key => $value) {
                $totalfarmersWritten = $farmersWritten[$key]['count'];
            }
            $farmersRemaining = $totalfarmers - $totalfarmersWritten;

            $returnedJson['TF'] = $totalfarmers;
            $returnedJson['TFW'] = $totalfarmersWritten;
            $returnedJson['TFR'] = $farmersRemaining;


            echo $this->separator .
            $this->startString .
            $this->separator .
            $this->next .
            $this->separator .
            CJSON::encode($returnedJson) .
            $this->separator .
            $this->endString .
            $this->separator;
        }
    }

    public function actionProcessCards() {
        $returnedJson = array();
        header('Content-type: application/json');
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $fmno = isset($_GET['FMNO']) ? $_GET['FMNO'] : false;
        $skipped = isset($_GET['SKIP']) ? $_GET['SKIP'] : false;
        $cno = isset($_GET['CNO']) ? $_GET['CNO'] : false;
        $rqst = isset($_GET['RQST']) ? $_GET['RQST'] : false;

        if (!$rqst) {
            $returnedJson['RSP'] = 'FALSE';
        } else {
            $str = $fmno;
            if (strlen($fmno) >= 10) {
                $str = substr($fmno, 0, 10);
            }
            if ($rqst == 2) {
                //$model= new Possmartcards();
                $modelsF = VoucherList2015::model()->findByAttributes(
                        array('farmer_no' => $str,
                        )
                );
                if ($modelsF) {
                    $fid = $modelsF->farmer_id;
                    if (!$skipped) {
                        $model = new Possmartcards();
                        $model->farmer_id = $fid;
                        $model->card_serial_no = $cno;
                        $model->farmer_no = $str;
                        $model->farmer_pin = '9ef0636627a60b8ac09dde4ef9dcc83781dc9bdb52d04dc20036dbd8313ed055';
                        $model->salt = '9ef0636627a60b8ac09dde4ef9dcc837';
                        $model->card_status = 1;
                        $returnedJson['FMNO'] = $str;
                        $returnedJson['FMNM'] = $modelsF->first_name . ' ' . $modelsF->middle_name . ' ' . $modelsF->surname;
                        if ($model->save()) {
                            $command = Yii::app()->db->createCommand();
                            $command->update('2015_voucher_list', array(
                                'card_write_status_id' => '2',
                                    ), 'farmer_id=:id', array(':id' => $fid));
                            $returnedJson['RSP'] = 'TRUE';
                        } else {
                            $returnedJson['RSP'] = 'FALSE';
                        }
                    } else {
                        $command = Yii::app()->db->createCommand();
                        $command->update('2015_voucher_list', array(
                            'card_write_status_id' => '3',
                                ), 'farmer_id=:id', array(':id' => $fid));
                        $returnedJson['RSP'] = 'FALSE';
                    }
                }
            } elseif ($rqst == 1) {
                if ($str === "") {
                    $returnedJson['WRITE'] = "TRUE";
                } else {
                    $modelsF = VoucherList2015::model()->findByAttributes(
                            array('farmer_no' => $str,
                            )
                    );
                    if ($modelsF) {
                        $modelP = Possmartcards::model()->findByAttributes(
                                array('farmer_id' => $modelsF->farmer_id,
                                    'card_serial_no' => $cno,
                                )
                        );
                        if ($modelP) {
                            $returnedJson['WRITE'] = "FALSE";
                            $returnedJson['INF'] = "OK";
                        } else {
                            $modelP = Possmartcards::model()->findByAttributes(
                                    array(
                                        'card_serial_no' => $cno,
                                    )
                            );
                            if ($modelP) {
                                $returnedJson['WRITE'] = "FALSE";
                                $returnedJson['INF'] = "MISMATCH";
                                $returnedJson['FMNO'] = Farmers::getfarmernom($modelP->farmer_id);
                            } else {
                                $returnedJson['WRITE'] = "TRUE";
                            }
                        }
                    }
                }
            }
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->write .
        $this->separator .
        CJSON::encode($returnedJson) .
        $this->separator .
        $this->endString .
        $this->separator;
    }

    public function actionStart() {
        $returnedJson = array();
        header('Content-type: application/json');
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $mc = isset($_GET['MC']) ? $_GET['MC'] : false;
        $version = isset($_GET['VERSION']) ? $_GET['VERSION'] : false;
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $success = 0;

        if ($version) {
            $modelVersion = Posappversions::model()->findByAttributes(
                    array('status' => 1,
                    )
            );
            if ($modelVersion) {
                $version_number = $modelVersion->version_number;
                if ($version == $version_number) {
                    $returnedJson['updateflag'] = 0;
                } else {
                    $returnedJson['updateflag'] = 1;
                    //echo Yii::app()->getBaseUrl(true);
                    $url = Yii::app()->getBaseUrl(true);
                    //  $url = str_replace('app/', '', $url);
                    $returnedJson['updateurl'] = $url . '/public/appversions/' . $modelVersion->filename;
                }
            }
        }
        // $tkid = isset($_GET['TKID']) ? $_GET['TKID'] : false;
        if (!$cmd || !$ser) {
            $returnedJson['RSP'] = 'Invalid Device';
        } else {
            $models = Posdevices::model()->findByAttributes(
                    array('imei_number' => $ser,
                    )
            );
            if (isset($models)) {
                if ($models->pos_status == 1) {
                    $returnedJson['RSP'] = 'TRUE';

                    $models_agrodealers = Agrodealers::model()->findByAttributes(
                            array('agrodealer_id' => $models->agrodealer_id,
                            )
                    );
                    $user_id = $models_agrodealers->user_id;
                    $district_id = $models_agrodealers->district_id;
                    $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
                    $lock_status = Agriseason::model()->getScalar('lock_status', '`active`=:t1', array(':t1' => 1));

                    $model_users = Users::model()->findByAttributes(
                            array('id' => $user_id,
                            )
                    );
                    $success = 1;
                    $password = $model_users->password;
                    $salt = $model_users->salt;
                    $returnedJson['DLRID'] = $models_agrodealers->agrodealer_code;
                    $returnedJson['DLRNM'] = $models_agrodealers->name;
                    $returnedJson['PASS'] = $password;
                    $returnedJson['SALT'] = $salt;
                    $returnedJson['DIST'] = $district_id;
                    $returnedJson['ASN'] = $season_id;
                    if ($lock_status == 1) {
                        $returnedJson['RDMS'] = $this->getDistrictRedeemableStatus($district_id);
                    } else {
                        $returnedJson['RDMS'] = 0;
                    }
                    $returnedJson['TKID'] = 'hfetfh6uktiug';
                } else {
                    $models_agrodealers = Agrodealers::model()->findByAttributes(
                            array('agrodealer_id' => $models->agrodealer_id,
                            )
                    );
                    $returnedJson['RSP'] = 'FALSE';
                    $returnedJson['DLRNM'] = $models_agrodealers->name;
                }
            } else {
                $returnedJson['RSP'] = 'Invalid Device';
            }
        }
        try {
            if ($ser) {
                $this->logDevice($ser, $season_id, $success);
            }
        } catch (Exception $e) {
            
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->deviceAuth .
        $this->separator .
        CJSON::encode($returnedJson) .
        $this->separator .
        $this->endString .
        $this->separator;
    }

    public function actionCsp() {
        $returnedJson = array();
        $sc = 0;
        header('Content-type: application/json');
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $cno = isset($_GET['CNO']) ? $_GET['CNO'] : false;
        $pass = isset($_GET['PASS']) ? $_GET['PASS'] : false;
        $nrc = isset($_GET['NRC']) ? $_GET['NRC'] : false;
        $fid = isset($_GET['FMID']) ? $_GET['FMID'] : false;
        $validator = false;
        if ($fid) {
            if (strlen($fid > 12)) {
                $str = substr($fid, 0, 12);
                if ($str[10] === '0' && $str[11] === '0') {
                    $str = $str = substr($fid, 0, 10);
                }

                $modelsF = Farmers::model()->findByAttributes(
                        array('farmer_no' => $str,
                        )
                );
                if ($modelsF) {
                    $fid = $modelsF->farmer_id;
                    $validator = true;
                }
            }
        }
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));

        if (!$cmd || !$cno || !$pass || !$fid || !$nrc || !$validator) {
            $returnedJson['RSP'] = 'Mismatch';
        } else {
            $models = Possmartcards::model()->findByAttributes(
                    array('card_serial_no' => $cno,
                        'farmer_id' => $fid,)
            );
            if ($models) {

                $salt = $models->salt;
                $farmer_pin = $models->farmer_pin;
                $status = $models->card_status;

                if ($salt . md5($pass) === $farmer_pin) {
                    $Farmers = Farmers::model()->findByAttributes(
                            array('farmer_id' => $fid,
                                'nrc_no' => $nrc,)
                    );
                    if ($Farmers) {
                        if ($status == 1) {
                            $returnedJson['RSP'] = 'TRUE';
                            $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
                            $models_vouchers = Vouchers::model()->findByAttributes(
                                    array('farmer_id' => $models->farmer_id,
                                        'season_id' => $season_id,
                                        'voucher_type_id' => 3
                                    )
                            );
                            if ($models_vouchers) {
                                if ($models_vouchers->voucher_status_id == 0) {
                                    $returnedJson['STATUS'] = 'redeemable';
                                    $sc = 1;
                                    $returnedJson['VOUCHERVALUE'] = $this->getVoucherValue($models_vouchers->voucher_id);
                                    $command = Yii::app()->db->createCommand();
                                    $command->update('tblpossmartcards', array(
                                        'session_status_id' => 1,
                                            ), 'card_serial_no=:id', array(':id' => $cno));
                                } else {
                                    $returnedJson['RSP'] = 'FALSE';
                                    $voucherstatus = Voucherstatus::model()->getScalar('voucher_status', '`voucher_status_id`=:t1', array(':t1' => $models_vouchers->voucher_status_id));
                                    $returnedJson['STATUS'] = $voucherstatus;
                                }
                            } else {
                                $returnedJson['RSP'] = 'No Voucher';
                            }
                            $models_farmers = Farmers::model()->findByAttributes(
                                    array('farmer_id' => $models->farmer_id,
                                    )
                            );
                            $returnedJson['NAME'] = $models_farmers->name;
                            $returnedJson['NRC'] = $models_farmers->nrc_no;
                            if (isset($models_vouchers->voucher_id)) {
                                $returnedJson['INP'] = $this->getVoucherInputs($models_vouchers->voucher_id);
                                $returnedJson['INPRED'] = $this->getRedeemedTransactions($models_vouchers->voucher_id, $ser, $cno);
                            }
                        } else {
                            $returnedJson['RSP'] = 'Inactive';
                        }
                    } else {
                        $returnedJson['RSP'] = 'PIN_INVALID';
                    }
                } else {
                    $returnedJson['RSP'] = 'PIN_INVALID';
                }
            } else {
                $returnedJson['RSP'] = 'Mismatch';
            }
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->next .
        $this->separator .
        CJSON::encode($returnedJson) .
        $this->separator .
        $this->endString .
        $this->separator;

        //log activity

        $this->logCardAuth($ser, $season_id, $cno, $fid, $sc, $nrc, $pass);
    }

    public function actionValidate() {
        $returnedJson = array();
        $inputsstring = '';
        $checker = false;
        $inputsGroup = array();
        $quantityGroup = array();
        header('Content-type: application/json');
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $cno = isset($_GET['CNO']) ? $_GET['CNO'] : false;
        $inp = isset($_GET['INP']) ? $_GET['INP'] : false;

        if (!$cmd || !$cno || !$inp) {
            $returnedJson['RSP'] = 'Enter required values';
        } else {
            $inputsarray = explode(';', $inp);

            foreach ($inputsarray as $value) {
                $inputscode = explode('=', $value);
                $input = $inputscode[0];
                if (isset($inputscode[1]) && isset($inputscode[0])) {
                    $inputsGroup[] = $input;
                    $q = ($inputscode[1]);
                    $qarray = explode(',', $q);
                    $quantity = ($qarray[0]);
                    $quantityGroup[] = $quantity;
                }
            }
            foreach ($inputsarray as $value) {
                $inputscode = explode('=', $value);
                $input = $inputscode[0];
                if (isset($inputscode[1]) && isset($inputscode[0])) {
                    $inputsstring.=$input . ',';
                    $q = ($inputscode[1]);
                    $qarray = explode(',', $q);
                    $quantity = ($qarray[0]);
                    $models = Possmartcards::model()->findByAttributes(
                            array('card_serial_no' => $cno,
                            )
                    );
                    if ($models) {
                        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
                        $models_vouchers = Vouchers::model()->findByAttributes(
                                array('farmer_id' => $models->farmer_id,
                                    'season_id' => $season_id,
                                    'voucher_type_id' => 3,
                                )
                        );

                        $return = $this->validateInputNew($input, $cno, $models_vouchers->voucher_id);
                        if ($return === 'OK') {
                            $validateQty = $this->validateQuantityNew($input, $cno, $models_vouchers->voucher_id, $quantity, $inputsGroup, $quantityGroup);
                            if ($validateQty === 'OK') {
                                $inputsstring.='OK#';
                            } else {
                                if ($validateQty === 'FULLY_REDEEMED') {
                                    $inputsstring.='FULLY_REDEEMED#';
                                    $checker = 1;
                                } else {
                                    $inputsstring.='EXCESS,' . $validateQty . '#';
                                    $checker = 1;
                                }
                            }
                        } else {
                            $checker = 1;
                            $inputsstring.='NOTALLOWED#';
                        }
                    } else {
                        $returnedJson['RSP'] = 'FALSE';
                    }
                }
            }
            if ($checker) {
                $returnedJson['RSP'] = 'FALSE';
            } else {
                $returnedJson['RSP'] = 'TRUE';
            }
            $models = Possmartcards::model()->findByAttributes(
                    array('card_serial_no' => $cno,
                    )
            );
            if ($models) {
                $returnedJson['INP'] = $inputsstring;
                $inputsNotRedeemed = $this->inputsNotRedeemed($inp, $cno);
                $list = implode(',', $inputsNotRedeemed);
                $returnedJson['MISSING'] = $list;
            } else {
                $returnedJson['RSP'] = 'FALSE';
                $returnedJson['MSG'] = 'INVALID_CARD';
            }
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->validate .
        $this->separator .
        CJSON::encode($returnedJson, JSON_UNESCAPED_SLASHES) .
        $this->separator .
        $this->endString .
        $this->separator;
    }

    public function actionSave() {
        $returnedJson = array();
        $inputsstring = '';
        $checker = false;
        $validChecker = false;
        $inputsGroup = array();
        $quantityGroup = array();
        header('Content-type: application/json');
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $cno = isset($_GET['CNO']) ? $_GET['CNO'] : false;
        $inp = isset($_GET['INP']) ? $_GET['INP'] : false;
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $success = 1;

        if (!$cmd || !$cno || !$inp || !$ser) {
            $returnedJson['RSP'] = 'FALSE';
        } else {
            $session = Yii::app()->db->createCommand()
                    ->select('session_status_id')
                    ->from('tblpossmartcards')
                    ->where("`card_serial_no`=$cno")
                    ->queryRow();
            if ($session && $session['session_status_id'] == 1) {
                $modeldevice = Posdevices::model()->findByAttributes(
                        array('imei_number' => $ser,
                        )
                );
                if ($modeldevice) {
                    $models_agrodealers = Agrodealers::model()->findByAttributes(
                            array('agrodealer_id' => $modeldevice->agrodealer_id,
                            )
                    );
                    $district_id = $models_agrodealers->district_id;
                    $modelcard = Possmartcards::model()->findByAttributes(
                            array('card_serial_no' => $cno,
                            )
                    );
                    if ($modelcard) {
                        $inputsarray = explode(';', $inp);
                        foreach ($inputsarray as $value) {
                            $inputscode = explode('=', $value);
                            $input = $inputscode[0];
                            if (isset($inputscode[1]) && isset($inputscode[0])) {
                                $inputsGroup[] = $input;
                                $q = ($inputscode[1]);
                                $qarray = explode(',', $q);
                                $quantity = ($qarray[0]);
                                $quantityGroup[] = $quantity;
                            }
                        }
                        foreach ($inputsarray as $value) {
                            $validChecker = false;
                            $inputscode = explode('=', $value);
                            $input = $inputscode[0];
                            if (isset($inputscode[1]) && isset($inputscode[0])) {
                                $inputsstring.=$input . ',';
                                $q = ($inputscode[1]);
                                $qarray = explode(',', $q);
                                $quantity = ($qarray[0]);
                                if (!is_numeric($quantity) || !is_numeric($input)) {
                                    $validChecker = 1;
                                    $inputsstring.='INVALID_NUMBER#';
                                } elseif (!$validChecker) {
                                    $models = Possmartcards::model()->findByAttributes(
                                            array('card_serial_no' => $cno,
                                            )
                                    );
                                    if ($models) {
                                        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
                                        $models_vouchers = Vouchers::model()->findByAttributes(
                                                array('farmer_id' => $models->farmer_id,
                                                    'season_id' => $season_id,
                                                    'voucher_type_id' => 3,
                                                )
                                        );
                                        $return = $this->validateInputNew($input, $cno, $models_vouchers->voucher_id);
                                        if ($return === 'OK') {
                                            $validateQty = $this->validateQuantityNew($input, $cno, $models_vouchers->voucher_id, $quantity, $inputsGroup, $quantityGroup);
                                            if ($validateQty === 'OK') {
                                                $inputsstring.='OK#';
                                            } else {
                                                $inputsstring.='EXCESS,' . $validateQty . '#';
                                                $checker = 1;
                                            }
                                        } else {
                                            $checker = 1;
                                            $inputsstring.='NOTALLOWED#';
                                        }
                                    }
                                }
                            }
                        }
                        if (!$checker) {
                            $returnedJson['RSP'] = 'FALSE';

                            $transaction = Yii::app()->db->beginTransaction();
                            try {
                                $modeldevice = Posdevices::model()->findByAttributes(
                                        array('imei_number' => $ser,
                                        )
                                );
                                $models_agrodealers = Agrodealers::model()->findByAttributes(
                                        array('agrodealer_id' => $modeldevice->agrodealer_id,
                                        )
                                );
                                $modelcard = Possmartcards::model()->findByAttributes(
                                        array('card_serial_no' => $cno,
                                        )
                                );
                                if ($modelcard) {
                                    $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
                                    $models_vouchers = Vouchers::model()->findByAttributes(
                                            array('farmer_id' => $modelcard->farmer_id,
                                                'season_id' => $season_id,
                                                'voucher_type_id' => 3,
                                            )
                                    );
                                }

                                $transactionCode = date('Ymd');
                                $modelTransaction = new Postransactions();
                                $modelTransaction->voucher_id = $models_vouchers->voucher_id;
                                $modelTransaction->tranc_code = $transactionCode;
                                $modelTransaction->imei_number = $ser;
                                $modelTransaction->agrodealer_id = $modeldevice->agrodealer_id;
                                $modelTransaction->card_serial_no = $cno;
                                $modelTransaction->farmer_id = $modelcard->farmer_id;
                                $modelTransaction->timestamp = date('Y-m-d H:i:s');

                                $timestamp = date('Y-m-d H:i:s');
                                if ($modelTransaction->save()) {
                                    $modelT = Postransactions::model()->findByPk($modelTransaction->pos_transac_id);
                                    $modelT->tranc_code = $modelT->tranc_code . $modelT->pos_transac_id;
                                    $modelT->save();
                                    $command = Yii::app()->db->createCommand();
                                    $command->update('tblpossmartcards', array(
                                        'session_status_id' => 0,
                                            ), 'card_serial_no=:id', array(':id' => $cno));
                                }
                                $inputNameArray = array();

                                $inputsarray = explode(';', $inp);
                                foreach ($inputsarray as $value) {
                                    $inputscode = explode('=', $value);
                                    $input = $inputscode[0];
                                    if (isset($inputscode[1]) && isset($inputscode[0])) {
                                        $inputName = Inputs::model()->getScalar('input_name', '`input_id`=:t1', array(':t1' => $input));
                                        $q = ($inputscode[1]);
                                        $qarray = explode(',', $q);
                                        $quantity = ($qarray[0]);
                                        $supplier = ($qarray[1]);
                                        $price = ($qarray[2]);
                                        $inputNameArray[] = $inputName . '-' . $quantity . ' Kgs';
                                        $modelRedemption = new Posinputsredemption();
                                        $modelRedemption->pos_transac_id = $modelTransaction->pos_transac_id;
                                        $modelRedemption->input_id = $input;
                                        $modelRedemption->input_qty = $quantity;
                                        $modelRedemption->supplier_id = $supplier;
                                        $modelRedemption->unit_price = $price;
                                        $modelRedemption->casu_retail_price = $this->getRetailPrice($input, $district_id);
                                        $modelRedemption->casu_wholesale_price = $this->getWholesalePrice($input, $district_id, $supplier);
                                        if ($modelRedemption->save()) {
                                            
                                        }
                                    }
                                }

                                $models_farmers = Farmers::model()->findByAttributes(
                                        array('farmer_id' => $modelcard->farmer_id,
                                        )
                                );
                                $mobile = $models_farmers->mobile;
                                $sender = $this->sender;
                                $status = 'send';
                                if ($mobile) {
                                    $modelOzeki = new Ozekimessageout();
                                    $inputsredeemed = implode(',', $inputNameArray);
                                    $message = "Dear $models_farmers->name, $models_farmers->nrc_no, you have successfully redeemed $inputsredeemed at $models_agrodealers->name agrodealer on $timestamp";
                                    $modelOzeki->sender = $sender;
                                    $modelOzeki->receiver = $mobile;
                                    $modelOzeki->msg = $message;
                                    $modelOzeki->status = $status;
                                    if ($modelOzeki->save()) {
                                        
                                    }
                                }
                                $returnedJson['RSP'] = 'TRUE';

                                $transaction->commit();
                            } catch (\Exception $e) {
                                $transaction->rollBack();
                                $returnedJson['RSP'] = 'FALSE';
                                $success = 0;
                            }
                            $returnedJson['INP'] = $inp;
                            if ($returnedJson['RSP'] !== 'FALSE') {
                                $returnedJson['TRANS'] = $modelT->tranc_code;
                                $returnedJson['VALUE'] = $this->getTransactionValue($modelTransaction->pos_transac_id);
                            }
                            $redeemed = $this->validateVouchers($models_vouchers->voucher_id);
                            $returnedJson['REDEEMED'] = 'FALSE';
                            if ($redeemed == TRUE) {
                                $modelVouchers = Vouchers::model()->findByPk($models_vouchers->voucher_id);
                                $modelVouchers->voucher_status_id = Vouchers::VOUCHER_STATUS_REEDEMED;
                                $modelVouchers->method_of_redemption = $this->redempption;
                                if ($modelVouchers->save()) {
                                    $returnedJson['REDEEMED'] = 'TRUE';
                                }
                            }
                        } else {
                            $returnedJson['RSP'] = 'FALSE';
                            $returnedJson['INP'] = $inputsstring;
                        }
                    } else {
                        $returnedJson['RSP'] = 'FALSE';
                        $returnedJson['INP'] = 'INVALID_CARD';
                    }
                } else {
                    $returnedJson['RSP'] = 'FALSE';
                    $returnedJson['INP'] = 'INVALID_IMEI';
                }
                //log this transaction
                $tra = null;
                if ($success == 1 && isset($modelT->tranc_code)) {
                    $tra = $modelT->tranc_code;
                }

                $this->logRedemption($ser, $season_id, $cno, $success, $inp, $tra);
            } else {
                $tra = null;
                $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
                $this->logRedemption($ser, $season_id, $cno, $success, $inp, $tra);
            }
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->save .
        $this->separator .
        CJSON::encode($returnedJson, JSON_UNESCAPED_SLASHES) .
        $this->separator .
        $this->endString .
        $this->separator;
    }

    public function actionTrn() {
        $returnedJson = array();
        header('Content-type: application/json');
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $cno = isset($_GET['CNO']) ? $_GET['CNO'] : false;
        $pass = isset($_GET['PASS']) ? $_GET['PASS'] : false;
        $tkid = isset($_GET['TKID']) ? $_GET['TKID'] : false;
        if (!$cmd || !$cno || !$pass || !$tkid) {
            $returnedJson['RSP'] = 'FALSE';
        } else {
            $models = Posfarmers::model()->findByAttributes(
                    array('card_number' => $cno,
                        'farmer_pin' => $pass,)
            );
            if (isset($models)) {
                $models_farmers = Farmers::model()->findByAttributes(
                        array('farmer_id' => $models->farmer_id,
                        )
                );
                if ($models_farmers->farmer_status_id == 1) {
                    $returnedJson['RSP'] = 'TRUE';
                    $returnedJson['FMID'] = $models_farmers->farmer_no;
                    $returnedJson['FDET'] = $models_farmers->name . ';' .
                            $models_farmers->nrc_no . ';' .
                            $models_farmers->mobile . ';';
                    $returnedJson['TKID'] = 'hfetfh6uktiug';
                } else {
                    $returnedJson['RSP'] = 'FALSE';
                    $returnedJson['FDET'] = 'Farmer blocked';
                }
            } else {
                $returnedJson['RSP'] = 'FALSE';
                $returnedJson['FDET'] = 'Wrong Credentials';
            }
        }
        echo $this->separator .
        $this->startString .
        $this->separator .
        $this->cardAuth .
        $this->separator .
        CJSON::encode($returnedJson) .
        $this->separator .
        $this->endString .
        $this->separator;
    }

    public function actionLogAgrodealer() {
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $code = isset($_GET['CODE']) ? $_GET['CODE'] : false;
        $success = isset($_GET['STATUS']) ? $_GET['STATUS'] : 0;
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        if ($cmd && $code && $ser) {
            $model = new Poslogagrodealerauth();
            $model->agrodealer_code = $code;
            $model->imei_number = $ser;
            $model->season_id = $season_id;
            $model->success = $success;
            $model->timestamp = date('Y-m-d h:i:s');
            $model->save();
        }
    }

    public function actionAuth() {
        header('Content-type: application/json');
        if (!isset($_GET['card']) || !isset($_GET['pin']) || !isset($_GET['agent'])) {
            echo CJSON::encode($this->error_message);
            exit;
        }
        $models = Posfarmers::model()->findByAttributes(
                array('card_number' => $_GET['card'],
                    'farmer_pin' => $_GET['pin'],)
        );
        $models_agrodealers = Agrodealers::model()->findByAttributes(
                array('agrodealer_code' => $_GET['agent'],
                )
        );

        if (isset($models) && isset($models_agrodealers)) {
            echo CJSON::encode($this->success_message);
        } else {
            echo CJSON::encode($this->error_message);
        }
    }

    public function getVoucherValue($id) {
        $sum = Yii::app()->db->createCommand()
                ->select('SUM(qty*unit_price) as sums')
                ->from('tblvrinvoiceinputs')
                ->join('tblvoucherinvoice', '(`tblvoucherinvoice`.`invoice_voucher_id`=`tblvrinvoiceinputs`.`invoice_voucher_id`)')
                ->where("`voucher_id`=$id")
                ->queryRow();
        $sum = !is_null($sum['sums']) ? $sum['sums'] : 0;

        $voucherValue = Yii::app()->db->createCommand()
                ->select('value')
                ->from('tblvrvouchers')
                ->where("`voucher_id`=$id")
                ->queryRow();
        $voucherValue = !is_null($voucherValue['value']) ? $voucherValue['value'] : 0;

        return number_format($voucherValue - $sum, 2);
    }

    public function validateInputNew($inp, $cno, $vid) {
        $inputtype = Inputs::model()->getScalar('input_type_id', '`input_id`=:t1', array(':t1' => $inp));

        $models_voucherinputs = Voucherinputs::model()->findByAttributes(
                array('voucher_id' => $vid,
                    'input_id' => $inputtype,
                )
        );
        if ($models_voucherinputs) {
            $status = 'OK';
        } else {
            $status = 'NOTALLOWED';
        }
        return $status;
    }

    public function validateQuantityNew($inp, $cno, $vid, $qty, $inputsGroup, $quantityGroup) {
        $relatedQuantity = 0;
        //$maximumquantity = 0;
        $inputtype = Inputs::model()->getScalar('input_type_id', '`input_id`=:t1', array(':t1' => $inp));
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $max = Yii::app()->db->createCommand()
                ->select('max_qty as max')
                ->from('tblvrvoucherinputs')
                ->where("`input_id`=$inputtype AND voucher_id=$vid")
                ->queryRow();
        $maximumquantity = $max['max'] ? $max['max'] : 0;

        $redeemed = Yii::app()->db->createCommand()
                ->select('SUM(input_qty) as sumq')
                ->from('tblposinputsredemption')
                ->where("`input_id` IN(SELECT input_id FROM tblsuinputs WHERE input_type_id =$inputtype) AND pos_transac_id IN "
                        . "(SELECT pos_transac_id FROM tblpostransactions WHERE voucher_id=$vid)")
                ->queryRow();
        $quantityredeemed = $redeemed['sumq'];
        $i = 0;
        foreach ($inputsGroup as $value) {
            $q = Yii::app()->db->createCommand()
                    ->select('input_id as in')
                    ->from('tblsuinputs')
                    ->where("`input_id`=$value AND input_type_id =$inputtype")
                    ->queryRow();
            if ($q) {
                $relatedQuantity = $relatedQuantity + $quantityGroup[$i];
            }
            $i++;
        }
        $qty = $relatedQuantity;
        $availableQty = $maximumquantity - $quantityredeemed;
        if ($availableQty == 0) {
            $excess = 'FULLY_REDEEMED';
        } else {
            if ($qty > $availableQty) {
                $excess = $qty - $availableQty;
            } else {
                $excess = 'OK';
            }
        }

        return $excess;
    }

    public function inputsNotRedeemed($inp, $cno) {
        $list = array();
        $inputtypeAray = array();
        $voucherinputsArray = array();
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));

        $inputsarray = explode(';', $inp);
        foreach ($inputsarray as $value) {
            $inputscode = explode('=', $value);
            $input = $inputscode[0];
            $inputtype = Inputs::model()->getScalar('input_type_id', '`input_id`=:t1', array(':t1' => $input));
            $inputtypeAray[] = $inputtype;
        }
        $models = Possmartcards::model()->findByAttributes(
                array('card_serial_no' => $cno,
                )
        );
        if ($models) {
            $models_vouchers = Vouchers::model()->findByAttributes(
                    array('farmer_id' => $models->farmer_id,
                        'season_id' => $season_id,
                        'voucher_type_id' => 3
                    )
            );
            $voucher_id = $models_vouchers->voucher_id;
            $redeemedFully = Yii::app()->db->createCommand()
                    ->select('DISTINCT(tblsuinputstype.input_type_id) as input,max_qty as max')
                    ->from('tblvrvoucherinputs')
                    ->join('tblsuinputstype', 'tblsuinputstype.input_type_id=tblvrvoucherinputs.input_id')
                    ->order('input_type_code')
                    ->where("voucher_id=$voucher_id")
                    ->queryAll();

            foreach ($redeemedFully as $value) {
                $voucherinputsArray[] = $value['input'];
            }
            $difference = array_diff($voucherinputsArray, $inputtypeAray);
            foreach ($difference as $value) {

                if (isset($value)) {
                    $myinput = $value;
                    $max = Yii::app()->db->createCommand()
                            ->select('max_qty as max')
                            ->from('tblvrvoucherinputs')
                            ->where("`input_id`=$myinput AND voucher_id=$voucher_id")
                            ->queryRow();
                    $maximumquantity = $max['max'] ? $max['max'] : 0;

                    $redeemed = Yii::app()->db->createCommand()
                            ->select('SUM(input_qty) as sumq')
                            ->from('tblposinputsredemption')
                            ->where("`input_id` IN(SELECT input_id FROM tblsuinputs WHERE input_type_id =$myinput) AND pos_transac_id IN "
                                    . "(SELECT pos_transac_id FROM tblpostransactions WHERE voucher_id=$voucher_id)")
                            ->queryRow();
                    $quantityredeemed = $redeemed['sumq'] ? $redeemed['sumq'] : 0;

                    if ($quantityredeemed < $maximumquantity) {
                        $list[] = $myinput . '#' . ($maximumquantity - $quantityredeemed);
                    }
                }
            }

            return $list;
        }
    }

    public function validateVouchers($voucher_id) {
        $inputs = Yii::app()->db->createCommand()
                ->select('DISTINCT(tblsuinputstype.input_type_id) as input,max_qty as max')
                ->from('tblvrvoucherinputs')
                ->join('tblsuinputstype', 'tblsuinputstype.input_type_id=tblvrvoucherinputs.input_id')
                ->where("voucher_id=$voucher_id")
                ->queryAll();
        foreach ($inputs as $value) {
            $input_type_id = $value['input'];
            $max_quantity = $value['max'];
            $redeemed = Yii::app()->db->createCommand()
                    ->select('SUM(input_qty) as sumq')
                    ->from('tblposinputsredemption')
                    ->where("`input_id` IN(SELECT input_id FROM tblsuinputs WHERE input_type_id =$input_type_id) AND pos_transac_id IN "
                            . "(SELECT pos_transac_id FROM tblpostransactions WHERE voucher_id=$voucher_id)")
                    ->queryRow();
            $quantityredeemed = $redeemed['sumq'];
            if ($quantityredeemed < $max_quantity) {
                return FALSE;
            }
        }
        return TRUE;
    }

    /**
     * Loog device authorisation to server
     * @param type $imei_no
     * @param type $season_id
     * @param type $success
     */
    public function logDevice($imei_no, $season_id, $success) {
        $model = new Poslogdevauth();
        $model->imei_number = $imei_no;
        $model->season_id = $season_id;
        $model->success = $success;
        $model->timestamp = date('Y-m-d h:i:s');
        $model->save();
    }

    public function logCardAuth($imei_no, $season_id, $serial_no, $fid, $sc, $nrc, $pass) {
        //echo $imei_no,',',$season_id,',',$serial_no,',',$fid;
        try {
            $modelPos = Posdevices::model()->findByAttributes(
                    array('imei_number' => $imei_no,
            ));
            if ($modelPos) {
                $models = Possmartcards::model()->findByAttributes(
                        array('card_serial_no' => $serial_no,
                            'farmer_id' => $fid,)
                );
                $salt = $models->salt;
                $cpin = $models->farmer_pin;
                $pin = $salt . md5($pass);

                $farmer_no = Farmers::model()->getScalar('farmer_no', '`farmer_id`=:t1', array(':t1' => $fid));
                $cnrc = Farmers::model()->getScalar('nrc_no', '`farmer_id`=:t1', array(':t1' => $fid));
                $agrodealer_code = Agrodealers::model()->getScalar('agrodealer_code', '`agrodealer_id`=:t1', array(':t1' => $modelPos->agrodealer_id));
                $model = new Poslogcardauth();
                //$model->pos_card_swiped = 1;
                $model->imei_number = $imei_no;
                $model->season_id = $season_id;
                $model->card_serial_no = $serial_no;
                if ($farmer_no) {
                    $model->farmer_no = $farmer_no;
                }
                $model->agrodealer_code = $agrodealer_code;
                $model->submitted_nrc = $nrc;
                $model->correct_nrc = $cnrc;
                $model->submitted_pin = $pin;
                $model->correct_pin = $cpin;
                $model->timestamp = date('Y-m-d h:i:s');
                $model->success = $sc;

                $model->save();
            }
        } catch (Exception $e) {
            
        }
    }

    /**
     * save to redemptionlogs
     * 
     * @param type $imei_no
     * @param type $season_id
     * @param type $serial_no
     * @param type $voucher_id
     * @param type $fid
     * @param type $success
     * @param type $tranc_code
     */
    public function logRedemption($imei_no, $season_id, $serial_no, $success, $inputs, $tranc_code = null) {
        $modelredemptiontrials = new Poslogredemptiontrials();

        $modelPos = Posdevices::model()->findByAttributes(
                array('imei_number' => $imei_no,
        ));
        if ($modelPos) {
            $modelCards = Possmartcards::model()->findByAttributes(
                    array('card_serial_no' => $serial_no,
                    )
            );
            if ($modelCards) {
                $farmer_no = Farmers::model()->getScalar('farmer_no', '`farmer_id`=:t1', array(':t1' => $modelCards->farmer_id));
                $agrodealer_code = Agrodealers::model()->getScalar('agrodealer_code', '`agrodealer_id`=:t1', array(':t1' => $modelPos->agrodealer_id));
                $models_vouchers = Vouchers::model()->findByAttributes(
                        array('farmer_id' => $modelCards->farmer_id,
                            'season_id' => $season_id,
                            'voucher_type_id' => 3,
                        )
                );
                if ($models_vouchers) {
                    $modelredemptiontrials->season_id = $season_id;
                    $modelredemptiontrials->card_serial_no = $serial_no;
                    $modelredemptiontrials->voucher_no = $models_vouchers->voucher_no;
                    $modelredemptiontrials->imei_number = $imei_no;
                    $modelredemptiontrials->farmer_no = $farmer_no;
                    $modelredemptiontrials->agrodealer_code = $agrodealer_code;
                    $modelredemptiontrials->tranc_code = $tranc_code;
                    if (is_null($tranc_code)) {
                        $success = 0;
                    }
                    $modelredemptiontrials->timestamp = date('Y-m-d h:i:s');
                    $modelredemptiontrials->success = $success;

                    if ($modelredemptiontrials->save()) {
                        $inputsarray = explode(';', $inputs);
                        foreach ($inputsarray as $value) {
                            $inputscode = explode('=', $value);
                            $input_id = $inputscode[0];
                            $q = ($inputscode[1]);
                            $qarray = explode(',', $q);
                            $quantity = ($qarray[0]);
                            $modelinputredemptions = new Posloginputredetrials();
                            $modelinputredemptions->pos_redemp_id = $modelredemptiontrials->pos_redemp_id;
                            $modelinputredemptions->input_id = $input_id;
                            $modelinputredemptions->qty = $quantity;
                            $modelinputredemptions->save();
                        }
                    }
                }
            }
        }
    }

    public function actionPrint() {
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $type = isset($_GET['TYPE']) ? $_GET['TYPE'] : false;
        $transaction_code = isset($_GET['TRANS']) ? $_GET['TRANS'] : false;
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $modelPos = Posdevices::model()->findByAttributes(
                array('imei_number' => $ser,
        ));
        if ($modelPos) {
            $agrodealer_code = Agrodealers::model()->getScalar('agrodealer_code', '`agrodealer_id`=:t1', array(':t1' => $modelPos->agrodealer_id));
            $model = new Poslogprinting();
            $model->season_id = $season_id;
            $model->imei_number = $ser;
            $model->agrodealer_code = $agrodealer_code;
            $model->timestamp = date('Y-m-d h:i:s');
            $model->print_type = $type;
            $model->transac_code = $transaction_code;

            $model->save();
        }
    }

    public function actionSwipe() {
        $cmd = isset($_GET['CMD']) ? $_GET['CMD'] : false;
        $ser = isset($_GET['SER']) ? $_GET['SER'] : false;
        $cno = isset($_GET['CNO']) ? $_GET['CNO'] : false;
        $fmno = isset($_GET['FMNO']) ? $_GET['FMNO'] : false;

        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $modelPos = Posdevices::model()->findByAttributes(
                array('imei_number' => $ser,
        ));
        if ($modelPos) {
            $agrodealer_code = Agrodealers::model()->getScalar('agrodealer_code', '`agrodealer_id`=:t1', array(':t1' => $modelPos->agrodealer_id));
            $model = new Poslogcardswiped();
            $model->season_id = $season_id;
            $model->imei_number = $ser;
            $model->agrodealer_code = $agrodealer_code;
            $model->timestamp = date('Y-m-d h:i:s');
            $model->farmer_no = $fmno;
            $model->card_serial_no = $cno;
            $model->save();
        }
    }

    public function getWholesalePrice($input_id, $district_id, $supplier_id) {
        $q = Yii::app()->db->createCommand()
                ->select('wholesale_price')
                ->from('tblsusuppliersdistrict')
                ->where("`input_id`=$input_id AND district_id=$district_id AND supplier_id=$supplier_id")
                ->queryRow();
        if (isset($q['wholesale_price'])) {
            return $q['wholesale_price'];
        }
        return 0;
    }

    public function getRetailPrice($input_id, $district_id) {
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $q = Yii::app()->db->createCommand()
                ->select('recommended_price')
                ->from('tblsuagrodealerinputprices')
                ->where("`input_id`=$input_id AND district_id=$district_id AND season_id=$season_id")
                ->queryRow();
        if (isset($q['recommended_price'])) {
            return $q['recommended_price'];
        }
        return 0;
    }

    public function getTransactionValue($transactionId) {
        $q = Yii::app()->db->createCommand()
                ->select('SUM(casu_retail_price*input_qty) as sum')
                ->from('tblposinputsredemption')
                ->where("`pos_transac_id`=$transactionId")
                ->queryRow();
        if (isset($q['sum'])) {
            return $q['sum'];
        }
        return 0;
    }

    public function getVoucherInputs($voucher_id) {
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        $inputsListArray = array();
        $voucherInputs = Yii::app()->db->createCommand()
                ->select('DISTINCT(tblsuinputstype.input_type_id) as input,max_qty as max')
                ->from('tblvrvoucherinputs')
                ->join('tblsuinputstype', 'tblsuinputstype.input_type_id=tblvrvoucherinputs.input_id')
                ->order('input_type_code')
                ->where("voucher_id=$voucher_id")
                ->queryAll();
        foreach ($voucherInputs as $value) {
            $input_type_id = $value['input'];
            $max_quantity = $value['max'];
            $redeemed = Yii::app()->db->createCommand()
                    ->select('SUM(input_qty) as sumq')
                    ->from('tblposinputsredemption')
                    ->where("`input_id` IN(SELECT input_id FROM tblsuinputs WHERE input_type_id =$input_type_id) AND pos_transac_id IN "
                            . "(SELECT pos_transac_id FROM tblpostransactions WHERE voucher_id=$voucher_id)")
                    ->queryRow();
            $quantityredeemed = $redeemed['sumq'];

            if ($max_quantity > $quantityredeemed) {
                $inputsListArray[] = $input_type_id . ',' . ($max_quantity - $quantityredeemed) . ',' . $this->getUnitOfMeasure($input_type_id);
            }
        }
        return implode('#', $inputsListArray);
    }

    public function getRedeemedTransactions($voucher_id, $ser, $serial_no) {

        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));

        $inputsListArray = array();
        $st = '';
        $modeldevice = Posdevices::model()->findByAttributes(
                array('imei_number' => $ser,
                )
        );
        if ($modeldevice) {
            $agrodealer_code = Agrodealers::model()->getScalar('agrodealer_code', '`agrodealer_id`=:t1', array(':t1' => $modeldevice->agrodealer_id));
            $agrodealer_name = Agrodealers::model()->getScalar('name', '`agrodealer_id`=:t1', array(':t1' => $modeldevice->agrodealer_id));
            $transactions = Yii::app()->db->createCommand()
                    ->select('pos_transac_id ,tranc_code,timestamp')
                    ->from('tblpostransactions')
                    ->where("voucher_id=$voucher_id AND  agrodealer_id=$modeldevice->agrodealer_id AND card_serial_no=$serial_no")
                    ->queryAll();
            foreach ($transactions as $value) {
                $printed = 0;
                $inputs_items = array();
                $pos_transac_id = $value['pos_transac_id'];
                $tranc_code = $value['tranc_code'];
                $timestamp = $value['timestamp'];
                $t_value = $this->getTransactionValue($pos_transac_id);
                $modelVouchers = Vouchers::model()->findByPk($voucher_id);
                $v_status = 0;
                if ($modelVouchers->voucher_status_id == Vouchers::VOUCHER_STATUS_REEDEMED) {
                    $v_status = 1;
                }


                $print = Yii::app()->db->createCommand()
                        ->select('pos_print_id')
                        ->from('tblposlogprinting')
                        ->where("transac_code=$tranc_code")
                        ->queryAll();
                if ($print) {
                    $printed = 1;
                }
                $st = $tranc_code . '$' . $agrodealer_code . '$' . $agrodealer_name . '$' . $timestamp . '$' . $printed . '$' . $t_value . '$' . $v_status . '$';

                $inputs = Yii::app()->db->createCommand()
                        ->select('input_id ,input_qty')
                        ->from('tblposinputsredemption')
                        ->where("pos_transac_id=$pos_transac_id")
                        ->queryAll();
                foreach ($inputs as $valueInputs) {
                    $input_type_id = Inputs::model()->getScalar('input_type_id', '`input_id`=:t1', array(':t1' => $valueInputs['input_id']));

                    $inputs_items[] = $valueInputs['input_id'] . ',' . $valueInputs['input_qty'] . ',' . $this->getUnitOfMeasure($input_type_id);
                }
                $inputsListArray[] = $st . '' . implode('#', $inputs_items);
            }
        }
        return implode('&', $inputsListArray);
    }

    public function getUnitOfMeasure($input_type_id) {
        if (!is_null($input_type_id)) {
            $unit = Tblsuinputstype::model()->getScalar('unit_of_measure_id', '`input_type_id`=:t1', array(':t1' => $input_type_id));
            if ($unit) {
                $unitOfMeasure = Inputsunitofmeasure::model()->getScalar('unit_of_measure', '`unit_of_measure_id`=:t1', array(':t1' => $unit));
                if ($unitOfMeasure) {
                    return $unitOfMeasure;
                }
            }
        }
        return '';
    }

    public function getDistrictRedeemableStatus($district_id) {
        $season_id = Agriseason::model()->getScalar('season_id', '`active`=:t1', array(':t1' => 1));
        if ($season_id) {
            $status = Yii::app()->db->createCommand()
                    ->select('lock_status')
                    ->from('tblposdistrictsredeeming')
                    ->where("`district_id`=$district_id AND season_id=$season_id ")
                    ->queryRow();
            if ($status) {
                $status_id = $status['lock_status'];
                return $status_id;
            }
        }
        return 0;
    }

    public function actionGeneratePins() {
        $digits = 4;
        $models = Possmartcards8275::model()->findAll();
        foreach ($models as $toSave) {
            $mynum = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
            $toSave->temp_pin = $mynum;
            $toSave->salt = md5(microtime());
            $toSave->farmer_pin = $toSave->salt . md5($mynum);
            $toSave->save();
        }
    }

}
