<?php

/**
 * Settings default controller
 * @author Joakim <kimoduor@gmail.com>
 */
class DataController extends SettingsModuleController {

    public function init() {

        $this->resourceLabel = Lang::t('Data');
// $this->resource = DistributorsModuleConstants::RES_DISTIBUTORS;
        $this->activeTab = 'data';
        parent::init();
    }

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array('rates'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('posts', 'messages', 'candidates', 'voters', 'votes', 'totalvotes'),
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionPosts() {
        $models = Posts::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['post_id'] = $model->post_id;
            $array['postname'] = $model->postname;
            $array['code'] = $model->code;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionMessages() {
        $condition = "1=1";
        $user_id = Yii::app()->user->id;
        if (isset($_GET['mymessages'])) {
            $condition = "touser_id=$user_id"; //0-new, 1unread,2read
        }
        $models = Messages::model()->findAll(array('condition' => $condition));
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionCandidates() {
        $models = Candidates::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['candidate_id'] = $model->candidate_id;
            $array['fname'] = $model->fname;
            $array['lname'] = $model->lname;
            $array['code'] = $model->code;
            $array['post_id'] = $model->post_id;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionVoters() {
        $models = Voters::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['voter_id'] = $model->voter_id;
            $array['fname'] = $model->fname;
            $array['lname'] = $model->lname;
            $array['idno'] = $model->idno;
            $array['pin'] = $model->pin;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionVotes() {
        $models = Votes::model()->findAll();
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['vote_id'] = $model->vote_id;
            $array['candidate_id'] = $model->candidate_id;
            $array['voter_id'] = $model->voter_id;
            $array['success'] = $model->success;
            $array['post_id'] = Candidates::model()->get($model->candidate_id, "post_id");
            $array['datetime'] = $model->datetime;
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

    public function actionTotalvotes() {
        $models = Candidates::model()->findAll(array("order"=>"post_id"));
        $output = array();
        foreach ($models as $model) {
            $array = array();
            $array['candidate_id'] = $model->candidate_id;
            $array['post_id'] = $model->post_id;            
            $array['total'] = Votes::model()->count("candidate_id='$model->candidate_id' AND candidate_id IN (SELECT candidate_id FROM candidates WHERE post_id=$model->post_id)");
            $output[] = $array;
        }
        $theoutput = json_encode($output);
        echo $theoutput;
    }

}
