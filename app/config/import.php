<?php

/**
 * stores the import configurations
 * @author Fredrick <mconyango@gmail.com>
 */
return array(
    'application.extensions.*',
    'application.models.*',
    'application.models.forms.*',
    'application.components.*',
    'application.components.widgets.*',
    'ext.easyimage.EasyImage',
    //user module
    'application.modules.users.models.*',
    'application.modules.users.components.*',
    'application.modules.users.components.behaviors.*',
    //settings module
    'application.modules.settings.models.*',
    'application.modules.settings.components.*',
    //auth module
    'application.modules.auth.models.*',
    'application.modules.auth.components.*',
    'application.modules.auth.forms.*',
    //tcpf module
    'application.modules.tcpf.*',
//
    'application.modules.votex.components.*',
);
