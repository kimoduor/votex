<?php

/**
 * stores the database configurations
 * @author joakim <kimoduor@gmail.com>
 * @since 1.0
 * @version 1.0
 */
//development
return array(
    'connectionString' => 'mysql:host=localhost;port=3306;dbname=votex1',

    'emulatePrepare' => true,
    'username' => 'root',
    'password' => '',
    'schemaCachingDuration' => 600,
    'tablePrefix' => '',
    'enableParamLogging' => false,
    'enableProfiling' => false,
    'charset' => 'utf8',
    'nullConversion' => PDO::NULL_EMPTY_STRING,
    'initSQLs' => array("set time_zone='+03:00';SET sql_mode = 'NO_ZERO_DATE';"),
);