<?php

/**
 * Description of MyGridPie
 *
 * @author mconyango <mconyango@gmail.com>
 */
class MyGridPieChart extends CWidget
{

    /**
     *
     * @var type
     */
    public $modelClass;

    /**
     *
     * @var type
     */
    public $id;

    /**
     *
     * @var type
     */
    public $filters = array();

    /**
     * htmlOptions of the pie container
     */
    public $htmlOptions = array('class' => 'grid-piechart');

    /**
     *
     */
    private $series;

    public function init()
    {
        $this->htmlOptions['id'] = 'gridPieChart_' . $this->id;
        parent::init();
    }

    public function run()
    {
        $data = HighChartsDataProvider::getData($this->modelClass, array('filters' => $this->filters), array('graphType' => HighChartsHelper::GRAPH_PIE, 'title' => null));
        $this->series = $data['series'];
        echo CHtml::tag('div', $this->htmlOptions, "");
        $this->registerScripts();
    }

    public function registerScripts()
    {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $assets_url = Yii::app()->assetManager->publish($assets, false, -1, YII_DEBUG ? true : null);
        Yii::log($this->htmlOptions['id']);
        Yii::app()->clientScript
                ->registerScriptFile($assets_url . '/highcharts-4.0.3/js/highcharts.js', CClientScript::POS_END)
                ->registerScriptFile($assets_url . '/gridPieChart.js', CClientScript::POS_END)
                ->registerScript('MyGridPieChart-' . $this->id, "MyGridPieChart.init('" . $this->htmlOptions['id'] . "'," . json_encode($this->series) . ")");
    }

}
