/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var MyGridPieChart = {
    optionsDefault: {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null, //null,
            plotShadow: false,
            backgroundColor: 'transparent',
            spacingTop: 3,
        },
        credits: {
            enabled: false,
        },
        title: {
            text: null,
        },
        tooltip: {
            pointFormat: '<b>{point.percentage:.0f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                size: 80,
                dataLabels: {
                    enabled: false,
                }
            }
        },
    },
    init: function(container, series, options) {
        'use strict';
        var settings = $.extend({}, this.optionsDefault, options || {});
        $('#' + container).highcharts({
            chart: settings.chart,
            credits: settings.credits,
            title: settings.title,
            tooltip: settings.tooltip,
            plotOptions: settings.plotOptions,
            series: series,
        });
    },
};


