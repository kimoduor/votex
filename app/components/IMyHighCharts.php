<?php

/**
 * All models implementing hicharts should have the following methods
 */
interface IMyHighCharts
{

    /**
     * @param string $graph_type @link http://api.highcharts.com/highcharts#chart.type
     * <pre>
     *  X,Y axis based graphs:
     *      array(
     *                  array(
     *                              "name"=>"Males",
     *                              "condition"=>"`gender`=:gender",
     *                              "params"=>array(":gender"=>"male"))),
     *                  array(
     *                              "name"=>"Females",
     *                              "condition"=>"`gender`=:gender",
     *                              "params"=>array(":gender"=>"female")))
     *           )
     * </pre>
     * <pre>
     * Series for PIE CHART
     *  array(
     *          array(
     *                 'data' => array(
     *                   array(
     *                     'name' =>"Males",
     *                      'condition' => '`gender`=:gender',
     *                      'params' => array(':gender' =>"Male"),
     *                   ),
     *                 array(
     *                      'name' => "Females",
     *                      'condition' => '`gender`=:gender',
     *                      'params' => array(':gender' =>"Females"),
     *              ),
     *      )
     *   )
     * );
     * </pre>
     * @param array $queryOptions e.g array('filters'=>array(),'condition'=>'','date_field'=>'date_created','sum'=>null),
     * @return array series.
     */
    public function highChartsSeriesOptions($graph_type, $queryOptions);
}
