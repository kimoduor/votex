<?php

/**
 *  AddressManager class. All models for address should inherit this class
 * @property string $id
 * @property string $address_name
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property string $fax
 * @property string $postal_code
 * @property string $city_id
 * @property string $country_id
 * @property integer $preferred
 * @property string $date_created
 * @property string $created_by
 *
 * @author JoAKIM <kimoduor@gmail.com>
 */
abstract class AddressManager extends ActiveRecord
{

    /**
     * The route to redirect to after saving the address.
     */
    const AFTERSAVE_GO_TO_ROUTE_GET_PARAM = 'route';

    /**
     *
     */
    const ADDRESS_ID_GET_PARAM = 'address_id';

    /**
     * Parent id field of the owner class of the address
     * @var type
     */
    public $parent_id_field;

    /**
     *
     * @var type
     */
    public $cityName;

    public function rules()
    {
        return array(
            array('address_name', 'required'),
            array('email', 'email', 'message' => Lang::t('Please enter a valid email address.')),
            array('preferred', 'numerical', 'integerOnly' => true),
            array('address_name', 'length', 'max' => 30),
            array('address', 'length', 'max' => 255),
            array('phone,fax,postal_code', 'length', 'max' => 20),
            array('email', 'length', 'max' => 128),
            array('city_id, country_id,cityName', 'safe'),
        );
    }

    /**
     * Get preferred address model.
     * Returns model or if null creates a new model.
     * @param type $parent_id
     * @param boolean $return_model default is true
     * @return mixed \AddressManager or array
     */
    public function getPreferredAddress($parent_id = null, $return_model = true)
    {
        $conditions = '`preferred`=1';
        $params = array();
        if (!empty($parent_id)) {
            $conditions .=' AND `' . $this->parent_id_field . '`=:t1';
            $params[':t1'] = $parent_id;
        }
        if ($return_model)
            $address = $this->find($conditions, $params);
        else {
            $address = $this->getRow('*', $conditions, $params);
        }
        if (($return_model && null === $address) || empty($address)) {
            $conditions = '';
            $params = array();
            if (!empty($parent_id)) {
                $conditions.='`' . $this->parent_id_field . '`=:t1';
                $params[':t1'] = $parent_id;
            }
            if ($return_model)
                $address = $this->find($conditions, $params);
            else
                $address = $this->getRow('*', $conditions, $params);

            if ($return_model && null === $address) {
                $class_name = $this->getClassName();
                $address = new $class_name();
                $address->{$this->parent_id_field} = $parent_id;
            }
        }
        return $address;
    }

    /**
     * Get preferred_address_id
     * @param type $parent_id
     * @return type
     */
    public function getPreferredAddressId($parent_id = null)
    {
        $conditions = '`preferred`=1';
        $params = array();
        if (!empty($parent_id)) {
            $conditions .=' AND `' . $this->parent_id_field . '`=:t1';
            $params[':t1'] = $parent_id;
        }
        return $this->getScalar('id', $conditions, $params);
    }

    public function beforeSave()
    {
        $this->setAsPreferred();
        $this->addCity();
        return parent::beforeSave();
    }

    public function afterFind()
    {
        $this->cityName = SettingsCity::model()->get($this->city_id, 'name');
        return parent::afterFind();
    }

    protected function setAsPreferred()
    {
        if ($this->preferred) {
            $conditions = '';
            $params = array();
            if (!empty($this->parent_id_field)) {
                $conditions .="`{$this->parent_id_field}`=:t1";
                $params[':t1'] = $this->{$this->parent_id_field};
            }
            Yii::app()->db->createCommand()
                    ->update($this->tableName(), array('preferred' => 0), $conditions, $params);
        }
    }

    protected function addCity()
    {
        if (!empty($this->cityName) && !SettingsCity::model()->exists('`name`=:t1 AND `country_id`=:t2', array(':t1' => $this->cityName, ':t2' => $this->country_id))) {
            $this->city_id = SettingsCity::model()->addCity($this->cityName, $this->country_id);
        }
    }

    public function attributeLabels()
    {
        return array(
            'address_name' => Lang::t('Address Name'),
            'address' => Lang::t('Postal Address'),
            'postal_code' => Lang::t('Post Code'),
            'phone' => Lang::t('Telephone'),
            'email' => Lang::t('Email'),
            'fax' => Lang::t('Fax'),
            'city_id' => Lang::t('City'),
            'country_id' => Lang::t('Country'),
        );
    }

}
