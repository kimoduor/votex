<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newUrlManager
 *
 * @author Robot
 */
class newUrlManager extends CUrlManager {

    const GET_PARAM_RETURN_URL = 'r_url';

    public $showScriptName = false;
    public $appendParams = false;

    public function createUrl($route, $params = array(), $ampersand = '&') {
        $route = preg_replace_callback('/(?<![A-Z])[A-Z]/', function($matches) {
            return '-' . lcfirst($matches[0]);
        }, $route);

        $route = $this->encrypt_decrypt('encrypt', $route);
        return parent::createUrl($route, $params, $ampersand);
    }

    public function parseUrl($request) {
        $route = parent::parseUrl($request);
        $route = $this->encrypt_decrypt('decrypt', $route);
        return lcfirst(str_replace(' ', '', ucwords(str_replace('-', ' ', $route))));
    }
    function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = 'This is my secret key';
        $secret_iv = 'This is my secret iv';

        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    /**
     * Get return link
     * @param type $url
     * @return type
     */
    public static function getReturnUrl($url = NULL) {
        $rl = filter_input(INPUT_GET, self::GET_PARAM_RETURN_URL);
        $url = $this->encrypt_decrypt('encrypt', url);
        return !empty($rl) ? $rl : $url;
    }

}


