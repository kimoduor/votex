<?php

/**
 * This class is mainly used for defining system constants that is not associated to any particular model
 * @author JoAKIM <kimoduor@gmail.com>
 */
class Constants {

    //active record constants
    const LABEL_CREATE = 'Add';
    const LABEL_UPDATE = 'Edit';
    const LABEL_CANCEL = 'Cancel Voucher';
    const LABEL_REDEEM = 'Make Voucher Redeemable';
    const LABEL_ACTION = 'Actions';
    const LABEL_DELETE = 'Delete';
    const LABEL_AGRODEALER_INVOICE_APPROVE = 'Approve';
    const LABEL_VIEW = 'View details';
    const LABEL_VIEW_SHORT = 'View';
    const LABEL_FARMER_PRIVILEGES = 'Farmer Privileges';
    const LABEL_AGRO_PRIVILEGES = 'Agro Dealer Privileges';
    const LABEL_VOUCHER_PRIVILEGES = 'Voucher Privileges';
    const LABEL_INVOICE_PRIVILEGES = 'Invoice Privileges';
    const LABEL_SUPPLIER_PRIVILEGES = 'Supplier Privileges';
    const LABEL_INPUT_PRIVILEGES = 'Input Privileges';
    const LABEL_REPORT_PRIVILEGES = 'Reports Privileges';
    const LABEL_SMS_PRIVILEGES = 'SMS Privileges';
    const LABEL_GEAR_PRIVILEGES = 'Setup Privileges';
    //miscelleneous constants
    const SPACE = ' ';
 //   const RANDMOBILE=Farmers::getsmsnumber;
    

}
