<?php

/**
 * PersonHelper class has helper function for person attributes e.g date of birth etc
 *
 * @author Joakim <kimoduor@gmail.com>
 */
class PersonHelper
{

    //gender constants
    const GENDER_MALE = '1';
    const GENDER_FEMALE = '2';

    /**
     * Get gender options
     * @return array Gender Options
     */
    public static function genderOptions()
    {
        return array(
            self::GENDER_MALE => Lang::t('Male'),
            self::GENDER_FEMALE => Lang::t('Female'),
        );
    }

    /**
     *
     * @param type $gender
     * @return type
     */
    public static function decodeGender($gender)
    {
        switch ($gender) {
            case self::GENDER_MALE:
                return Lang::t('Male');
            case self::GENDER_FEMALE:
                return Lang::t('Female');
            default :
                return $gender;
        }
    }

    /**
     * Birth date year options
     * @return type
     */
    public static function birthDateYearOptions()
    {
        $min_year = 10;
        $max_year = 100;
        $current_year = date('Y');
        $years = array('' => Lang::t('Year'));

        for ($i = $min_year; $i <= $max_year; $i++) {
            $year = $current_year - $i;
            $years[$year] = $year;
        }

        return $years;
    }

    /**
     * Birth date month options
     * @return type
     */
    public static function birthDateMonthOptions()
    {
        return array(
            '' => Lang::t('Month'),
            1 => Lang::t('Jan'),
            2 => Lang::t('Feb'),
            3 => Lang::t('Mar'),
            4 => Lang::t('Apr'),
            5 => Lang::t('May'),
            6 => Lang::t('Jun'),
            7 => Lang::t('Jul'),
            8 => Lang::t('Aug'),
            9 => Lang::t('Sep'),
            10 => Lang::t('Oct'),
            11 => Lang::t('Nov'),
            12 => Lang::t('Dec'),
        );
    }

    /**
     * Birthdate day options
     * @return type
     */
    public static function birthDateDayOptions()
    {
        $days = array('' => Lang::t('Day'));
        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = $i;
        }
        return $days;
    }

}
