<?php

/**
 *
  * @author JoAKIM <kimoduor@gmail.com>
 */
class CountryCityHelper
{

    /**
     * Join country and city as one string to represent a person's location
     * @param integer $country_id
     * @param integer $city_id
     * @param string $template
     * @return string
     */
    public static function joinCountryCity($country_id, $city_id, $template = '{city}-{country}')
    {
        if (empty($country_id) && empty($city_id))
            return null;
        if (empty($country_id))
            $template = '{city}';
        if (empty($city_id))
            $template = '{country}';
        $country = SettingsCountry::model()->get($country_id, 'name');
        $city = SettingsCity::model()->get($city_id, 'name');
        return strtr($template, array('{city}' => $city, '{country}' => $country));
    }

}
