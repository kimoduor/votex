<?php

//review
class Utility {

    public static function imploze($array, $key, $pieces) {
        $iArray = array();
        if (is_array($array) && count($array) > 0) {
            foreach ($array as $row) {
                $iArray[] = $row[$key];
            }
        }
        return implode($iArray, $pieces);
    }

    public static function getDeviceProfile() {
        $brand = Yii::app()->session['brand'];
        $model = Yii::app()->session['model'];
        $deviceProfile = Device::model()->getDeviceProfile($brand, $model);

        return $deviceProfile;
    }

    /**
     * Get column specified by langAlias
     * @param object Object want to get column
     * @param key Key will be parsed
     */
    public static function t($object, $key) {
        $lang = Yii::app()->getLanguage();

        $langAlias = Yii::app()->params['langAlias'];
        $alias = isset($langAlias[$lang]) ? $langAlias[$lang] : '';
        if ($alias) {
            $key = ($alias) ? $key . '_' . $langAlias[$lang] : $key;
        }

        return $object->$key;
    }

    public static function clean($string) {
        $string = str_replace(" ", "-", $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        return preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one.
    }

    /**
     * Safe input from user
     * @param input input from user
     * @param objectType type of object will be return
     */
    public static function safeInput($input, $objectType = 'string') {
        $p = new CHtmlPurifier();
        $input = trim($input);
        if ($objectType == 'int')
            return intval($input);
        else if ($objectType == 'string') {
            $str = $p->purify(htmlspecialchars(strip_tags(stripslashes($input))));
            return $str;
        }
        return $input;
    }

    public static function friendlyTime($date) {
        $period = strtotime(date('Y-m-d H:i:s')) - strtotime(date('Y-m-d H:i:s', strtotime($date)));

        if ($period < 60)
            return $period . ' ' . Yii::t('wap', 'second_ago');
        else {
            $minute = round($period / 60);
            if ($minute < 60)
                return $minute . ' ' . Yii::t('wap', 'minute_ago');
            else {
                $hour = round($minute / 60);
                if ($hour < 24)
                    return $hour . ' ' . Yii::t('wap', 'hour_ago');
                else {
                    $day = round($hour / 24);
                    if ($day < 7)
                        return $day . ' ' . Yii::t('wap', 'day_ago');
                    else {
                        $week = round($day / 7);
                        if ($week < 4)
                            return $week . ' ' . Yii::t('wap', 'week_ago');
                        else {
                            return round('d/m/Y H:i', strtotime($date));
                            /* $month = ceil($week / 4);
                              if ($month < 12)
                              echo $month. ' month ago';
                              else {
                              $year = ceil($month / 12);
                              echo $year. ' year ago';
                              } */
                        }
                    }
                }
            }
        }
    }

    public static function secondToTime($s) {
        $hour = floor($s / 3600);
        $minute = floor(($s - $hour * 3600) / 60);
        $second = $s - $hour * 3600 - $minute * 60;

        if ($hour == 0) {
            $hour = '';
        } else if ($hour < 10) {
            $hour = '0' . $hour . ':';
        } else {
            $hour = $hour . ':';
        }

        if ($minute == 0) {
            $minute = '00:';
        } else if ($minute < 10) {
            $minute = '0' . $minute . ':';
        } else {
            $minute = $minute . ':';
        }

        if ($second == 0) {
            $second = '00';
        } else if ($second < 10) {
            $second = '0' . $second;
        }

        return $hour . $minute . $second;
    }

    public static function timeFormat($time) {
        $datetime = date_parse($time);

        return $datetime['day'] . '/' . $datetime['month'] . '/' . $datetime['year'];
    }

    public static function truncate($string, $len = 40, $dots = TRUE) {
        //return $string;
        $retVal = $string;

        /*
         * get current encoding:
         * "auto" is expanded to: ASCII,JIS,UTF-8,EUC-JP,SJIS
         */

        $encoding = mb_detect_encoding($string, "auto");

        // leng of string in current encoding
        $strlen = mb_strlen($string, $encoding);

        $delta = $strlen - $len;
        if ($delta > 0) {
            $shortText = "";

            // trim it by length in current encoding
            $shortText = mb_substr($string, 0, $len, $encoding);

            // find the last break word
            $breakPos = $len;
            $breakPatten = array(" ", ",", ".", ":", "_", "-", "+");
            foreach ($breakPatten as $id => $breakKey) {
                if (mb_strrpos($shortText, $breakKey, $encoding)) {
                    if ($id == "0") {
                        $breakPos = mb_strrpos($shortText, $breakKey, $encoding);
                    } else {
                        $breakPos = ( $breakPos > mb_strrpos($shortText, $breakKey, $encoding) ) ? $breakPos : mb_strrpos($shortText, $breakKey, $encoding);
                    }
                }
            }

            //remove break word
            $shortText = mb_substr($shortText, 0, $breakPos, $encoding);

            if ($dots)
                $shortText .= "...";

            $retVal = $shortText;
        }

        return $retVal;
    }

    public static function makePagingUrl($params) {
        $re = null;

        if ($params) {
            foreach ($params AS $param => $value) {
                $re .= $param . '=' . $value . '&';
            }
        }

        return $re;
    }

    public static function makeClipThumb($thumb, $link) {
        if (eregi('iphone', $_SERVER['HTTP_USER_AGENT']) || eregi('ipod', $_SERVER['HTTP_USER_AGENT'])) { //for iphone
            // iPhone
            $html = '<embed src="' . $thumb . '" href="' . $link . '" width="104" height="78" type="video/mp4" autoplay="false" target="myself" scale="1" align="left" class="thumb" controller="false" cache="true" />';
        } else {
            // Others
            $html = '<a href="' . $link . '"><img src="' . $thumb . '" width="104" height="78" alt="thumbnail" /></a>';
        }

        return $html;
    }

    // in a helper file
    public static function yiiParam($name, $default = null) {
        if (isset(Yii::app()->params[$name]))
            return Yii::app()->params[$name];
        else
            return $default;
    }

    public static function sendEmail($to, $subject = '[LiveTV]-Your password', $message = '', $cc = array()) {
        require_once "Mail.php";
        $from = "TVViet <no-reply@gmail.com>";
        if ($to)
            $pieces = array_merge($cc, array($to));
        else
            $pieces = $cc;

        $to = implode(', ', $pieces);
        # die($to);
        $subject = $subject;
        $body = $message;

        $host = Yii::app()->params['mail_live']['host'];

        $username = Yii::app()->params['mail_live']['user'];
        $password = Yii::app()->params['mail_live']['pass'];

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject
        );
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password));

        $mail = $smtp->send($to, $headers, $body);

        if (PEAR::isError($mail)) {
            echo($mail->getMessage());
            return false;
        }
        return true;
    }

    /*
     *  Ham bo dau tieng Viet
     */

    public static function unicode2Anscii($str) {
        $str = str_replace(array('á', 'à', 'ả', 'ã', 'ạ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ'), 'a', $str);
        $str = str_replace(array('é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ'), 'e', $str);
        $str = str_replace(array('í', 'ì', 'ỉ', 'ĩ', 'ị'), 'i', $str);
        $str = str_replace(array('ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ'), 'o', $str);
        $str = str_replace(array('ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ'), 'u', $str);
        $str = str_replace(array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ'), 'y', $str);
        $str = str_replace(array('Á', 'À', 'Ả', 'Ã', 'Ạ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ'), 'A', $str);
        $str = str_replace(array('É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ'), 'E', $str);
        $str = str_replace(array('Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'), 'I', $str);
        $str = str_replace(array('Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ'), 'O', $str);
        $str = str_replace(array('Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ'), 'U', $str);
        $str = str_replace(array('Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'), 'Y', $str);
        $str = str_replace(array('đ', 'Đ'), array('d', 'D'), $str);

        return $str;
    }

    public static function composite2Unicode($str) {
        $chars_utf8 = array(
            array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),
            array('ế', 'ề', 'ể', 'ễ', 'ệ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),
            array('í', 'ì', 'ỉ', 'ĩ', 'ị', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),
            array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),
            array('ứ', 'ừ', 'ử', 'ữ', 'ự', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),
            array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),
            array('đ', 'Đ'),
        );

        $chars_unicode = array(
            array('ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'á', 'à', 'ả', 'ã', 'ạ', 'â', 'ă', 'Á', 'À', 'Ả', 'Ã', 'Ạ', 'Â', 'Ă'),
            array('ế', 'ề', 'ể', 'ễ', 'ệ', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê'),
            array('í', 'ì', 'ỉ', 'ĩ', 'ị', 'Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị'),
            array('ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'Ố', 'Ồ', 'Ổ', 'Ô', 'Ộ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ơ', 'Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ơ'),
            array('ứ', 'ừ', 'ử', 'ữ', 'ự', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư'),
            array('ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ'),
            array('đ', 'Đ'),
        );
        for ($i = 0; $i < count($chars_utf8); ++$i)
            for ($j = 0; $j < count($chars_utf8[$i]); ++$j) {
                $str = str_replace($chars_unicode[$i][$j], $chars_utf8[$i][$j], $str);
            }
        return $str;
    }

    public static function parseDate($dateRange) {
        $dateRange = str_replace('+', ' ', $dateRange);
        $dates = explode(' - ', $dateRange);

        $startDate = $dates[0];
        $endDate = isset($dates[1]) ? $dates[1] : $dates[0];
        $startDate = preg_replace('/([\d]+)[^\d]([\d]+)[^\d]([\d]+)/', '$3-$2-$1', $startDate);
        $endDate = preg_replace('/([\d]+)[^\d]([\d]+)[^\d]([\d]+)/', '$3-$2-$1', $endDate);

        return array('start' => $startDate, 'end' => $endDate);
    }

    public static function calTimeFormat($date) {
        $startDate = $date['start'];
        $endDate = $date['end'];
        if (date('d-m', strtotime($startDate)) == date('d-m', strtotime($endDate)))
            return '%H:00';
        elseif (date('Y', strtotime($startDate)) == date('Y', strtotime($endDate)))
            return '%d-%m';
        else
            return '%d-%m-%Y';
    }

    public static function stripText($string) {
        $from = array("à", "ả", "ã", "á", "ạ", "ă", "ằ", "ẳ", "ẵ", "ắ", "ặ", "â", "ầ", "ẩ", "ẫ", "ấ", "ậ", "đ", "è", "ẻ", "ẽ", "é", "ẹ", "ê", "ề", "ể", "ễ", "ế", "ệ", "ì", "ỉ", "ĩ", "í", "ị", "ò", "ỏ", "õ", "ó", "ọ", "ô", "ồ", "ổ", "ỗ", "ố", "ộ", "ơ", "ờ", "ở", "ỡ", "ớ", "ợ", "ù", "ủ", "ũ", "ú", "ụ", "ư", "ừ", "ử", "ữ", "ứ", "ự", "ỳ", "ỷ", "ỹ", "ý", "ỵ", "À", "Ả", "Ã", "Á", "Ạ", "Ă", "Ằ", "Ẳ", "Ẵ", "Ắ", "Ặ", "Â", "Ầ", "Ẩ", "Ẫ", "Ấ", "Ậ", "Đ", "È", "Ẻ", "Ẽ", "É", "Ẹ", "Ê", "Ề", "Ể", "Ễ", "Ế", "Ệ", "Ì", "Ỉ", "Ĩ", "Í", "Ị", "Ò", "Ỏ", "Õ", "Ó", "Ọ", "Ô", "Ồ", "Ổ", "Ỗ", "Ố", "Ộ", "Ơ", "Ờ", "Ở", "Ỡ", "Ớ", "Ợ", "Ù", "Ủ", "Ũ", "Ú", "Ụ", "Ư", "Ừ", "Ử", "Ữ", "Ứ", "Ự", "Ỳ", "Ỷ", "Ỹ", "Ý", "Ỵ");
        $to = array("a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "d", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "e", "i", "i", "i", "i", "i", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "o", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "u", "y", "y", "y", "y", "y", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "A", "D", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "E", "I", "I", "I", "I", "I", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "O", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "U", "Y", "Y", "Y", "Y", "Y");
        return str_replace($from, $to, $string);
    }

    public static function rewrite($string) {
        $string = Utility::stripText($string);
        $string = strtolower(trim($string));
        $string = preg_replace('/[^a-z0-9-]/', '-', $string);
        $string = preg_replace('/-+/', "-", $string);
        return $string;
    }

    public static function encryptAES($plain_text) {
        $cipher = "rijndael-128";
        $mode = "cbc";

        $secret_key = "f5D9dgd0kRK1ske5";
        $iv = "fedcba9876543210";
        $td = mcrypt_module_open($cipher, "", $mode, $iv);
        mcrypt_generic_init($td, $secret_key, $iv);
        $cyper_text = mcrypt_generic($td, $plain_text);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return base64_encode($cyper_text);
    }

    public static function decryptAES($text) {
        $cipher = "rijndael-128";
        $mode = "cbc";
        $secret_key = "f5D9dgd0kRK1ske5";
        $iv = "fedcba9876543210";

        $td = mcrypt_module_open($cipher, "", $mode, $iv);
        //_va "td: ".$td."<br/>";
        mcrypt_generic_init($td, $secret_key, $iv);

        $baseText = base64_decode($text);
        //echo "BasedText: ". $baseText. "<br/>";
        $decrypted_text = mdecrypt_generic($td, $baseText);
        //echo "Decrypted Text: ".$decrypted_text."<br/>";
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);

        return trim($decrypted_text);
    }

    public static function countDayBetween($fromDate, $toDate) {
        $t1 = strtotime($fromDate);
        $t2 = strtotime($toDate);
        return intval(($t2 - $t1) / 86400);
    }

    public static function displayTime($time, $timeUnit) {
        switch ($timeUnit) {
            case 'hour':
                return date('h a', strtotime($time));
                break;
            case 'day':
                return date('d/m/Y', strtotime($time));
                break;
            case 'week':
                return date('d/m/Y', strtotime($time));
                break;
            case 'month':
                return date('m/Y', strtotime($time));
                break;
            case 'year':
                return date('Y', strtotime($time));
                break;
        }
    }

    public static function dateAfter($date, $count) {
        return date('Y-m-d', strtotime($date) + 86400 * $count);
    }

    public static function timeDiff($fromDate, $toDate, $timeUnit) {
        switch ($timeUnit) {
            case 'hour':
                return intval((strtotime($toDate) - strtotime($fromDate)) / 3600);
                break;
            case 'day':
                return intval((strtotime($toDate) - strtotime($fromDate)) / 86400);
                break;
            case 'week':
                return intval((strtotime($toDate) - strtotime($fromDate)) / 604800);
                break;
            case 'month':
                return intval((strtotime($toDate) - strtotime($fromDate)) / 2592000);
                break;
            case 'year':
                return intval((strtotime($toDate) - strtotime($fromDate)) / 31536000);
                break;
        }
    }

    public static function timeAdd($time, $count, $timeUnit) {
        switch ($timeUnit) {
            case 'hour':
                return date('Y-m-d H:i:s', strtotime($time) + 3600 * $count);
                break;
            case 'day':
                return date('Y-m-d H:i:s', strtotime($time) + 86400 * $count);
                break;
            case 'week':
                return date('Y-m-d H:i:s', strtotime($time) + 604800 * $count);
                break;
            case 'month':
                return date('Y-m-d H:i:s', strtotime($time) + 2592000 * $count);
                break;
            case 'year':
                return date('Y-m-d H:i:s', strtotime($time) + 31536000 * $count);
                break;
        }
    }

    public static function displayTimeUnit($timeUnit) {
        switch ($timeUnit) {
            case 'hour':
                return 'giờ';
                break;
            case 'day':
                return 'ngày';
                break;
            case 'week':
                return 'tuần';
                break;
            case 'month':
                return 'tháng';
                break;
            case 'year':
                return 'năm';
                break;
        }
    }

    public static function exportToExcel($results, $headers, $fileName, $d = 10) {
        require_once 'Spreadsheet/Excel/Writer.php';
        $workbook = new Spreadsheet_Excel_Writer();

        //set filename
        $workbook->send($fileName);

        //set version
        $workbook->setVersion(8);

        //add a worksheet
        $worksheet = & $workbook->addWorksheet($fileName);

        $worksheet->setInputEncoding('utf-8');

        $format = & $workbook->addFormat();
        // $format->setBold();
        $format->setAlign('center');

        //Write header
        $iRow = 0;

        for ($i = 0; $i < $d; $i++) {
            $c = $headers[$i];
            $worksheet->write($iRow, $i, $c, $format);
        }
        $iRow++;

        /* data */

        foreach ($results as $oneResult) {
            $keys = array_keys($oneResult);
            for ($i = 0; $i < $d; $i++) {
                $c = $oneResult[$keys[$i]];
                $worksheet->write($iRow, $i, $c, $format);
            }
            $iRow++;
        }

        $workbook->close();
    }

    /**
     *
     * @param string $imgSrc
     * @param array $arrSize
     * @param string $imgTarget
     * @return mixed
     */
    public static function cropImage_bk($imgSrc, $arrSize, $imgTarget = null) {
        $thumbnail_width = $arrSize[0];
        $thumbnail_height = $arrSize[1];

        if (!$imgTarget)
            $imgTarget = $imgSrc;

        $newThumb = Utility::__getThumbnailResource($imgSrc, $thumbnail_width, $thumbnail_height);
        $ext = substr($imgTarget, -4);

        if ($ext == '.gif') {
            imagegif($newThumb, $imgTarget);
        } elseif ($ext == '.jpg') {
            imagejpeg($newThumb, $imgTarget);
        } elseif ($ext == '.png') {
            imagepng($newThumb, $imgTarget);
        } else {
            return false;
        }

        #die($imgTarget);
        return $imgTarget;
    }

    public static function genPassword() {
        $char = array('c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y');
        $max = 6;
        $pass = '';
        for ($i = 0; $i < 6; $i++) {
            $rand = rand(10, 99);
            $pos = $rand % count($char);
            if ($pos < 6)
                $pass .= $pos;
            else
                $pass .= $char[$pos];

            if (strlen($pass) == 6)
                break;
        }

        return $pass;
    }

    public static function formatName($msisdn, $firstName, $lastName) {
        $retStr = $firstName . ' ' . $lastName;
        if (!trim($retStr)) {
            $msisdn = substr($msisdn, 0, strlen($msisdn) - 3);
            $msisdn = substr($msisdn, 2);
            $retStr = '0' . $msisdn . 'xxx';
        }
        return $retStr;
    }

    public static function generatePlayChannelImage($img, $play, $target) {
        $info = getimagesize($img);
        $mime = $info['mime'];
        $type = substr(strrchr($mime, '/'), 1);
        $image_types = array('jpeg', 'png', 'gif', 'bmp');
        if (!in_array($type, $image_types))
            $type = 'Jpeg';
        $img_create_func = 'ImageCreateFrom' . $type;

        $dest = $img_create_func($img);
        $src = imagecreatefromjpeg($play);
        $playW = imagesx($src);
        $playH = imagesy($src);

        $dH = $info[1] - $playH;
        //_play.jpg
        imagecopymerge($dest, $src, 0, $dH, 0, 0, $playW, $playH, 100);
        imagejpeg($dest, $target);
        imagedestroy($dest);
        imagedestroy($src);
    }

    public static function cropImage($src, $size, $target = null, $keepBorder = true) {
        if ($target == null) {
            $name = basename($src);
            $pos = strrpos($name, '.');
            $name = ($pos === false) ? $name : substr($name, 0, $pos);
            $target = $name . '_' . implode('x', $size) . '.jpg';
        }
        $info = GetImageSize($src);
        $width = $info[0];
        $height = $info[1];
        /* if ($width == $size[0] && $height == $size[1]) {
          return true;
          } */


        $mime = $info['mime'];
        $type = substr(strrchr($mime, '/'), 1);
        $image_types = array('jpeg', 'png', 'gif', 'bmp');
        if (!in_array($type, $image_types))
            $type = 'Jpeg';


        if ($keepBorder) {
            Utility::resizeImage($type, $src, $target, $size[0], $size[1]);
            return true;
        }

        $img_create_func = 'ImageCreateFrom' . $type;
        list($w, $h) = $size;

        $scale_x = $width / $w;
        $scale_y = $height / $h;

        $scale = min($scale_x, $scale_y);
        // Kich thuoc anh crop
        $w *= $scale;
        $h *= $scale;
        $w = intval($w);
        $h = intval($h);




        $src = $img_create_func($src);
        $dst = ImageCreateTrueColor($w, $h);

        $src_x = ($width - $w) / 2;
        $src_y = ($height - $h) / 2;

        // Crop
        imagecopy($dst, $src, 0, 0, $src_x, $src_y, $w, $h);

        // Resize
        $res = ImageCreateTrueColor($size[0], $size[1]);
        ImageCopyResampled($res, $dst, 0, 0, 0, 0, $size[0], $size[1], $w, $h);
        imagejpeg($res, $target, 100); //gif give better quatility
        imagedestroy($src);
        imagedestroy($dst);
        return $target;
    }

    public static function resizeImage($ext, $tmpname, $target, $xmax, $ymax) {
        $ext = strtolower($ext);

        if ($ext == "jpg" || $ext == "jpeg")
            $im = imagecreatefromjpeg($tmpname);
        elseif ($ext == "png")
            $im = imagecreatefrompng($tmpname);
        elseif ($ext == "gif")
            $im = imagecreatefromgif($tmpname);

        $x = imagesx($im);
        $y = imagesy($im);

        if ($x <= $xmax && $y <= $ymax)
            return $im;

        if ($x >= $y) {
            $newx = $xmax;
            $newy = $newx * $y / $x;
        } else {
            $newy = $ymax;
            $newx = $x / $y * $newy;
        }

        $im2 = imagecreatetruecolor($newx, $newy);
        imagecopyresampled($im2, $im, 0, 0, 0, 0, floor($newx), floor($newy), $x, $y);
        imagejpeg($im2, $target);
        imagedestroy($im2);
        imagedestroy($im);
    }

    public static function getRoundRobin($url, $index) {
        $url = explode(';', $url);
        $total = sizeof($url);
        if (($index + 1) >= $total) {
            return array('url' => trim($url[0]), 'rrb' => 0);
        }
        return array('url' => trim($url[$index + 1]), 'rrb' => $index + 1);
    }

    public static function makeThumbnailChannel($item) {
        if ($item)
            return Yii::app()->params['thumbs_channel']['url'] . $item;
        else
            return Yii::app()->params['thumbs_channel']['url'] . 'logo.png';
    }

    public static function makeClientDownloadLink($item) {
        return Yii::app()->params['thumbs_channel']['client'] . $item;
    }

    public static function makeThumbnailVideo($item) {
        return Yii::app()->params['vod']['url'] . $item;
    }

    public static function makeVideoStreaming($item) {
        return Yii::app()->params['vod']['url'] . $item;
    }

    public static function makeThumbnailVod($item) {
        return Yii::app()->params['video']['streaming_url'] . $item->FILENAME . $item->THUMBNAIL_VERSION . '.jpg';
    }

    public static function makeVodStreaming($item) {
        return Yii::app()->params['video']['streaming_url'] . $item->FILENAME . '.mp4';
    }

    public static function getTokenUrlStreaming($item, $method = 'PC') {
        $user = isset(Yii::app()->user->id) ? Yii::app()->user->id : '';
        $method = strtoupper($method);
        $expired = Yii::app()->params['publish']['time_expired'];
        $key = Yii::app()->params['publish']['key'];
        $ip = $_SERVER['REMOTE_ADDR'];

        $md5 = base64_encode(md5($key . $ip . $expired, true));
        $md5 = strtr($md5, '+/', '-_');
        $md5 = str_replace('=', '', $md5);
        $output = $method . '_OUTPUT';
        $rrb = 'RRB_' . $method;
        $rrbs = Utility::getRoundRobin($item->$output, $item->$rrb);
        $url = $rrbs['url'] . "?token=$md5&e=$expired&u=$user";
        if ($rrb) {
            $item->$rrb = $rrbs['rrb'];
            $item->save(false);
        }
        return $url;
    }

    public static function getTokenUrlStreaming_web($item, $method = 'PC') {
        $user = isset(Yii::app()->user->id) ? Yii::app()->user->id : '';
        $method = strtoupper($method);
        $expired = Yii::app()->params['publish']['time_expired'];
        $key = Yii::app()->params['publish']['key'];
        $ip = $_SERVER['REMOTE_ADDR'];

        $md5 = base64_encode(md5($key . $ip . $expired, true));
        $md5 = strtr($md5, '+/', '-_');
        $md5 = str_replace('=', '', $md5);
        $output = $method . '_OUTPUT';
        $rrb = 'RRB_' . $method;
        $rrbs = Utility::getRoundRobin($item->$output, $item->$rrb);
        $url = $rrbs['url'] . "?token=$md5&e=$expired&u=$user";
        if ($rrb) {
            $item->$rrb = $rrbs['rrb'];
            $item->save(false);
        }
        /*
          $file = "/tmp/live.log";
          $handle = fopen($file, "a");
          fwrite($handle, "\n $md5 | $ip | $item->NAME | $expired | $user\n");
         */
        if (Yii::app()->session['is_mobile']) {
            if ($method == 'HLS')
                $url .= '&channel=' . $item->NAME;
        }
        return $url;
    }

    public static function getProfileViaOS() {
        if (isset(Yii::app()->session['os']))
            return Yii::app()->session['os'];
        $detect = new Mobile_Detect();
        $format = 'PC';
        if ($detect->isAndroidOS())
            $format = "HLS";
        else if ($detect->isiOS())
            $format = "HLS";
        else if ($detect->isSymbianOS())
            $format = "RTSP";
        else if ($detect->isNokiaOS())
            $format = "RTSP";
        else if ($detect->isWindowsMobileOS())
            $format = "HLS";
        else if ($detect->isBlackBerryOS())
            $format = "SS";
        else if ($detect->isBREWOS())
            $format = "SS";
        else if ($detect->isTablet())
            $format = "HLS";
        Yii::app()->session['os'] = strtoupper($format);
        return strtoupper($format);
    }

    public static function makeChannelThumb($thumb, $link) {
        if (isset($_SERVER['HTTP_USER_AGENT']) && (strstr($_SERVER['HTTP_USER_AGENT'], 'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPod') || strstr($_SERVER['HTTP_USER_AGENT'], 'iPad'))) {
            // iPhone
            $html = '<embed src="' . $thumb . '" href="' . $link . '" width="60" height="45" type="video/mp4" autoplay="false" target="myself" scale="1" align="left" class="thumb" controller="false" cache="true" />';
        } else {
            // Others
            $html = '<a href="' . $link . '"><img src="' . $thumb . '" width="60" height="45" alt="thumbnail" /></a>';
        }
        return $html;
    }

    public static function last_monday($date) {
        if (!is_numeric($date))
            $date = strtotime($date);
        if (date('w', $date) == 1)
            return date('Y-m-d', $date);
        else
            return date('Y-m-d', strtotime('last monday', $date));
    }

    public static function last_sunday($date) {
        if (!is_numeric($date))
            $date = strtotime($date);
        if (date('w', $date) == 0)
            return date('Y-m-d', $date);
        else
            return date('Y-m-d', strtotime('This Sunday', $date));
    }

    public static function stripContent($str) {
        if (is_array($str)) {
            foreach ($str as &$item) {
                $item = Utility::stripContent($item);
            }
        } else {
            $stripArr = array(
                'onblur', 'onchange', 'alert', 'onclick', 'ondblclick', 'onfocus', 'onmousedown', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onkeydown', 'onkeypress', 'onkeyup', 'onselect'
            );
            foreach ($stripArr as $tag) {
                $str = str_replace($tag, '', $str);
            }
            $str = is_int($str) ? intval($str) : Utility::safeInput($str);
        }
        return $str;
    }

    /*
     * XSS filter
     *
     * This was built from numerous sources
     * (thanks all, sorry I didn't track to credit you)
     *
     * It was tested against *most* exploits here: http://ha.ckers.org/xss.html
     * WARNING: Some weren't tested!!!
     * Those include the Actionscript and SSI samples, or any newer than Jan 2011
     *
     *
     * TO-DO: compare to SymphonyCMS filter:
     * https://github.com/symphonycms/xssfilter/blob/master/extension.driver.php
     * (Symphony's is probably faster than my hack)
     */

    public static function xss_clean($data) {
        // Fix &entity\n;
        $data = str_replace(array('&amp;', '&lt;', '&gt;'), array('&amp;amp;', '&amp;lt;', '&amp;gt;'), $data);
        $data = preg_replace('/(&#*\w+)[\x00-\x20]+;/u', '$1;', $data);
        $data = preg_replace('/(&#x*[0-9A-F]+);*/iu', '$1;', $data);
        $data = html_entity_decode($data, ENT_COMPAT, 'UTF-8');

        // Remove any attribute starting with "on" or xmlns
        $data = preg_replace('#(<[^>]+?[\x00-\x20"\'])(?:on|xmlns)[^>]*+>#iu', '$1>', $data);

        // Remove javascript: and vbscript: protocols
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`\'"]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2nojavascript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:#iu', '$1=$2novbscript...', $data);
        $data = preg_replace('#([a-z]*)[\x00-\x20]*=([\'"]*)[\x00-\x20]*-moz-binding[\x00-\x20]*:#u', '$1=$2nomozbinding...', $data);

        // Only works in IE: <span style="width: expression(alert('Ping!'));"></span>
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?expression[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?behaviour[\x00-\x20]*\([^>]*+>#i', '$1>', $data);
        $data = preg_replace('#(<[^>]+?)style[\x00-\x20]*=[\x00-\x20]*[`\'"]*.*?s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*+>#iu', '$1>', $data);

        // Remove namespaced elements (we do not need them)
        $data = preg_replace('#</*\w+:\w[^>]*+>#i', '', $data);

        do {
            // Remove really unwanted tags
            $old_data = $data;
            $data = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $data);
        } while ($old_data !== $data);

        // we are done...
        return $data;
    }

    public static function filterContentPostGet(&$str) {
        if (is_array($str)) {
            foreach ($str as &$item) {
                Utility::filterContentPostGet($item);
            }
        } else
            Utility::xss_clean(strtolower($str));
    }

    public static function AntiSQLInjection($cautruyvan) {
        $cautruyvan = strtolower($cautruyvan);
        //'admin_' :tam thoi bo;
        $tukhoa = array('chr(', 'chr=', 'chr%20', '%20chr', 'wget%20', '%20wget', 'wget(',
            'cmd=', '%20cmd', 'cmd%20', 'rush=', '%20rush', 'rush%20',
            'union%20', '%20union', 'union(', 'union=', 'echr(', '%20echr', 'echr%20', 'echr=',
            'esystem(', 'esystem%20', 'cp%20', '%20cp', 'cp(', 'mdir%20', '%20mdir', 'mdir(',
            'mcd%20', 'mrd%20', 'rm%20', '%20mcd', '%20mrd', '%20rm',
            'mcd(', 'mrd(', 'rm(', 'mcd=', 'mrd=', 'mv%20', 'rmdir%20', 'mv(', 'rmdir(',
            'chmod(', 'chmod%20', '%20chmod', 'chmod(', 'chmod=', 'chown%20', 'chgrp%20', 'chown(', 'chgrp(',
            'locate%20', 'grep%20', 'locate(', 'grep(', 'diff%20', 'kill%20', 'kill(', 'killall',
            'passwd%20', '%20passwd', 'passwd(', 'telnet%20', 'vi(', 'vi%20',
            'insert%20into', 'select%20', 'nigga(', '%20nigga', 'nigga%20', 'fopen', 'fwrite', '%20like', 'like%20',
            '$_request', '$_get', '$request', '$get', '.system', 'HTTP_PHP', '&aim', '%20getenv', 'getenv%20',
            'new_password', '&icq', '/etc/password', '/etc/shadow', '/etc/groups', '/etc/gshadow',
            'HTTP_USER_AGENT', 'HTTP_HOST', '/bin/ps', 'wget%20', 'uname\x20-a', '/usr/bin/id',
            '/bin/echo', '/bin/kill', '/bin/', '/chgrp', '/chown', '/usr/bin', 'g\+\+', 'bin/python',
            'bin/tclsh', 'bin/nasm', 'perl%20', 'traceroute%20', 'ping%20', '.pl', '/usr/X11R6/bin/xterm', 'lsof%20',
            '/bin/mail', '.conf', 'motd%20', 'HTTP/1.', '.inc.php', 'config.php', 'cgi-', '.eml',
            'file\://', 'window.open', '<SCRIPT>', 'javascript\://', 'img src', 'img%20src', '.jsp', 'ftp.exe',
            'xp_enumdsn', 'xp_availablemedia', 'xp_filelist', 'xp_cmdshell', 'nc.exe', '.htpasswd',
            'servlet', '/etc/passwd', 'wwwacl', '~root', '~ftp', '.js', '.jsp', '.history',
            'bash_history', '.bash_history', '~nobody', 'server-info', 'server-status', 'reboot%20', 'halt%20',
            'powerdown%20', '/home/ftp', '/home/www', 'secure_site, ok', 'chunked', 'org.apache', '/servlet/con',
            '<script', '/robot.txt', '/perl', 'mod_gzip_status', 'db_mysql.inc', '.inc', 'select%20from',
            'select from', 'drop%20', '.system', 'getenv', 'http_', '_php', 'php_', 'phpinfo()', '<?php', '?>', 'sql=');

        $kiemtra = str_replace($tukhoa, '*', $cautruyvan);
        if ($cautruyvan != $kiemtra) {
            $cremotead = $_SERVER['REMOTE_ADDR'];
            $cuseragent = $_SERVER['HTTP_USER_AGENT'];
            die("Phat hien co su tan cong! <br /><br /><b>Viec tan cong nay da bi ngan chan:</b><br />$cremotead - $cuseragent");
        }
    }

    public static function getSchedulePlaying($schdules) {
        if ($schdules) {
            $count = sizeof($schdules);
            for ($i = 0; $i < $count; $i++) {
                if ($i < $count - 1) {
                    if (strtotime($schdules[$i]->STARTTIME) > strtotime(date('Y-m-d H:i:s'))) {
                        return $schdules[$i - 1]->PROGRAMNAME;
                    }
                } else {
                    return $schdules[$i]->PROGRAMNAME;
                }
                ++$index;
            }
        }
        return '';
    }

    public static function getSkypeStatus($username) {
        $data = file_get_contents('http://mystatus.skype.com/' . urlencode($username) . '.xml');
        return strpos($data, '<presence xml:lang="en">Offline</presence>') ? 'Offline' : 'Online';
    }

    public static function getLanguageContentOfColumn($object, $column, $isObject = 1) {
        $lang = Yii::app()->language;
        $column = ($lang == 'en') ? $column .= '_EN' : $column;
        return ($isObject) ? $object->$column : $object[$column];
    }

    public static function renderPlayer($clip, $width = 448, $height = 372) {
        $videoUrl = Utility::makeVodStreaming($clip);

        $previewUrl = self::makeThumbnailVod($clipId);

        $html = "<script type='text/javascript' src='/js/admin/swfobject.js'></script>
        <script type=\"text/javascript\">
            swfobject.registerObject(\"player\",\"9.0.98\",\"/swf/expressInstall.swf\");
        </script>

        <object type=\"application/x-shockwave-flash\" data=\"/swf/player.swf\" width=\"$width\" height=\"$height\">
            <param name=\"movie\" value=\"/swf/player-viral.swf\" />
            <param name=\"allowfullscreen\" value=\"true\" />
            <param name=\"allowscriptaccess\" value=\"always\" />
            <param name=\"wmode\" value=\"transparent\" />
            <param name=\"flashvars\" value=\"file=$videoUrl&image=$previewUrl&skin=/swf/modieus.swf\" />
            <p><a href=\"http://get.adobe.com/flashplayer\">Get Flash</a> to see this player.</p>
        </object>";

        return $html;
    }

    public static function getThumbnailList($clip) {
        $thumbs = array();
        $baseURL = Yii::app()->params['video']['streaming_url'] . $clip->FILENAME;

        for ($i = 1; $i < 21; $i++) {
            $thumbnailURL = "$baseURL" . $i . ".jpg";
            //version of thumb
            $thumb['name'] = $i;
            $thumb['url'] = $thumbnailURL;
            $thumbs[] = $thumb;
        }

        return $thumbs;
    }

}

?>
