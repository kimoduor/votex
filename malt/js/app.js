/***
 Muskey AngularJS App Main Script
 ***/

/* Muskey App */
var MuskeyApp = angular.module("MuskeyApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MuskeyApp.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            cssFilesInsertBefore: 'ng_load_plugins_before' // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
        });
    }]);

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
 *********************************************/
//AngularJS v1.3.x workaround for old style controller declarition in HTML
MuskeyApp.config(['$controllerProvider', function($controllerProvider) {
        // this option might be handy for migrating old apps, but please don't use it
        // in new ones!
        $controllerProvider.allowGlobals();
    }]);

/* Setup global settings */
MuskeyApp.factory('settings', ['$rootScope', function($rootScope) {
        // supported languages
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            layoutImgPath: Muskey.getAssetsPath() + 'admin/layout/img/',
            layoutCssPath: Muskey.getAssetsPath() + 'admin/layout/css/'
        };

        $rootScope.settings = settings;

        return settings;
    }]);

/* Setup App Main Controller */
MuskeyApp.controller('AppController', ['$scope', '$rootScope', function($scope, $rootScope) {
        $scope.$on('$viewContentLoaded', function() {
            Muskey.initComponents(); // init core components
            //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
        });
    }]);

/***
 Layout Partials.
 By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
 initialization can be disabled and Layout.init() should be called on page load complete as explained above.
 ***/

/* Setup Layout Part - Header */
MuskeyApp.controller('HeaderController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Layout.initHeader(); // init header
        });
    }]);

/* Setup Layout Part - Sidebar */
MuskeyApp.controller('SidebarController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Layout.initSidebar(); // init sidebar
        });
    }]);

/* Setup Layout Part - Sidebar */
MuskeyApp.controller('PageHeadController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Demo.init(); // init theme panel
        });
    }]);

/* Setup Layout Part - Footer */
MuskeyApp.controller('FooterController', ['$scope', function($scope) {
        $scope.$on('$includeContentLoaded', function() {
            Layout.initFooter(); // init footer
        });
    }]);

/* Setup Rounting For All Pages */
MuskeyApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        // Redirect any unmatched url
        $urlRouterProvider.otherwise("/dashboard");

        $stateProvider

                // Dashboard

                .state("dashboard", {
                    url: "/dashboard",
                    templateUrl: "default/dashboard",
                    data: {pageTitle: 'Welcome', pageSubTitle: ''}
                })
                              .state("profile", {
                    url: "/profile",
                    templateUrl: "auth/profile",
                    data: {pageTitle: 'My Profile', pageSubTitle: ''}
                })
               
                .state("messages", {
                    url: "/messages",
                    templateUrl: "messages",
                    data: {pageTitle: 'Messages', pageSubTitle: ''}
                })
                 .state("mymessages", {
                    url: "/mymessages",
                    templateUrl: "messages/mymessages",
                    data: {pageTitle: 'Messages', pageSubTitle: ''}
                })
                .state('users', {
                    url: '/users',
                    templateUrl: "users",
                    data: {pageTitle: 'users', pageSubTitle: 'Manage Users'},
                })
                .state('posts', {
                    url: '/posts',
                    templateUrl: "votex/posts",
                    data: {pageTitle: 'Candidate Posts', pageSubTitle: ''},
                })
                .state('candidates', {
                    url: '/candidates',
                    templateUrl: "votex/candidates",
                    data: {pageTitle: 'Candidates', pageSubTitle: ''},
                })
                 .state('voters', {
                    url: '/voters',
                    templateUrl: "votex/voters",
                    data: {pageTitle: 'Voters', pageSubTitle: ''},
                })
                 .state('votes', {
                    url: '/votes',
                    templateUrl: "votex/votes",
                    data: {pageTitle: 'Votes', pageSubTitle: ''},
                })
                 .state("current", {
                    url: "/current",
                    templateUrl: "default/current",
                    data: {pageTitle: 'Current Results', pageSubTitle: ''}
                })
                 .state("candidate-qs", {
                    url: "/candidate?candidate_id",
                    templateUrl: "votex/candidates/candidateimage",
                    controller: function($scope, $stateParams) {
                        $scope.candidate_id = $stateParams.candidate_id;
                    },
                    data: {pageTitle: 'Google Map,Barcode,Thumb Print and e-cards', pageSubTitle: ''}
                })
               
                
               
               
                 
    }]);

/* Init global settings and run the app */
MuskeyApp.run(["$rootScope", "settings", "$state", function($rootScope, settings, $state) {
        $rootScope.$state = $state; // state to be accessed from view
    }]);