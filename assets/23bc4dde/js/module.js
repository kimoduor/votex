/*
 *Module based js functions
 *@auuthor Joakim <kimoduor@gmail.com>
 */
var UsersModule = {
    Roles: {
        init: function() {
            'use strict';
            var $this = this;
            $this.toggleCheckAll();
        },
        toggleCheckAll: function() {
            $('button.my-select-all').on('click', function(event) {
                event.preventDefault();
                var checkbox = $('input:checkbox.my-roles-checkbox')
                        , isChecked = checkbox.is(':checked');
                if (isChecked) {
                    checkbox.prop('checked', false);
                    $(this).text('Uncheck all');
                } else {
                    checkbox.prop('checked', true);
                    $(this).text('Check all');
                }
            });
        }
    },
    User: {
        init: function() {
            'use strict';
            this.deleteUser();
        },
        initForm: function() {
            'use strict';
            var $this = this;
            $this.toggle_role();
        },
        toggle_role: function() {
            var selector = '#Users_user_level'
                    , toggle_role = function(e) {
                        var role_selector = '#Users_role_wrapper';
                        if ($(e).val() === $(e).data('show-role')) {
                            $(role_selector).removeClass('hidden');
                        }
                        else {
                            $(role_selector).addClass('hidden');
                        }
                    };

            //on page load
            toggle_role(selector);
            //on change
            $(selector).on('change', function() {
                toggle_role(this);
            });
        },
        deleteUser: function() {
            var selector = '#delete_user';
            var delete_user = function(e) {
                var url = $(e).data('href')
                        , confirm_msg = $(e).data('confirm');
                BootstrapDialog.confirm(confirm_msg, function(result) {
                    if (result) {
                        $.ajax({
                            type: 'POST',
                            url: url,
                            dataType: 'json',
                            success: function(data) {
                                MyUtils.reload(data.redirectUrl);
                            },
                            beforeSend: function() {
                                MyUtils.startBlockUI('Please wait...');
                            },
                            complete: function() {
                                MyUtils.stopBlockUI();
                            },
                            error: function(XHR) {
                                MyUtils.showAlertMessage(XHR.responseText, 'error');
                            }
                        });
                    }
                });
            };
            $(selector).on('click', function(e) {
                e.preventDefault();
                delete_user(this);
            });
        },
    },
    UserLevels: {},
};


