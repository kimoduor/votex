/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var VoucherModule = VoucherModule || {};
//voucher registration
VoucherModule.Voucher = {
    optionsDefault: {
        farmersSearchUrl: undefined,
        FarmerNameSelector: '#Vouchers_name',
        FarmerIdSelector: '#Vouchers_farmer_id',
        FarmerNrcSelector: '#Vouchers_nrc_no',
        FarmerCampIdSelector: '#Vouchers_camp_id',
        FarmerMobileSelector: '#Vouchers_mobile',
        FarmerGenderSelector: '#Vouchers_gender',
    },
    options: {},
    init: function (options) {
        'use strict';
        this.options = $.extend({}, this.optionsDefault, options || {});
        this.initTypeAhead();
    },
    initTypeAhead: function () {
        var $this = this;
        var form_fields_selector = '#voucher_form_fields';
        var init_typeahead = function () {
            var loading_selector = '#farmers_search_loading';
            var url = MyUtils.addParameterToURL($this.options.farmersSearchUrl, 'q', '%QUERY');
            var engine = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace(d.name);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: url,
                    ajax: {
                        beforeSend: function () {
                            $(loading_selector).removeClass('hidden');
                        },
                        complete: function () {
                            $(loading_selector).addClass('hidden');
                        },
                    },
                },
            });
            engine.initialize();
            $('#Vouchers_farmer_search_term').typeahead(null, {
                name: 'farmers-search',
                source: engine.ttAdapter(),
                minLength: 1,
                displayKey: 'nrc_no',
                templates: {
                    empty: [
                        '<div class="empty-message text-danger padding-5">',
                        'Unable to find a Farmer with NRC number or Farmer number specified',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<p><strong>{{name}}</strong></p>')
                }
            }).on('typeahead:selected', function (e, datum) {
                $($this.options.FarmerIdSelector).val(datum.farmer_id);
                $($this.options.FarmerNameSelector).val(datum.name);
                $($this.options.FarmerNrcSelector).val(datum.nrc_no);
                $($this.options.FarmerCampIdSelector).val(datum.agri_camp_id);
                $($this.options.FarmerMobileSelector).val(datum.mobile);
                $($this.options.FarmerGenderSelector).val(datum.gender);
                toggle_form_fields();
            }
            );
        };
        var toggle_form_fields = function () {
            if (!MyUtils.empty($($this.options.FarmerIdSelector).val())) {
                $(form_fields_selector).removeClass('hidden');
            } else {
                $(form_fields_selector).addClass('hidden');
            }

        };

        init_typeahead();
        toggle_form_fields();
    }
};
//VoucherInputs
VoucherModule.VoucherInputs = {
    options: {
        inputIdSelector: 'select.Voucherinputs_input_id',
    },
    init: function (options) {
        'use strict';
        this.options = $.extend({}, this.options, options || {});
        this.initSelect2();
    },
    initSelect2: function () {
        $(this.options.inputIdSelector).select2({});
    },
};
//invoice registration
VoucherModule.Invoice = {
    optionsDefault: {
        voucherSearchUrl: undefined,
        voucherIdSelector: '#Invoices_voucher_id',
        farmerIdSelector: '#Invoices_farmer_id',
        farmerSelector: '#Invoices_farmer',
    },
    options: {
    },
    init: function (options) {
        'use strict';
        this.options = $.extend({}, this.optionsDefault, options || {});
        this.initTypeAhead();
    },
    initTypeAhead: function () {
        var $this = this;
        var form_fields_selector = '#invoice_form_fields';
        var init_typeahead = function () {
            var loading_selector = '#typeahead_search_loading';
            var url = MyUtils.addParameterToURL($this.options.voucherSearchUrl, 'q', '%QUERY');
            var engine = new Bloodhound({
                datumTokenizer: function (d) {
                    return Bloodhound.tokenizers.whitespace(d.voucher_no);
                },
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: url,
                    ajax: {
                        beforeSend: function () {
                            $(loading_selector).removeClass('hidden');
                        },
                        complete: function () {
                            $(loading_selector).addClass('hidden');
                        },
                    },
                },
            });
            engine.initialize();
            $('#Invoices_voucher_no').typeahead(null, {
                name: 'vouchers-search',
                source: engine.ttAdapter(),
                minLength: 1,
                displayKey: 'voucher_no',
                templates: {
                    empty: [
                        '<div class="empty-message text-danger padding-5">',
                        'Unable to find a voucher with the voucher number specified',
                        '</div>'
                    ].join('\n'),
                    suggestion: Handlebars.compile('<p><strong>{{voucher_no}}</strong></p>')
                }
            }).on('typeahead:selected', function (e, datum) {
                console.log(datum);
                $($this.options.voucherIdSelector).val(datum.voucher_id);
                $($this.options.farmerIdSelector).val(datum.farmer_id);
                $($this.options.farmerSelector).val(datum.farmer);

                toggle_form_fields();
            }
            );
        };
        var toggle_form_fields = function () {
            if (!MyUtils.empty($($this.options.voucherIdSelector).val())) {
                $(form_fields_selector).removeClass('hidden');
            } else {
                $(form_fields_selector).addClass('hidden');
            }

        };

        init_typeahead();
        toggle_form_fields();
    }
};
//InvoiceInputs
VoucherModule.InvoiceInputs = {
    options: {
        filterSuppliersUrl: undefined,
        inputIdSelector: 'select.Invoiceinputs_input_id',
        supplierIdSelector: 'select.Invoiceinputs_supplier_id',
        unitPriceSelector: 'input.Invoiceinputs_unit_price',
        qtySelector: 'input.Invoiceinputs_qty',
        totalPriceSelector: 'input.Invoiceinputs_total_price',
    },
    init: function (options) {
        'use strict';
        this.options = $.extend({}, this.options, options || {});
        this.initSelect2();
        this.saveRowEvents();
    },
    initSelect2: function () {
        var $this = this;

        $(this.options.inputIdSelector + ',' + this.options.supplierIdSelector).select2({});

        $this.filterSuppliers(this.options.inputIdSelector, false);
        $(this.options.inputIdSelector).each(function () {
            var tr = $(this).closest('tr');
            var supplier_id = tr.find($this.options.supplierIdSelector).select2('val');
            if (!MyUtils.empty($(this).val()) && MyUtils.empty(supplier_id)) {
                $this.filterSuppliers(this, false);
            }
        });
        $(this.options.inputIdSelector).off('change').on('change', function () {
            $this.filterSuppliers(this, true);
        });

        $(this.options.supplierIdSelector).off('change').on('change', function () {
            $this.saveRow(this);
        });
    },
    saveRowEvents: function () {
        var $this = this;
        var on_blur_selector = $this.options.unitPriceSelector + ',' + $this.options.qtySelector;
        $(on_blur_selector).off('blur').on('blur', function () {
            $this.saveRow(this);
        });

    },
    saveRow: function (e) {
        var tr = $(e).closest('tr');
        var input_id = tr.find(this.options.inputIdSelector).select2('val')
                , supplier_id = tr.find(this.options.supplierIdSelector).select2('val')
                , unit_price = tr.find(this.options.unitPriceSelector).val()
                , qty = tr.find(this.options.qtySelector).val();

        if (!MyUtils.empty(input_id) && !MyUtils.empty(supplier_id) && !MyUtils.empty(unit_price) && !MyUtils.empty(qty)) {
            tr.find('.save-line-item').trigger('click');
        }
    },
    afterSave: function (tr, response, settings) {
        if (!MyUtils.empty(response.data.total_price)) {
            tr.find(this.options.totalPriceSelector).val(response.data.total_price);
        }
    },
    filterSuppliers: function (e, save) {
        var $this = this;
        var url = this.options.filterSuppliersUrl
                , input_id = $(e).val()
                , tr = $(e).closest('tr')
                , supplier_selector = tr.find(this.options.supplierIdSelector);
        $.ajax({
            url: url,
            data: 'input_id=' + input_id,
            dataType: 'json',
            success: function (data) {
                if (data) {
                    MyUtils.populateDropDownList(supplier_selector, data);
                    if (save) {
                        $this.saveRow(e);
                    }
                }
            }
        });
    },
};


